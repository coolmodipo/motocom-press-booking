
// sales bar chart

    $(function() {
        //some data
        var d1 = [];
        for (var i = 0; i <= 10; i += 1)
            d1.push([i, parseInt(Math.random() * 60)]);

        var d2 = [];
        for (var i = 0; i <= 10; i += 1)
            d2.push([i, parseInt(Math.random() * 40)]);

        var d3 = [];
        for (var i = 0; i <= 10; i += 1)
            d3.push([i, parseInt(Math.random() * 25)]);

        var ds = new Array();

        ds.push({
            label : "Unused Fleet",
            data : d1,
            bars : {
                order : 1
            }
        });
        ds.push({
            label : "Serviced",
            data : d2,
            bars : {
                order : 2
            }
        });
        ds.push({
            label : "Faulty",
            data : d3,
            bars : {
                order : 3
            }
        });

        var stack = 0,
            bars = true,
            lines = true,
            steps = true;

        var options = {
            bars : {
                show : true,
                barWidth : 0.2,
                fill : 1
            },
            grid : {
                show : true,
                aboveData : false,
                labelMargin : 5,
                axisMargin : 0,
                borderWidth : 1,
                minBorderMargin : 5,
                clickable : true,
                hoverable : true,
                autoHighlight : false,
                mouseActiveRadius : 20,
                borderColor : '#f5f5f5',
                color: "#AFAFAF",
                backgroundColor: '#e6e6e6'
            },
            series : {
                stack : stack
            },
            legend : {
                position : "ne",
                margin : [0, 0],
                noColumns : 0,
                labelBoxBorderColor : null,
                labelFormatter : function(label, series) {
                    // just add some space to labes
                    return '' + label + '&nbsp;&nbsp;';
                },
                width : 30,
                height : 5
            },
            yaxis : {
                tickColor : '#f5f5f5',
                font : {
                    color : '#bdbdbd'
                }
            },
            xaxis : {
                tickColor : '#f5f5f5',
                font : {
                    color : '#bdbdbd'
                }
            },
            colors : ["#707CD2", "#FFC36D", "#FF7676"],
            tooltip : true, //activate tooltip
            tooltipOpts : {
                content : "%s : %y.0",
                shifts : {
                    x : -30,
                    y : -50
                }
            }
        };

        $.plot($(".sales-bars-chart"), ds, options);
    });
