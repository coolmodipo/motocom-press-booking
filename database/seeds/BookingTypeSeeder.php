<?php

use Illuminate\Database\Seeder;

use App\Bookingtype;

class BookingTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Single', 'slug' => 'single', 'hex_code' => '#CD5C5C'],
            ['name' => 'Comparison', 'slug' => 'comparison', 'hex_code' => '#FFA07A'],
            ['name' => 'Group', 'slug' => 'group', 'hex_code' => '#a9cce3'],
            ['name' => 'Photoshoot', 'slug' => 'photoshoot', 'hex_code' => '#76d7c4'],
            ['name' => 'Internal Evaluation / Run-in', 'slug' => 'internal-evaluation-run-in', 'hex_code' => '#f0b27a'],
            ['name' => 'Launch', 'slug' => 'launch', 'hex_code' => '#b2babb'],
            ['name' => 'Long Term Test', 'slug' => 'long-term-test', 'hex_code' => '#45b39d'],
            ['name' => 'Courtesy Loan (Service)', 'slug' => 'courtesy-loan-service', 'hex_code' => '#e74c3c'],
            ['name' => 'Event', 'slug' => 'event', 'hex_code' => '#dc7633'],
            ['name' => 'N/A', 'slug' => 'n-a', 'hex_code' => '#ffffff'],
            ['name' => 'Demo', 'slug' => 'demo', 'hex_code' => '#78281f'],
            ['name' => 'Single', 'slug' => 'single', 'hex_code' => '#7d3c98'],
            ['name' => 'B-King Roadshow', 'slug' => 'b-king-roadshow', 'hex_code' => '#16a085'],
            ['name' => 'Technical Training', 'slug' => 'technical-training', ],
            ['name' => 'Photoshoot', 'slug' => 'photoshoot', 'hex_code' => '#1e8449'],
            ['name' => 'Race', 'slug' => 'race', 'hex_code' => '#7b7d7d'],
            ['name' => 'BSM Training School', 'slug' => 'bsm-training-school', 'hex_code' => '#5d6d7e'],
            ['name' => 'Defleet', 'slug' => 'defleet', 'hex_code' => '#0e6251'],
            ['name' => 'Off road lease', 'slug' => 'off-road-lease', 'hex_code' => '#2e86c1'],
            ['name' => 'TT Marshall Bike', 'slug' => 'tt-marshall-bike', 'hex_code' => '#7e5109'],
            ['name' => 'Show Room', 'slug' => 'show-room', 'hex_code' => '#aed6f1'],
            ['name' => 'Sales Campaign', 'slug' => 'sales-campaign', 'hex_code' => '#f39c12'],
            ['name' => 'Tyre Launch', 'slug' => 'tyre-launch', 'hex_code' => '#d98880'],
            ['name' => 'Dealer Event', 'slug' => 'dealer-event', 'hex_code' => '#2c3e50'],
            ['name' => 'SGB Event', 'slug' => 'sgb-event', 'hex_code' => '#17a589'],
            ['name' => 'Race Team Loan', 'slug' => 'race-team-loan', 'hex_code' => '#d2b4de'],
            ['name' => 'Ride 2 Work Week', 'slug' => 'ride-2-work-week', 'hex_code' => '#154360'],
            ['name' => 'Ace Cafe', 'slug' => 'ace-cafe', 'hex_code' => '#95a5a6'],
            ['name' => 'Brighton Burn Up', 'slug' => 'brighton-burn-up', 'hex_code' => '#edbb99'],
            ['name' => 'Test Ride Roadshow', 'slug' => 'test-ride-roadshow', 'hex_code' => '#3498db'],
            ['name' => 'Track bike', 'slug' => 'track-bike', 'hex_code' => '#d4ac0d'],
            ['name' => 'M.O.R.E', 'slug' => 'm-o-r-e', 'hex_code' => '#21618c'],
            ['name' => 'Copdock Bike Show', 'slug' => 'copdock-bike-show', 'hex_code' => '#45b39d']
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('bookingtypes')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        foreach ($items as $item) {
            Bookingtype::create($item);
        }
    }
}
