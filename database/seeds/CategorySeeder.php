<?php

use Illuminate\Database\Seeder;

use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Street', 'slug' => 'street'],
            ['name' => 'Supersports', 'slug' => 'supersports'],
            ['name' => 'Adventure Sports', 'slug' => 'adventure-sports'],
            ['name' => 'Cruiser', 'slug' => 'cruiser'],
            ['name' => 'Learner', 'slug' => 'learner'],
            ['name' => 'Scooter', 'slug' => 'scooter'],
            ['name' => 'Dual Purpose', 'slug' => 'dual-purpose'],
            ['name' => 'Motocross', 'slug' => 'motocross'],
            ['name' => 'Endurance', 'slug' => 'endurance'],
            ['name' => 'ATV', 'slug' => 'atv'],
            ['name' => 'Junior', 'slug' => 'junior']
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('categories')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        foreach ($items as $item) {
            Category::create($item);
        }
    }
}
