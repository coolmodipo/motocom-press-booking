<?php

use Illuminate\Database\Seeder;

use App\MessageType;

class MessageTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['type' => 'Inbox'],
            ['type' => 'Sent'],
            ['type' => 'Trash'],

        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('message_types')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        foreach ($items as $item) {
            MessageType::create($item);
        }
    }
}
