<?php

use Illuminate\Database\Seeder;

use App\Insurance;

class InsuranceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Provided by Borrower', 'slug' => 'provided-by-borrower'],
            ['name' => 'Suzuki GB', 'slug' => 'suzuki-gb'],
            ['name' => 'Honda GB', 'slug' => 'honda-gb'],
            ['name' => 'Kawasaki GB', 'slug' => 'kawasaki-gb'],
            ['name' => 'Yamaha GB', 'slug' => 'yamaha-gb'],

        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('insurances')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        foreach ($items as $item) {
            Insurance::create($item);
        }
    }
}
