<?php

use Illuminate\Database\Seeder;

use App\Message;

class MessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'title' => 'This is test number 1',
                'slug' => '1-this-is-test-number-1',
                'message' => 'This is the message for Test 1',
                'sender_id' => 2,
                'client_id' => 1,
                'type_id' => 1
            ],
            [
                'title' => 'This is test number 2',
                'slug' => '2-this-is-test-number-2',
                'message' => 'This is the message for Test 2',
                'sender_id' => 3,
                'client_id' => 1,
                'type_id' => 1
            ],
            [
                'title' => 'This is test number 3',
                'slug' => '3-this-is-test-number-3',
                'message' => 'This is the message for Test 3',
                'sender_id' => 4,
                'client_id' => 1,
                'type_id' => 1
            ],
            [
                'title' => 'This is test number 4',
                'slug' => '4-this-is-test-number-4',
                'message' => 'This is the message for Test 4',
                'sender_id' => 6,
                'client_id' => 1,
                'type_id' => 1
            ],
            [
                'title' => 'This is test number 5',
                'slug' => '5-this-is-test-number-5',
                'message' => 'This is the message for Test 5',
                'sender_id' => 7,
                'client_id' => 1,
                'type_id' => 1
            ],
        ];

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('messages')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        foreach ($items as $item) {
            Message::create($item);
        }
    }
}
