<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMileageInfoToBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->integer('mileage_out')->after('insurance_id')->nullable();
            $table->integer('mileage_in')->after('mileage_out')->nullable();
            $table->integer('mileage_used')->after('mileage_in')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('mileage_out');
            $table->dropColumn('mileage_out');
            $table->dropColumn('mileage_used');
        });
    }
}
