<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFleetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fleets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vehicle_id');
            $table->string('reg_number');
            $table->string('frame_number')->nullable();
            $table->string('engine')->nullable();
            $table->integer('colour_id');
            $table->integer('status_id')->default(1);
            $table->text('accessories')->nullable();
            $table->text('notes')->nullable();
            $table->integer('booking_id')->nullable();
            $table->string('slug');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fleets');
    }
}
