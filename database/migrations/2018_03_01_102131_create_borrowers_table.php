<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBorrowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrowers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('organisation_name');
            $table->string('address');
            $table->string('postcode');
            $table->string('telephone');
            $table->string('email')->unique();
            $table->string('home_address')->nullable();
            $table->string('home_postcode')->nullable();
            $table->string('home_telephone')->nullable();
            $table->integer('company_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrowers');
    }
}
