<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('borrower_id');
            $table->integer('fleet_id');
            $table->integer('bookingtype_id');
            $table->date('start_date');
            $table->date('end_date');
            $table->text('special_request')->nullable();
            $table->string('collector');
            $table->time('collector_time');
            $table->string('returnee');
            $table->time('returnee_time');
            $table->integer('insurance_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
