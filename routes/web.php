<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/login-now', function () {
    return view('_layouts.login');
});

// Authentication Routes...
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', function () {

    Auth::guard()->logout();

    Session::flush();

    Session::regenerate();

    return redirect('/');

})->name('user.logout');

// Password Reset Routes...
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/home', 'HomeController@index')->name('home');

//CLIENT PAGES
Route::group(['middleware' => ['client'], 'prefix' => 'client', 'as' => 'client.'], function () {

    Route::get('/', 'Client\HomeController@index')->name('index');
    Route::get('/profile', 'Client\HomeController@profile')->name('profile');
    Route::post('/update-profile', 'Client\HomeController@updateProfile')->name('update.profile');
    Route::post('/add-company', 'Client\HomeController@addCompany')->name('add.company');
    Route::post('/company/make-default/{uid}/{id}', 'Client\HomeController@makeDefaultCompany')->name('update.company.defualt');
    Route::post('/company/delete/{id}', 'Client\HomeController@deleteCompany')->name('update.company.delete');
    Route::post('/company/add-user-form/{id}', 'Client\HomeController@addUserCompanyForm')->name('company.add.user.form');
    Route::post('/company/add-user', 'Client\HomeController@addUserCompany')->name('company.add.user');

    //NOTIFICATION PROCESS
    Route::post('/notification/update/{cid}/{type}', 'Client\NotificationController@update')->name('update.notification');

    //MESSAGES
    Route::get('/messages/{type}', 'Client\MessagesController@index')->name('messages');
    Route::get('/message/{slug}', 'Client\MessagesController@show')->name('show.message');
    Route::post('/message/reply-message-form/{id}', 'Client\MessagesController@getReplyForm')->name('message.reply.form');
    Route::post('/message/reply', 'Client\MessagesController@reply')->name('message.reply');
    Route::post('/message/create', 'Client\MessagesController@store')->name('message.create');
    Route::post('/message/delete/{id}', 'Client\MessagesController@delete')->name('message.delete');
    Route::post('/message/restore/{id}', 'Client\MessagesController@restore')->name('message.restore');
    Route::post('/message/read/{id}', 'Client\MessagesController@read')->name('message.read');
    Route::post('/message/unread/{id}', 'Client\MessagesController@unread')->name('message.unread');
    Route::post('/message/trash/{id}', 'Client\MessagesController@trash')->name('message.trash');

    //BOOKING PROCESS
    Route::resource('/bookings', 'Client\BookingController');

    //MODELS
    Route::get('/models', 'Client\ModelController@index')->name('models');
    Route::post('/model/create', 'Client\ModelController@create')->name('model.create');
    Route::post('/model-fleet-create-form/{id}', 'Client\ModelController@getFleetForm')->name('create.model.fleet.form');
    Route::post('/model-edit-form/{id}', 'Client\ModelController@getForm')->name('edit.model.form');
    Route::get('/model/{slug}', 'Client\ModelController@availability')->name('model.availability');
    Route::post('/model/edit', 'Client\ModelController@update')->name('update.model');

    //FLEETS
    Route::get('/fleets', 'Client\FleetController@index')->name('fleets');
    Route::get('/fleet/{slug}', 'Client\FleetController@show')->name('fleet.show');
    Route::post('/fleet/create', 'Client\FleetController@create')->name('fleet.create');
    Route::post('/fleet/add-accessory', 'Client\FleetController@addAccessory')->name('fleet.add.accessory');
    Route::post('/fleet/update-accessory', 'Client\FleetController@updateAccessory')->name('fleet.update.accessory');
    Route::post('/fleet/add-note', 'Client\FleetController@addNote')->name('fleet.add.note');
    Route::post('/fleet/update-note', 'Client\FleetController@updateNote')->name('fleet.update.note');
    Route::post('/fleet-edit-form/{id}', 'Client\FleetController@editForm')->name('edit.fleet.form');
    Route::post('/fleet/edit', 'Client\FleetController@update')->name('update.fleet');
    Route::post('/fleet-book-form/{id}', 'Client\FleetController@bookingForm')->name('book.fleet.form');
    Route::post('/fleet/book', 'Client\FleetController@book')->name('fleet.book');
    Route::post('/fleet-collected-form/{id}/{bid}', 'Client\FleetController@collectedForm')->name('collected.fleet.form');
    Route::post('/fleet/collected', 'Client\FleetController@collected')->name('fleet.collected');
    Route::post('/fleet-returned-form/{id}/{bid}', 'Client\FleetController@returnedForm')->name('returned.fleet.form');
    Route::post('/fleet/returned', 'Client\FleetController@returned')->name('fleet.returned');
    Route::post('/fleet-status-form/{id}', 'Client\FleetController@statusForm')->name('status.fleet.form');
    Route::post('/fleet/status', 'Client\FleetController@updateStatus')->name('fleet.status');
    Route::post('/fleet-return-form/{id}', 'Client\FleetController@returnForm')->name('return.fleet.form');
    Route::post('/fleet/return', 'Client\FleetController@returnItem')->name('fleet.return');

    Route::post('/fleet-available-form/{id}', 'Client\FleetController@availableForm')->name('available.fleet.form');
    Route::post('/fleet/available', 'Client\FleetController@makeAvailable')->name('fleet.available');

    Route::post('/fleet-information-form/{id}', 'Client\FleetController@informationForm')->name('information.fleet.form');
    Route::post('/fleet/delete/{id}', 'Client\FleetController@delete')->name('fleet.delete');

    //Borrowers
    Route::get('/borrowers/{cid}', 'Client\BorrowerController@index')->name('borrowers.index');
    Route::post('/borrower-form/{bid}', 'Client\BorrowerController@editForm')->name('borrower.form');
    Route::post('/borrower-add', 'Client\BorrowerController@store')->name('borrower.create');
    Route::post('/borrower-update', 'Client\BorrowerController@update')->name('borrower.update');

    //LOG BACK AS ADMIN
    Route::get('users/logback-as/{id}',  'Admin\UserController@logbackAs')->name('logback.as');

});

//ADMIN PAGES
Route::group(['middleware' => ['admin'], 'prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::get('/', 'Admin\HomeController@index')->name('index');
    Route::get('/clients', 'Admin\HomeController@clients')->name('clients');

    //USERS
    Route::get('/users', 'Admin\UserController@index')->name('users');
    Route::post('/user-form/{id}', 'Admin\UserController@getUserForm')->name('user.info');
    Route::post('/user-role-form/{id}', 'Admin\UserController@getRoleForm')->name('user.role.info');
    Route::post('/user-permissions-form/{id}', 'Admin\UserController@getPermissionsForm')->name('user.permission.info');
    Route::post('/user-delete-form/{id}', 'Admin\UserController@getDeleteForm')->name('user.delete.info');
    Route::post('/user/info', 'Admin\UserController@updateUser')->name('update.user');
    Route::post('/user/role', 'Admin\UserController@updateRole')->name('update.role');
    Route::post('/user/permissions', 'Admin\UserController@updatePermissions')->name('update.permissions');
    Route::post('/user/delete', 'Admin\UserController@delete')->name('user.delete');
    Route::post('/user/register', 'Admin\UserController@store')->name('register.user');

    //COMPANIES
    Route::get('/companies', 'Admin\CompanyController@index')->name('companies');
    Route::post('/company/register', 'Admin\CompanyController@store')->name('register.company');
    Route::post('/company-user-form/{id}', 'Admin\CompanyController@companyUsersForm')->name('company.users.form');
    Route::post('/company-form/{id}', 'Admin\CompanyController@getForm')->name('company.info');
    Route::post('/company/update', 'Admin\CompanyController@update')->name('update.company');
    Route::post('/company-delete-form/{id}', 'Admin\CompanyController@getDeleteForm')->name('user.delete.info');
    Route::post('/company/delete', 'Admin\CompanyController@delete')->name('delete.company');

    //BORROWERS
    Route::get('/borrowers', 'Admin\BorrowerController@index')->name('borrowers');
    Route::post('/borrower/create', 'Admin\BorrowerController@create')->name('borrower.create');
    Route::post('/borrower-form/{id}', 'Admin\BorrowerController@getForm')->name('borrower.info');
    Route::post('/borrower/update', 'Admin\BorrowerController@update')->name('update.borrower');
    Route::post('/borrower-delete-form/{id}', 'Admin\BorrowerController@getDeleteForm')->name('borrower.delete.info');
    Route::post('/borrower/delete', 'Admin\BorrowerController@delete')->name('delete.borrower');

    //BOOKING TYPE
    Route::get('/bookingtypes', 'Admin\BookingTypeController@index')->name('bookingtypes');
    Route::post('/bookingtype/create', 'Admin\BookingTypeController@create')->name('bookingtype.create');
    Route::post('/bookingtype-form/{id}', 'Admin\BookingTypeController@getForm')->name('bookingtype.info');
    Route::post('/bookingtype/update', 'Admin\BookingTypeController@update')->name('update.bookingtype');
    Route::post('/bookingtype-delete-form/{id}', 'Admin\BookingTypeController@getDeleteForm')->name('bookingtype.delete.info');
    Route::post('/bookingtype/delete', 'Admin\BookingTypeController@delete')->name('delete.bookingtype');

    //CATEGORIES
    Route::get('/categories', 'Admin\CategoryController@index')->name('categories');
    Route::post('/category/create', 'Admin\CategoryController@create')->name('category.create');
    Route::post('/category-form/{id}', 'Admin\CategoryController@getForm')->name('category.info');
    Route::post('/category/update', 'Admin\CategoryController@update')->name('update.category');
    Route::post('/category-delete-form/{id}', 'Admin\CategoryController@getDeleteForm')->name('category.delete.info');
    Route::post('/category/delete', 'Admin\CategoryController@delete')->name('delete.category');

    //VEHICLE (Shown as MODEL on the frontend)
    Route::get('/models', 'Admin\VehicleController@index')->name('vehicles');
    Route::post('/model/create', 'Admin\VehicleController@create')->name('vehicle.create');
    Route::post('/model-form/{id}', 'Admin\VehicleController@getForm')->name('vehicle.info');
    Route::post('/model/update', 'Admin\VehicleController@update')->name('update.vehicle');
    Route::post('/model-delete-form/{id}', 'Admin\VehicleController@getDeleteForm')->name('vehicle.delete.info');
    Route::post('/model/delete', 'Admin\VehicleController@delete')->name('delete.vehicle');

    //FLEET
    Route::get('/fleets', 'Admin\FleetController@index')->name('fleets');
    Route::get('/fleets/{slug}', 'Admin\FleetController@company')->name('fleets.company');
    Route::post('/fleet/create', 'Admin\FleetController@create')->name('fleet.create');
    Route::post('/fleet-form/{id}', 'Admin\FleetController@getForm')->name('fleet.info');
    Route::post('/fleet/update', 'Admin\FleetController@update')->name('update.fleet');
    Route::post('/fleet-delete-form/{id}', 'Admin\FleetController@getDeleteForm')->name('fleet.delete.info');
    Route::post('/fleet/delete', 'Admin\FleetController@delete')->name('delete.fleet');

    //SETTINGS: ROLES AND PERMISSIONS
    Route::get('/settings/roles', 'Admin\SettingsController@roles')->name('settings.roles');
    Route::get('/settings/permissions', 'Admin\SettingsController@permissions')->name('settings.permissions');
    Route::post('/settings/roles', 'Admin\SettingsController@roleCreate')->name('settings.role.create');
    Route::post('/settings/permissions', 'Admin\SettingsController@permissionCreate')->name('settings.permission.create');
    Route::post('/settings/role-form/{id}', 'Admin\SettingsController@getRoleForm')->name('settings.role.info');
    Route::post('/settings/permission-form/{id}', 'Admin\SettingsController@getPermissionForm')->name('settings.permission.info');
    Route::post('/settings/role-update', 'Admin\SettingsController@roleUpdate')->name('settings.role.update');
    Route::post('/settings/permission-update', 'Admin\SettingsController@permissionUpdate')->name('settings.permission.update');
    Route::post('/settings/role-delete-form/{id}', 'Admin\SettingsController@getRoleDeleteForm')->name('settings.role.delete.info');
    Route::post('/settings/permission-delete-form/{id}', 'Admin\SettingsController@getPermissionDeleteForm')->name('settings.permission.delete.info');
    Route::post('/settings/role-delete', 'Admin\SettingsController@roleDelete')->name('settings.role.delete');
    Route::post('/settings/permission-delete', 'Admin\SettingsController@permissionDelete')->name('settings.permission.delete');

    //SETTINGS: STATUS
    Route::get('/settings/status', 'Admin\SettingsController@status')->name('settings.status');
    Route::post('/settings/status', 'Admin\SettingsController@statusCreate')->name('settings.status.create');
    Route::post('/settings/status-form/{id}', 'Admin\SettingsController@getStatusForm')->name('settings.status.info');
    Route::post('/settings/status-update', 'Admin\SettingsController@statusUpdate')->name('settings.status.update');
    Route::post('/settings/status-delete-form/{id}', 'Admin\SettingsController@getStatusDeleteForm')->name('settings.status.delete.info');
    Route::post('/settings/status-delete', 'Admin\SettingsController@statusDelete')->name('settings.status.delete');

    //SETTINGS: COLOUR
    Route::get('/settings/colours', 'Admin\SettingsController@colours')->name('settings.colours');
    Route::post('/settings/colour', 'Admin\SettingsController@colourCreate')->name('settings.colour.create');
    Route::post('/settings/colour-form/{id}', 'Admin\SettingsController@getColourForm')->name('settings.colour.info');
    Route::post('/settings/colour-update', 'Admin\SettingsController@colourUpdate')->name('settings.colour.update');
    Route::post('/settings/colour-delete-form/{id}', 'Admin\SettingsController@getColourDeleteForm')->name('settings.colour.delete.info');
    Route::post('/settings/colour-delete', 'Admin\SettingsController@colourDelete')->name('settings.colour.delete');

    //SETTINGS: INSURANCE
    Route::get('/settings/insurances', 'Admin\SettingsController@insurances')->name('settings.insurances');
    Route::post('/settings/insurance', 'Admin\SettingsController@insuranceCreate')->name('settings.insurance.create');
    Route::post('/settings/insurance-form/{id}', 'Admin\SettingsController@getInsuranceForm')->name('settings.insurance.info');
    Route::post('/settings/insurance-update', 'Admin\SettingsController@insuranceUpdate')->name('settings.insurance.update');
    Route::post('/settings/insurance-delete-form/{id}', 'Admin\SettingsController@getInsuranceDeleteForm')->name('settings.insurance.delete.info');
    Route::post('/settings/insurance-delete', 'Admin\SettingsController@insuranceDelete')->name('settings.insurance.delete');

    //SUPER ADMIN OR ADMIN LOGIN IN AS...
    Route::get('users/login-as/{id}',  'Admin\UserController@loginAs')->name('login.as');

});
