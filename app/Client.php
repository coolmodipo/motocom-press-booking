<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'user_id', 'image','lastname', 'mobile', 'phone', 'profile'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'client_id')->where(['default' => 1]);
    }

    public function userCompany()
    {
        return $this->belongsToMany(Company::class, 'client_company');
    }

    public function companies()
    {
        return $this->hasMany(Company::class, 'client_id');
    }

    public function unreadMessages()
    {
        return $this->hasMany(Message::class, 'client_id')->where(['read' => 0]);
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'client_id');
    }

    public function notification()
    {
        return $this->hasOne(Notification::class, 'client_id');
    }

    public function getImageUrlAttribute($value) {

        $imageUrl = "";

        if (!is_null($this->image) && $this->image != '') {
            $imagePath = public_path() . "/images/clients/" . $this->image;
            if (file_exists($imagePath)) {
                $imageUrl = asset("/images/clients/" . $this->image);
            } else {
                $imageUrl = asset("/images/no-image.jpg");
            }
        } else {
            $imageUrl = asset("/images/no-image.jpg");
        }

        return $imageUrl;
    }

}
