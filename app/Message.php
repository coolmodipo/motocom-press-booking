<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id','title','message','sender_id','client_id','parent_id','read','slug'
    ];

    public function owner()
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }

    public function sender()
    {
        return $this->hasOne(Client::class, 'id', 'sender_id');
    }

    public function scopeFindBySlug($query, $slug) {
        return $query->where('slug', '=', $slug)->first();
    }

    public function children() {
        return $this->hasMany(Message::class,'parent_id')->orderBy('created_at', 'desc');
    }

    public function parent() {
        return $this->belongsTo(Message::class,'parent_id');
    }

    public function parentID() {
        return $this->belongsToMany(Message::class,'message_reply', 'message_id', 'reply_id');
    }


}
