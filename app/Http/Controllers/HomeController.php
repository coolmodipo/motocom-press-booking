<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laratrust;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(Auth::user()->client->companys);
        //return view('home');
    }
}
