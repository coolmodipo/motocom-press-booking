<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Form;

use App\Bookingtype;

class BookingTypeController extends Controller
{
    public function index()
    {
        $bookingtypes = BookingType::all();
        return view('admin.bookingtypes.lists', compact('bookingtypes'));
    }

    public function create(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->name);

        BookingType::create($data);

        return redirect()->route('admin.bookingtypes')->with('success', 'That bookingtype has been created.');
    }

    public function getForm($id)
    {
        $bookingtype = BookingType::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Booking Type</h4>
                </div>
    
                <div class="modal-body">
                
                    <input type="hidden" name="booking_id" value="'.$bookingtype->id.'">
    
                    <div class="form-group">
                            '.Form::label('Name', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::text('name', $bookingtype->name, ['class' => 'form-control', 'placeholder' => 'Booking Type Name', 'required']).'
                        </div>
                    </div>
                    
                    <div class="form-group">
                        '.Form::label('Colour', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::text('hex_code', $bookingtype->hex_code, ['class' => 'form-control', 'placeholder' => 'Background Colour', 'required']).'
                        </div>
                    </div>
    
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Edit Booking Type</button>
                </div>';

        return $html;
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->name);

        $bookingtype = BookingType::find($request->booking_id);
        $bookingtype->update($data);

        return redirect()->route('admin.bookingtypes')->with('success', 'That bookingtype has been updated.');
    }

    public function getDeleteForm($id)
    {

        $bookingtype = BookingType::find($id);
        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                
                <input type="hidden" name="booking_id" value="'.$bookingtype->id.'"/>
    
                <div class="modal-body">
                    <p>You are about to delete <strong>'.$bookingtype->name.'</strong>.</p>
                    <p>Do you want to proceed?</p>
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger btn-ok">Delete</button>
                </div>';

        return $html;
    }

    public function delete(Request $request)
    {
        $bookingtype = BookingType::find($request->booking_id);
        $bookingtype->delete();

        return redirect()->route('admin.bookingtypes')->with('warning', 'That bookingtype has been deleted.');
    }
}
