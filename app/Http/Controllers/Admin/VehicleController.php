<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

use Form;

use App\Category;
use App\Company;
use App\Vehicle;


class VehicleController extends Controller
{
    protected $uploadPath;

    public function __construct()
    {
        parent::__construct();
        $this->uploadPath = public_path('images/vehicles');

    }

    public function index()
    {
        $vehicles = Vehicle::all();

        return view('admin.vehicles.lists', compact('vehicles'));
    }

    public function create(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->name);

        $vehicle = Vehicle::create($data);

        $image = $this->handleRequest($request, $vehicle);

        if($image){
            $vehicle->update($image);
        }

        return redirect()->route('admin.vehicles')->with('success', 'That Vehicle has been created.');

    }

    public function getForm($id)
    {
        $vehicle = Vehicle::find($id);

        $html = '<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Model</h4>
            </div>

            <div class="modal-body">
            
                <input type="hidden" name="vehicle_id" value="'.$vehicle->id.'">

                <div class="form-group">
                    '.Form::label('image', NULL, ['class' => 'col-md-4 control-label']).'
                    <div class="col-md-6">
                    <img src="'.$vehicle->image_url.'" alt="'.$vehicle->name.'">                    
                    </div>

                </div>


                <div class="form-group">
                    '.Form::label('image', NULL, ['class' => 'col-md-4 control-label']).'
                    <div class="col-md-6">
                    '.Form::file('image', ['class' => 'field']).'
                    <small>350px X 350px</small>
                    </div>

                </div>

                <div class="form-group">
                    '.Form::label('Name', NULL, ['class' => 'col-md-4 control-label']).'
                    <div class="col-md-6">
                        '.Form::text('name', $vehicle->name, ['class' => 'form-control', 'placeholder' => 'Vehicle Name', 'required']).'
                    </div>
                </div>

                <div class="form-group">
                    '.Form::label('Category', NULL, ['class' => 'col-md-4 control-label']).'
                    <div class="col-md-6">
                        '.Form::select('category_id', Category::pluck('name', 'id'), $vehicle->category->id, ['class' => 'form-control', 'placeholder' => 'Select Vehicle', 'required']).'
                    </div>
                </div>

                <div class="form-group">
                    '.Form::label('Company', NULL, ['class' => 'col-md-4 control-label']).'
                    <div class="col-md-6">
                        '.Form::select('company_id', Company::pluck('company_name', 'id'), $vehicle->company->id, ['class' => 'form-control', 'placeholder' => 'Select Company/ Make', 'required']).'
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update Model</button>
            </div>';

        return $html;
    }

    public function update(Request $request)
    {

        $vehicle = Vehicle::find($request->vehicle_id);

        $data = $this->handleRequest($request, $vehicle);
        $data['slug'] = str_slug($request->name);

        $vehicle->update($data);

        return redirect()->route('admin.vehicles')->with('success', 'That Vehicle has been updated.');
    }

    public function getDeleteForm($id)
    {

        $vehicle = Vehicle::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                
                <input type="hidden" name="vehicle_id" value="'.$vehicle->id.'"/>
    
                <div class="modal-body">
                    <p>You are about to delete <strong>'.$vehicle->name.'</strong>.</p>
                    <p>Do you want to proceed?</p>
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger btn-ok">Delete</button>
                </div>';

        return $html;
    }

    public function delete(Request $request)
    {
        $vehicle = Vehicle::find($request->vehicle_id);
        $this->removeImage($vehicle->image);
        $vehicle->delete();

        return redirect()->route('admin.vehicles')->with('warning', 'That Vehicle has been deleted.');
    }

    /**
     * Request information for image.
     */
    private function handleRequest($request, $vehicle)
    {
        $data = $request->all();

        if ($request->hasFile('image'))
        {
            $this->removeImage($vehicle->image);
            $image       = $request->file('image');
            $fileName    = $vehicle->id.'-'.$image->getClientOriginalName();
            $destination = $this->uploadPath;

            $successfullyUploaded = $image->move($destination, $fileName);

            if($successfullyUploaded){
                Image::make($destination.'/'.$fileName)->resize(269, 160)->save();
            }

            $data['image'] = $fileName;
        }

        return $data;
    }

    /**
     * Remove images from the image directory.
     */
    private function removeImage($image)
    {
        if ( ! empty($image) )
        {
            $imageClientPath     = $this->uploadPath . '/' . $image;

            if ( file_exists($imageClientPath) ) unlink($imageClientPath);
        }
    }
}
