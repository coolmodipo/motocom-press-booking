<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;


use Artisan;
use Auth;
use Form;
use Session;

use App\Client;
use App\Company;
use App\Notification;
use App\Permission;
use App\Role;
use App\User;

class UserController extends Controller
{
    protected $uploadClientPath;
    protected $uploadLogoPath;

    public function __construct()
    {
        parent::__construct();
        $this->uploadClientPath = public_path('images/clients');
        $this->uploadLogoPath = public_path('images/company');

    }

    /**
     * Get list of users.
    */
    public function index()
    {
        $users = User::all();
        return view('admin.users.lists', compact('users'));
    }

    /**
     * Get user form in a modal.
    */
    public function getUserForm($id)
    {
        $user = User::find($id);

        $html = '<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit User: '.$user->name.'</h4>
            </div>

            <div class="modal-body">
            
                <input type="hidden" name="user_id" class="user_id" value="'.$user->id.'"/>
                
                <input type="hidden" name="role_id" class="role_id" value="'.$user->roles->first()->id.'">

                <div class="row">                
                
                <div class="col-md-4">
                
                    <div class="box-body box-profile">
                    
                      <img class="profile-user-img img-responsive img-circle" src="'.$user->client->image_url.'" alt="'.$user->name.' picture">
        
                      <h3 class="profile-username text-center">'.$user->name.'</h3>
                      
                    </div> 
                
                
                </div>
                
                <div class="col-md-8">
                  
                  
                  <div class="col-md-12">
                  
                        <div class="form-group">                        
                            '.Form::label('image').'
                            '.Form::file('image', ['class' => 'field']).'
                            <small>128x128px</small>
                        </div>
                        
                        <div class="form-group">
                            '.Form::label('firstname').'
                            '.Form::text('firstname', $user->client->firstname, ['class' => 'form-control']).'                
                        </div>
                        
                        <div class="form-group">
                            '.Form::label('lastname').'
                            '.Form::text('lastname', $user->client->lastname, ['class' => 'form-control']).'
                        </div>
                     
                        <div class="form-group">
                            '.Form::label('email').'
                            '.Form::email('email', $user->email, ['class' => 'form-control']).'
                        </div>
                        
                  </div> 
                  
                  
                </div>
                
               
              </div>  
              
              </div>
             

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save User</button>
            </div>';

        return $html;
    }

    /**
     * Get role form in a modal.
    */
    public function getRoleForm($id)
    {
        $user = User::find($id);

        $html = '<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit User Role: '.$user->name.'</h4>
            </div>

            <div class="modal-body">
                <input type="hidden" name="user_id" class="user_id" value="'.$user->id.'"/>

                <div class="form-group">
                    '.Form::label('role', NULL, ['class' => 'col-md-4 control-label']).'
                    <div class="col-md-6">
                        '.Form::select('role', Role::pluck('display_name', 'id'), $user->roles->first()->id, ['class' => 'form-control', 'placeholder' => 'Select Role', 'required']).'                            
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save User</button>
            </div>';

        return $html;
    }

    /**
     * Get permission form in a modal.
    */
    public function getPermissionsForm($id)
    {
        $user = User::find($id);
        $userPerm = $user->permissions->pluck('id')->toArray();
        $permissions = Permission::all();

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit User Permissions: '.$user->name.'</h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" name="user_id" class="user_id" value="'.$user->id.'"/>

                    <div class="row">

                        <div class="col-md-12">';

                        foreach ($permissions as $permission){


                            $html .= '<label class="col-md-3checkbox-inline" for="checkboxes-0" style="margin-right: 15px">
                                        <input type="checkbox" name="user_permission[]" value="'.$permission->id.'" '.(in_array( $permission->id, $userPerm ) ? 'checked="checked"' : '').' />
                                         '.$permission->display_name.'
                                      </label>';


                        }

                 $html .= '</div>

                    </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save User</button>
                </div>';

        return $html;
    }

    /**
     * Get company form in a modal.
    */
    public function getDeleteForm($id)
    {
        $user = User::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                
                <input type="hidden" name="user_id" value="'.$user->id.'"/>
    
                <div class="modal-body">
                    <p>You are about to delete <strong>'.$user->name.'</strong>.</p>
                    <p>Do you want to proceed?</p>
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger btn-ok">Delete</button>
                </div>';

        return $html;
    }

    /**
     * Update user from getUserForm() from modal function.
    */
    public function updateUser(Request $request)
    {
        //dd($request->all());
        //$data = $request->all();
        $user = User::find($request->user_id);
        $data = $this->handleRequest($request, $user);

        //dd($data);
        $data['name'] = $request->firstname.' '.$request->lastname;

        $user->update($data);
        $user->client->update($data);

        if($user->client->company){
            $user->client->company->update($data);
        }
        else{
            $data['client_id'] = $user->client->id;
            $company = Company::create($data);
        }

        return redirect()->route('admin.users')->with('success', 'That user\'s role has been updated.');
    }

    /**
     * Update role from getRoleForm() from modal function.
    */
    public function updateRole(Request $request)
    {
        //dd($request);
        $user = User::find($request->user_id);
        $user->roles()->sync($request->role);

        Artisan::call('cache:clear');
        Artisan::call('view:clear');

        return redirect()->route('admin.users')->with('success', 'That user\'s role has been updated.');
    }

    /**
     * Update permission from getPermissionForm() from modal function.
    */
    public function updatePermissions(Request $request)
    {
        //dd($request->all());
        $user = User::find($request->user_id);
        $user->permissions()->sync($request->user_permission);

        return redirect()->route('admin.users')->with('success', 'That user\'s permissions have been updated.');
    }

    /**
     * Create new user.
    */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['name'] = $request->firstname.' '.$request->lastname;

        //CREATE A NEW USER
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        //IF NOT ADMIN, CREATE CLIENT
        if($request->roles == 3){

            //Create client
            $client = Client::create([
                'firstname'     => $data['firstname'],
                'lastname'      => $data['lastname'],
                'user_id'       => $user->id,
            ]);

            //Create client notification
            Notification::create([
               'client_id'      => $client->id,
               'email'          => 0,
               'site'           => 0,
            ]);
        }

        //GIVE USER A ROLE
        $user->roles()->attach($request->roles);

        return redirect()->route('admin.users')->with('success', 'That user has been created.');
    }

    /**
     * Admin can log in as a user.
    */
    public function loginAs($id)
    {
        Session::put('adminUser', Auth::user()->id);
        Auth::guard()->logout();
        $user =  User::find($id);
        Auth::login($user);
        //return Redirect::home();
        return redirect()->route('client.index')->with('message', 'You have logged in as '. $user->name);
    }

    /**
     * Admin can log back.
    */
    public function logbackAs($id)
    {
        Auth::guard()->logout();
        $user =  User::find($id);
        Session::flush('adminUser');
        Auth::login($user);
        //return Redirect::home();
        return redirect()->route('admin.index');
    }

    /**
     * Delete user.
    */
    public function delete(Request $request)
    {
        //dd($request);
        $user = User::find($request->user_id);
        $user->delete();

        return redirect()->route('admin.users')->with('warning', 'That user has been deleted from the records.');

    }

    /**
     * Request information for image.
    */
    private function handleRequest($request, $user)
    {
        $data = $request->all();

        if ($request->hasFile('image'))
        {
            $this->removeImage($user->client->image);
            $image       = $request->file('image');
            $fileName    = $user->id.'-'.$image->getClientOriginalName();
            $destination = $this->uploadClientPath;

            $successfullyUploaded = $image->move($destination, $fileName);

            if($successfullyUploaded){
                Image::make($destination.'/'.$fileName)->resize(300, 200)->save();
            }

            $data['image'] = $fileName;
        }

        if ($request->hasFile('logo'))
        {
            $this->removeImage($user->client->company->logo);
            $image       = $request->file('logo');
            $fileName    = $user->id.'-'.$image->getClientOriginalName();
            $destination = $this->uploadLogoPath;

            $successfullyUploaded = $image->move($destination, $fileName);

            if($successfullyUploaded){
                Image::make($destination.'/'.$fileName)->resize(300, 200)->save();
            }

            $data['logo'] = $fileName;
        }

        return $data;
    }

    /**
     * Remove images from the image directory.
    */
    private function removeImage($image)
    {
        if ( ! empty($image) )
        {
            $imageClientPath     = $this->uploadClientPath . '/' . $image;
            $imageLogoPath     = $this->uploadLogoPath . '/' .  $image;

            if ( file_exists($imageClientPath) ) unlink($imageClientPath);
            if ( file_exists($imageLogoPath) ) unlink($imageLogoPath);
        }
    }

}
