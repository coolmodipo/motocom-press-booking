<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Form;

use App\Category;

class InsuranceController extends Controller
{
    public function index()
    {
        $insurances = Insurance::all();
        return view('admin.categories.lists', compact('insurances'));
    }

    public function create(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->name);

        Insurance::create($data);

        return redirect()->route('admin.categories')->with('success', 'That insurance has been created.');

    }

    public function getForm($id)
    {
        $insurance = Insurance::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit insurance</h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" name="insurance_id" value="'.$insurance->id.'">

                    <div class="form-group">
                            '.Form::label('Name', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::text('name', $insurance->name, ['class' => 'form-control', 'placeholder' => 'insurance Name', 'required']).'
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Edit insurance</button>
                </div>';

        return $html;
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->name);

        $insurance = Insurance::find($request->insurance_id);
        $insurance->update($data);

        return redirect()->route('admin.categories')->with('success', 'That insurance has been updated.');
    }

    public function getDeleteForm($id)
    {

        $insurance = Insurance::find($id);
        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>

                <input type="hidden" name="insurance_id" value="'.$insurance->id.'"/>

                <div class="modal-body">
                    <p>You are about to delete <strong>'.$insurance->name.'</strong>.</p>
                    <p>Do you want to proceed?</p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger btn-ok">Delete</button>
                </div>';

        return $html;
    }

    public function delete(Request $request)
    {
        $insurance = Insurance::find($request->insurance_id);
        $insurance->delete();

        return redirect()->route('admin.categories')->with('warning', 'That insurance has been deleted.');
    }
}
