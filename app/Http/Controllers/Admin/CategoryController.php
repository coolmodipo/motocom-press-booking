<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Form;

use App\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('admin.categories.lists', compact('categories'));
    }

    public function create(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->name);

        Category::create($data);

        return redirect()->route('admin.categories')->with('success', 'That category has been created.');

    }

    public function getForm($id)
    {
        $category = Category::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Category</h4>
                </div>
    
                <div class="modal-body">
                
                    <input type="hidden" name="category_id" value="'.$category->id.'">
    
                    <div class="form-group">
                            '.Form::label('Name', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::text('name', $category->name, ['class' => 'form-control', 'placeholder' => 'Category Name', 'required']).'
                        </div>
                    </div>
    
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Edit Category</button>
                </div>';

        return $html;
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->name);

        $category = Category::find($request->category_id);
        $category->update($data);

        return redirect()->route('admin.categories')->with('success', 'That category has been updated.');
    }

    public function getDeleteForm($id)
    {

        $category = Category::find($id);
        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                
                <input type="hidden" name="category_id" value="'.$category->id.'"/>
    
                <div class="modal-body">
                    <p>You are about to delete <strong>'.$category->name.'</strong>.</p>
                    <p>Do you want to proceed?</p>
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger btn-ok">Delete</button>
                </div>';

        return $html;
    }

    public function delete(Request $request)
    {
        $category = Category::find($request->category_id);
        $category->delete();

        return redirect()->route('admin.categories')->with('warning', 'That category has been deleted.');
    }
}
