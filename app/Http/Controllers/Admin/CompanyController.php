<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

use Auth;
use Form;
use Session;

use App\Client;
use App\Company;

class CompanyController extends Controller
{
    protected $uploadLogoPath;

    public function __construct()
    {
        parent::__construct();
        $this->uploadLogoPath = public_path('images/company');

    }

    /**
     * List all the users.
    */
    public function index()
    {
        $companies = Company::all();
        return view('admin.companies.lists', compact('companies'));
    }

    /**
     * Get company form in a modal.
    */
    public function getForm($id)
    {
        $company = Company::find($id);
        
        $html = '<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Company: <strong>'.$company->company_name.'</strong></h4>
            </div>

            <div class="modal-body">
            
                <input type="hidden" name="company_id" class="company_id" value="'.$company->id.'"/>

                <div class="row">                
                
                    <div class="col-md-4">
                    
                        <div class="box-body box-profile">
                        
                          <img class="profile-user-img img-responsive img-circle" src="'.$company->image_url.'" alt="'.$company->company_name.' picture">
            
                          <h3 class="profile-username text-center">'.$company->company_name.'</h3>
                          
                        </div>                 
                    
                    </div>
                    
                    <div class="col-md-8">
                      
                      
                          <div class="col-md-12">
                          
                                <div class="form-group">                        
                                    '.Form::label('logo').'
                                    '.Form::file('logo', ['class' => 'field']).'
                                </div>
                                
                                <div class="form-group">
                                    '.Form::label('Company Name').'
                                    '.Form::text('company_name', $company->company_name, ['class' => 'form-control']).'                
                                </div>
                                
                                <div class="form-group">
                                    '. Form::label('Admin') .'                                   
                                    '. Form::select('client_id', Client::join('users', 'clients.user_id', '=','users.id')->pluck('users.name', 'clients.id'), $company->admin->id, ['class' => 'form-control', 'placeholder' => 'Select Client', 'required']) .'                                   
                                </div>
                                
                                <div class="form-group">
                                    '.Form::label('Address Line 1').'
                                    
                                    '.Form::text('address_1', $company->address_1, ['class' => 'form-control']).'
                                   
                                </div>
                                
                                <div class="form-group">
                                    '.Form::label('Address Line 2').'
                                    '.Form::text('address_2', $company->address_2, ['class' => 'form-control']).'
                                </div>
                                
                                <div class="form-group">
                                    '.Form::label('City').'
                                    '.Form::text('city', $company->city, ['class' => 'form-control']).'
                                </div>
                                
                                <div class="form-group">
                                    '.Form::label('County').'
                                    '.Form::text('county', $company->county, ['class' => 'form-control']).'
                                </div>
                                
                                <div class="form-group">
                                    '.Form::label('Postcode').'
                                    '.Form::text('postcode', $company->postcode, ['class' => 'form-control']).'
                                </div>
                                
                          </div>
                          
                    </div>
                
               
                </div>  
              
              </div>
             

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update Company</button>
            </div>';

        return $html;
    }

    /**
     * Update company information from getForm() modal function.
    */
    public function update(Request $request)
    {
        //dd($request);
        $company = Company::find($request->company_id);
        $data = $this->handleRequest($request, $company);
        $data['slug'] = str_slug($request->company_name);
        $company->update($data);

        return redirect()->route('admin.companies')->with('success', 'That company has been updated.');
    }

    /**
     * Get delete warning form in a modal.
    */
    public function getDeleteForm($id)
    {
        $company = Company::find($id);
        
        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                
                <input type="hidden" name="company_id" value="'.$company->id.'"/>
    
                <div class="modal-body">
                    <p>You are about to delete <strong>'.$company->company_name.'</strong>.</p>
                    <p>Do you want to proceed?</p>
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger btn-ok">Delete</button>
                </div>';

        return $html;
    }

    /**
     * Delete company from getDeleteForm() modal function.
    */
    public function delete(Request $request)
    {
        //dd($request->all());
        $company = Company::find($request->company_id);
        $company->delete();

        return redirect()->route('admin.companies')->with('warning', 'That company has been deleted.');
    }

    /**
     * Get company users in a modal.
    */
    public function companyUsersForm($id)
    {
        $company = Company::find($id);

        $html = '<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">List of users: <strong>'.$company->company_name.' </strong></h4>
            </div>

            <div class="modal-body">';

                if($company->clients->count()){
                    $html .= '<ul class="list-group list-group-full">';
                    foreach ($company->clients  as $client){
                        $html .= '<li class="list-group-item">'.$client->firstname.' '.$client->lastname.'<span class="badge badge-default"><a class="white" href="'.route('admin.login.as', $client->user->id).'">login As</a></span></li>';
                    }
                    $html .= '</ul>';
                }
                else {
                    $html .= 'There are no users at present';
                }

        $html .= '</div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            </div>';

        return $html;
    }

    /**
     * Create a new company.
    */
    public function store(Request $request)
    {
        //dd($request->all());
        $data = $request->all();
        $data['slug'] = str_slug($request->company_name);
        $client = Client::find($request->client_id);
        $this->companyUpdate($client);
        $company = Company::create($data);

        $image = $this->handleRequest($request, $company);
        $company->update($image);

        $client->userCompany()->sync($company->id);

        return redirect()->route('admin.companies')->with('success', 'That company has been created.');
    }

    /**
     * Update the company information relation.
     */
    private function companyUpdate($client)
    {
        if($client->company){

            $client->companies()->update(['default' => 0]);
        }
    }

    /**
     * Request information for image.
     */
    private function handleRequest($request, $user)
    {
        $data = $request->all();


        if ($request->hasFile('logo'))
        {
            $this->removeImage($user->logo);
            $image       = $request->file('logo');
            $fileName    = $user->id.'-'.$image->getClientOriginalName();
            $destination = $this->uploadLogoPath;

            $successfullyUploaded = $image->move($destination, $fileName);

            if($successfullyUploaded){
                Image::make($destination.'/'.$fileName)->resize(135, 80)->save();
            }

            $data['logo'] = $fileName;
        }

        return $data;
    }

    /**
     * Remove images from the image directory.
     */
    private function removeImage($image)
    {
        if ( ! empty($image) )        {

            $imageLogoPath     = $this->uploadLogoPath . '/' .  $image;

            if ( file_exists($imageLogoPath) ) unlink($imageLogoPath);
        }
    }


}
