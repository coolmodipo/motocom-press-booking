<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Form;

use App\Colour;
use App\Company;
use App\Insurance;
use App\Permission;
use App\Role;
use App\Status;

class SettingsController extends Controller
{
    /**
     * Get all roles from the database.
    */
    public function roles()
    {
        $roles = Role::all();
        return view('admin.settings.roles', compact('roles'));
    }

    /**
     * Get all permissions from the database.
    */
    public function permissions()
    {
        $permissions = Permission::all();
        return view('admin.settings.permissions', compact('permissions'));
    }

    /**
     * Get all statuses from the database.
     */
    public function status()
    {
        $statuses = Status::all();
        return view('admin.settings.status', compact('statuses'));
    }

    /**
     * Get all colours from the database.
     */
    public function colours()
    {
        $colours = Colour::all();
        return view('admin.settings.colours', compact('colours'));
    }

    /**
     * Get all colours from the database.
     */
    public function insurances()
    {
        $insurances = Insurance::all();
        return view('admin.settings.insurances', compact('insurances'));
    }


    /**
     * Create a role.
    */
    public function roleCreate(Request $request)
    {
        $data = $request->all();

        $data['name'] = str_slug($request->display_name);

        Role::create($data);

        return redirect()->route('admin.settings.roles')->with('success', 'That role has been created.');

    }

    /**
     * Create a permission.
    */
    public function permissionCreate(Request $request)
    {
        $data = $request->all();

        $data['name'] = str_slug($request->display_name);

        Permission::create($data);

        return redirect()->route('admin.settings.permissions')->with('success', 'That permission has been created.');

    }

    /**
     * Create a colour.
     */
    public function colourCreate(Request $request)
    {
        $data = $request->all();

        $data['slug'] = str_slug($request->name);

        Colour::create($data);

        return redirect()->route('admin.settings.colours')->with('success', 'That colour has been created.');

    }

    /**
     * Create a status.
     */
    public function statusCreate(Request $request)
    {
       $data = $request->all();

        $data['slug'] = str_slug($request->title);

        Status::create($data);

        return redirect()->route('admin.settings.status')->with('success', 'That status has been created.');

    }

    /**
     * Create a insurance.
     */
    public function insuranceCreate(Request $request)
    {
        $data = $request->all();

        $data['slug'] = str_slug($request->name);

        Insurance::create($data);

        return redirect()->route('admin.settings.insurances')->with('success', 'That insurance has been created.');

    }
    /**
     * Create a role information in a modal.
    */
    public function getRoleForm($id)
    {
        $role = Role::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Role: '.$role->name.'</h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" name="role_id" class="role_id" value="'.$role->id.'"/>

                    <div class="form-group">
                        '.Form::label('Display Name', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::text('display_name', $role->display_name, ['class' => 'form-control', 'placeholder' => 'Role Name', 'required', ($role->name == 'super-administrator' || $role->name == 'administrator' ? 'disabled' : '')]).'
                        </div>
                    </div>

                    <div class="form-group">
                        '.Form::label('Description', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::textarea('description', $role->description, ['class' => 'form-control', 'placeholder' => 'Role Description', 'required']).'
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update Role</button>
                </div>';

        return $html;
    }

    /**
     * Update role from getRoleForm() modal function.
    */
    public function roleUpdate(Request $request)
    {
        $role = Role::find($request->role_id);
        $role->name = str_slug($request->display_name);
        $role->display_name = $request->display_name;
        $role->description = $request->description;
        $role->save();

        return redirect()->route('admin.settings.roles')->with('success', 'That role has been updated.');
    }

    /**
     * Create a permission information in a modal.
    */
    public function getPermissionForm($id)
    {
        $permission = Permission::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Permission: '.$permission->name.'</h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" name="permission_id" class="permission_id" value="'.$permission->id.'"/>

                    <div class="form-group">
                        '.Form::label('Display Name', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::text('display_name', $permission->display_name, ['class' => 'form-control', 'placeholder' => 'Permission Name', 'required']).'
                        </div>
                    </div>

                    <div class="form-group">
                        '.Form::label('Description', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::textarea('description', $permission->description, ['class' => 'form-control', 'placeholder' => 'Permission Description', 'required']).'
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update Permission</button>
                </div>';

        return $html;
    }

    /**
     * Update role from getPermissionForm() modal function.
    */
    public function permissionUpdate(Request $request)
    {

        $permission = Permission::find($request->permission_id);
        $permission->name = str_slug($request->display_name);
        $permission->display_name = $request->display_name;
        $permission->description = $request->description;
        $permission->save();

        return redirect()->route('admin.settings.permissions')->with('success', 'That permission has been updated.');
    }

    /**
     * Create a colour information in a modal.
     */
    public function getColourForm($id)
    {
        $colour = Colour::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Colour: '.$colour->name.'</h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" name="colour_id" class="colour_id" value="'.$colour->id.'"/>

                    <div class="form-group">
                        '.Form::label('Name', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::text('name', $colour->name, ['class' => 'form-control', 'placeholder' => 'Colour Name', 'required']).'
                        </div>
                    </div>

                    <div class="form-group">
                        '.Form::label('Hex Colour', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::text('hex', $colour->hex, ['class' => 'form-control', 'placeholder' => 'Colour Hex Code']).'
                        </div>
                    </div>
                    
                    <div class="form-group">
                        '.Form::label('Company', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::select('company_id', \App\Company::pluck('company_name', 'id'), $colour->company_id ? $colour->company_id : NULL, ['class' => 'form-control', 'placeholder' => 'Select Company', 'required']).'
                        </div>
                    </div>
                   
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update Colour</button>
                </div>';

        return $html;
    }

    /**
     * Update colour from getColourForm() modal function.
     */
    public function colourUpdate(Request $request)
    {
        $colour = Colour::find($request->colour_id);
        $colour->name = $request->name;
        $colour->slug = str_slug($request->name);
        $colour->hex = $request->hex;
        $colour->save();

        return redirect()->route('admin.settings.colours')->with('success', 'That colour has been updated.');
    }

    /**
     * Create a status information in a modal.
     */
    public function getStatusForm($id)
    {
        $status = Status::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Status: '.$status->title.'</h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" name="status_id" class="status_id" value="'.$status->id.'"/>

                    <div class="form-group">
                        '.Form::label('Title', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::text('title', $status->title, ['class' => 'form-control', 'placeholder' => 'Role Name', 'required']).'
                        </div>
                    </div>


                    <div class="form-group">
                        '.Form::label('Colour', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::select('class', ['success' => 'Green', 'info' => 'Blue', 'warning' => 'Orange', 'default' => 'Grey', 'danger' => 'Red'], $status->class, ['class' => 'form-control']).'
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update Status</button>
                </div>';

        return $html;
    }

    /**
     * Update role from getStatusForm() modal function.
     */
    public function statusUpdate(Request $request)
    {
        $status = Status::find($request->status_id);
        $status->title = $request->title;
        $status->slug = str_slug($request->title);
        $status->save();

        return redirect()->route('admin.settings.status')->with('success', 'That status has been updated.');
    }

    /**
     * Create a insurance information in a modal.
     */
    public function getInsuranceForm($id)
    {
        $insurance = Insurance::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Insurance: '.$insurance->name.'</h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" name="insurance_id" class="insurance_id" value="'.$insurance->id.'"/>

                    <div class="form-group">
                        '.Form::label('Name', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::text('name', $insurance->name, ['class' => 'form-control', 'placeholder' => 'Insurance Name', 'required']).'
                        </div>
                    </div>
                    
                    <div class="form-group">
                        '.Form::label('Name', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::select('company_id', Company::pluck('company_name', 'id'), $insurance->company_id, ['class' => 'form-control', 'placeholder' => 'Select Company', 'required']).'
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update Insurance</button>
                </div>';

        return $html;
    }

    /**
     * Update insurance from getInsuranceForm() modal function.
     */
    public function insuranceUpdate(Request $request)
    {
        $insurance = Insurance::find($request->insurance_id);
        $insurance->name = $request->name;
        $insurance->slug = str_slug($request->name);
        $insurance->company_id = $request->company_id;
        $insurance->save();

        return redirect()->route('admin.settings.insurances')->with('success', 'That insurance has been updated.');
    }

    /**
     * Get delete role form.
    */
    public function getRoleDeleteForm($id)
    {

        $role = Role::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>

                <input type="hidden" name="user_id" value="'.$role->id.'"/>

                <div class="modal-body">
                    <p>You are about to delete <strong>'.$role->name.'</strong>.</p>
                    <p>Do you want to proceed?</p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger btn-ok">Delete</button>
                </div>';

        return $html;

    }

    /**
     * Get delete permisssion form.
    */
    public function getPermissionDeleteForm($id)
    {
        $permission = Permission::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>

                <input type="hidden" name="user_id" value="'.$permission->id.'"/>

                <div class="modal-body">
                    <p>You are about to delete <strong>'.$permission->name.'</strong>.</p>
                    <p>Do you want to proceed?</p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger btn-ok">Delete</button>
                </div>';

        return $html;
    }

    /**
     * Get delete colour form.
     */
    public function getColourDeleteForm($id)
    {

        $colour = Colour::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>

                <input type="hidden" name="colour_id" value="'.$colour->id.'"/>

                <div class="modal-body">
                    <p>You are about to delete <strong>'.$colour->name.'</strong>.</p>
                    <p>Do you want to proceed?</p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger btn-ok">Delete</button>
                </div>';

        return $html;

    }

    /**
     * Get delete status form.
     */
    public function getStatusDeleteForm($id)
    {
        $status = Status::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>

                <input type="hidden" name="status_id" value="'.$status->id.'"/>

                <div class="modal-body">
                    <p>You are about to delete <strong>'.$status->title.'</strong>.</p>
                    <p>Do you want to proceed?</p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger btn-ok">Delete</button>
                </div>';

        return $html;
    }

    /**
     * Get delete insurance form.
     */
    public function getInsuranceDeleteForm($id)
    {

        $insurance = Insurance::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>

                <input type="hidden" name="insurance_id" value="'.$insurance->id.'"/>

                <div class="modal-body">
                    <p>You are about to delete <strong>'.$insurance->name.'</strong>.</p>
                    <p>Do you want to proceed?</p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger btn-ok">Delete</button>
                </div>';

        return $html;

    }

    /**
     * Delete role.
    */
    public function roleDelete(Request $request)
    {

        $role = Role::find($request->role_id);
        $role->delete();

        return redirect()->route('admin.settings.roles')->with('success', 'That role has been deleted.');

    }

    /**
     * Delete permission.
    */
    public function permissionDelete(Request $request)
    {
        $permission = Permission::find($request->permission_id);
        $permission->delete();

        return redirect()->route('admin.settings.permissions')->with('warning', 'That permission has been deleted.');
    }

    /**
     * Delete colour.
     */
    public function colourDelete(Request $request)
    {
        $colour = Colour::find($request->colour_id);
        $colour->delete();

        return redirect()->route('admin.settings.colours')->with('warning', 'That colour has been deleted.');
    }

    /**
     * Delete status.
     */
    public function statusDelete(Request $request)
    {
        $status = Status::find($request->status_id);
        $status->delete();

        return redirect()->route('admin.settings.status')->with('warning', 'That status has been deleted.');
    }
    /**
     * Delete role.
    */
    public function insuranceDelete(Request $request)
    {

        $insurance = Insurance::find($request->insurance_id);
        $insurance->delete();

        return redirect()->route('admin.settings.insurances')->with('warning', 'That insurance has been deleted.');

    }
}
