<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Form;

use App\Borrower;
use App\Company;

class BorrowerController extends Controller
{
    public function index()
    {
        $borrowers = Borrower::all();
        return view('admin.borrowers.lists', compact('borrowers'));
    }

    public function create(Request $request)
    {
        $data = $request->all();
        //$data['slug'] = str_slug($request->name);

        Borrower::create($data);

        return redirect()->route('admin.borrowers')->with('success', 'That borrower has been created.');
    }

    public function getForm($id)
    {
        $borrower = Borrower::find($id);

        $html = '<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Borrower: '.$borrower->name.'</h4>
            </div>

            <div class="modal-body">
            
                <input type="hidden" name="borrower_id" value="'.$borrower->id.'">

                <div class="form-group">
                    '.Form::label('Name', NULL, ['class' => 'col-md-4 control-label']).'
                    <div class="col-md-6">
                        '.Form::text('name', $borrower->name, ['class' => 'form-control', 'placeholder' => 'Name', 'required']).'
                    </div>
                </div>

                <div class="form-group">
                    '.Form::label('Organisation Name', NULL, ['class' => 'col-md-4 control-label']).'
                    <div class="col-md-6">
                        '.Form::text('organisation_name', $borrower->organisation_name, ['class' => 'form-control', 'placeholder' => 'Organisation Name', 'required']).'
                    </div>
                </div>

                <div class="form-group">
                    '.Form::label('Address', NULL, ['class' => 'col-md-4 control-label']).'
                    <div class="col-md-6">
                        '.Form::text('address', $borrower->address, ['class' => 'form-control', 'placeholder' => 'Organisation Address', 'required']).'
                    </div>
                </div>

                <div class="form-group">
                    '.Form::label('Postcode', NULL, ['class' => 'col-md-4 control-label']).'
                    <div class="col-md-6">
                        '.Form::text('postcode', $borrower->postcode, ['class' => 'form-control', 'placeholder' => 'Organisation Postcode', 'required']).'
                    </div>
                </div>

                <div class="form-group">
                    '.Form::label('Telephone', NULL, ['class' => 'col-md-4 control-label']).'
                    <div class="col-md-6">
                        '.Form::text('telephone', $borrower->telephone, ['class' => 'form-control', 'placeholder' => 'Organisation Telephone', 'required']).'
                    </div>
                </div>

                <div class="form-group">
                    '.Form::label('Email', NULL, ['class' => 'col-md-4 control-label']).'
                    <div class="col-md-6">
                        '.Form::text('email', $borrower->email, ['class' => 'form-control', 'placeholder' => 'Organisation Email', 'required']).'
                    </div>
                </div>

                <div class="form-group">
                    '.Form::label('Linked Company', NULL, ['class' => 'col-md-4 control-label']).'
                    <div class="col-md-6">
                        '.Form::select('company_id', Company::pluck('company_name', 'id') , $borrower->company_id, ['class' => 'form-control', 'placeholder' => 'Select Company', 'required']).'
                    </div>
                </div>

                <div class="form-group">
                    '.Form::label('Home Address', NULL, ['class' => 'col-md-4 control-label']).'
                    <div class="col-md-6">
                        '.Form::text('home_address', $borrower->home_address, ['class' => 'form-control', 'placeholder' => 'Home Address']).'
                    </div>
                </div>

                <div class="form-group">
                    '.Form::label('Home Postcode', NULL, ['class' => 'col-md-4 control-label']).'
                    <div class="col-md-6">
                        '.Form::text('home_postcode', $borrower->home_postcode, ['class' => 'form-control', 'placeholder' => 'Home Postcode']).'
                    </div>
                </div>

                <div class="form-group">
                    '.Form::label('Home Telephone', NULL, ['class' => 'col-md-4 control-label']).'
                    <div class="col-md-6">
                        '.Form::text('home_telephone', $borrower->home_telephone, ['class' => 'form-control', 'placeholder' => 'Home Telephone']).'
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update Borrower</button>
            </div>';

        return $html;
    }

    public function update(Request $request)
    {
        $data = $request->all();
        //$data['slug'] = str_slug($request->name);

        $borrower = Borrower::find($request->borrower_id);
        $borrower->update($data);

        return redirect()->route('admin.borrowers')->with('success', 'That borrower has been updated.');
    }

    public function getDeleteForm($id)
    {

        $borrower = Borrower::find($id);
        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                
                <input type="hidden" name="borrower_id" value="'.$borrower->id.'"/>
    
                <div class="modal-body">
                    <p>You are about to delete <strong>'.$borrower->name.'</strong>.</p>
                    <p>Do you want to proceed?</p>
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger btn-ok">Delete</button>
                </div>';

        return $html;
    }

    public function delete(Request $request)
    {
        $borrower = Borrower::find($request->borrower_id);
        $borrower->delete();

        return redirect()->route('admin.borrowers')->with('warning', 'That borrower has been deleted.');
    }
}
