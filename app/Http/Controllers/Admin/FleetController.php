<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;

use Form;

use App\Colour;
use App\Company;
use App\Fleet;
use App\Vehicle;

class FleetController extends Controller
{
    public function index()
    {
        $fleets = Fleet::all();
        return view('admin.fleets.lists', compact('fleets'));
    }

    public function company($slug)
    {
        $company = Company::findBySlug($slug);

        $fleets = Fleet::whereHas('vehicle', function($q) use ($company) {
            $q->whereHas('company',  function($q) use ($company){
                $q->where('company_id', '=', $company->id);
            });
        })->get();


        return view('admin.fleets.lists', compact('company','fleets'));
    }

    public function create(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->reg_number);

        Fleet::create($data);

        return redirect()->route('admin.fleets')->with('success', 'That fleet item has been created.');

    }

    public function getForm($id)
    {
        $fleet = Fleet::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Fleet</h4>
                </div>
    
                <div class="modal-body">
                
                    <input type="hidden" name="fleet_id" value="'.$fleet->id.'">
    
                    <div class="form-group">
                        '.Form::label('Reg Number:', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::text('reg_number', $fleet->reg_number, ['class' => 'form-control', 'placeholder' => 'Registration Number', 'required']).'
                        </div>
                    </div>
    
                    <div class="form-group">
                        '.Form::label('Vehicle:', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::select('vehicle_id', Vehicle::pluck('name', 'id'), $fleet->vehicle_id, ['class' => 'form-control', 'placeholder' => 'Select Vehicle', 'required']).'
                        </div>
                    </div>
    
                    <div class="form-group">
                        '.Form::label('Frame Number:', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::text('frame_number', $fleet->frame_number, ['class' => 'form-control', 'placeholder' => 'Frame Number', 'required']).'
                        </div>
                    </div>
    
                    <div class="form-group">
                        '.Form::label('Engine:', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::text('engine', $fleet->engine, ['class' => 'form-control', 'placeholder' => 'Engine Type', 'required']).'
                        </div>
                    </div>
    
                    
                    <div class="form-group">
                        '.Form::label('Colour:', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                            '.Form::select('colour_id', Colour::pluck('name', 'id') , $fleet->colour_id, ['class' => 'form-control', 'placeholder' => 'Select Colour', 'required']).'
                        </div>
                    </div>

                    <div class="form-group">
                        '.Form::label('Accessories:', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                        '.Form::textarea('accessories', $fleet->accessories, ['class' => 'form-control', 'placeholder' => 'Accessories']).'
                        </div>
                    </div>
    
                    <div class="form-group">
                        '.Form::label('Notes:', NULL, ['class' => 'col-md-4 control-label']).'
                        <div class="col-md-6">
                        '.Form::textarea('notes', $fleet->notes, ['class' => 'form-control', 'placeholder' => 'Additional Notes']).'
                        </div>
                    </div>
    
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update Fleet</button>
                </div>';

        return $html;
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->reg_number);
        $fleet = Fleet::find($request->fleet_id);
        $fleet->update($data);

        return redirect()->route('admin.fleets')->with('success', 'That fleet item has been updated.');
    }

    public function getDeleteForm($id)
    {

        $fleet = Fleet::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                
                <input type="hidden" name="fleet_id" value="'.$fleet->id.'"/>
    
                <div class="modal-body">
                    <p>You are about to delete <strong>'.$fleet->reg_number.'</strong>.</p>
                    <p>Do you want to proceed?</p>
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger btn-ok">Delete</button>
                </div>';

        return $html;
    }

    public function delete(Request $request)
    {
        $fleet = Fleet::find($request->fleet_id);
        $fleet->delete();

        return redirect()->route('admin.fleets')->with('warning', 'That fleet item has been deleted.');
    }

}
