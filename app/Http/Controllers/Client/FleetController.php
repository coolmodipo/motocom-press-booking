<?php

namespace App\Http\Controllers\Client;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Form;

use Mail;
use App\Mail\FleetBookMail;

use App\Booking;
use App\Bookingtype;
use App\Borrower;
use App\Colour;
use App\Fleet;
use App\Insurance;
use App\Status;
use App\Vehicle;

class FleetController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        if(!$user->client->company && !$user->client->userCompany){
            return redirect()->route('client.index')->with('warning', 'You are not linked to a company at present.');
        }

        if($user->client->company){
            $company = $user->client->company;
        }elseif($user->client->userCompany){
            $company = $user->client->userCompany->first();
        }

        $fleets = Fleet::whereHas('vehicle', function($q) use ($company) {
            $q->whereHas('company',  function($q) use ($company){
                $q->where('company_id', '=', $company->id);
            });
        })->get();

        //dd($fleets);


        return view('client.fleets', compact('company','fleets'));
    }

    public function show($slug)
    {
      $fleet = Fleet::findBySlug($slug);

      return view('client.fleet', compact('fleet'));
    }

    public function create(Request $request)
    {
        //dd($request);
        $data = $request->all();
        $data['slug'] = str_slug($request->reg_number);
        $data['accessories'] =  nl2br($request->accessories);
        $data['notes'] = nl2br($request->notes);
        $data['status_id'] = 1;

        Fleet::create($data);

        return redirect()->route('client.fleets')->with('success', 'That has been added for you.');
    }

    public function editForm($id)
    {
        $company = Auth::user()->client->company;
        $fleet = Fleet::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Edit Fleet : <strong>'.$fleet->reg_number.'</strong></h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" name="fleet_id" value="'.$fleet->id.'">

                    <div class="form-group">
                    '.Form::label('Reg Number:').'
                    '.Form::text('reg_number', $fleet->reg_number, ['class' => 'form-control', 'placeholder' => 'Registration Number', 'disabled']).'
                    </div>


                    <div class="form-group">
                        '.Form::label('Frame Number:').'
                        '.Form::text('frame_number', $fleet->frame_number, ['class' => 'form-control', 'placeholder' => 'Frame Number', 'required']).'
                    </div>

                    <div class="form-group">
                        '.Form::label('Engine:').'
                        '.Form::text('engine', $fleet->engine, ['class' => 'form-control', 'placeholder' => 'Engine Type', 'required']).'
                    </div>

                    <div class="form-group">
                        '.Form::label('Colour:').'
                        '.Form::select('colour_id', Colour::pluck('name', 'id') , $fleet->colour_id, ['class' => 'form-control', 'placeholder' => 'Select Colour', 'required']).'
                    </div>

                    <div class="form-group">
                        '.Form::label('Status:').'
                        '.Form::select('status_id', Status::pluck('title', 'id') , $fleet->status_id, ['class' => 'form-control', 'placeholder' => 'Select Status']).'
                    </div>

                    <div class="form-group">
                        '.Form::label('Accessories:').'
                        '.Form::textarea('accessories', strip_tags($fleet->accessories), ['class' => 'form-control', 'placeholder' => 'Accessories']).'
                    </div>

                    <div class="form-group">
                        '.Form::label('Notes:').'
                        '.Form::textarea('notes', strip_tags($fleet->notes), ['class' => 'form-control', 'placeholder' => 'Additional Notes']).'
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Update Fleet</button>
                </div>';

        return $html;
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->reg_number);
        $data['accessories'] =  nl2br($request->accessories);
        $data['notes'] = nl2br($request->notes);
        $fleet = Fleet::find($request->fleet_id);
        $fleet->update($data);

        return redirect()->route('client.fleets')->with('success', 'That has been updated for you.');
    }

    public function addAccessory(Request $request)
    {
        $fleet = Fleet::find($request->fleet_id);
        $data = [
            'accessories' =>  $fleet->accessories. PHP_EOL .'<br/>'.nl2br($request->accessories, true)
        ];

        $fleet->update($data);

        return redirect()->route('client.fleet.show', $fleet->slug)->with('success', 'That has been updated for you.');
    }

    public function addNote(Request $request)
    {
        $fleet = Fleet::find($request->fleet_id);
        $data = [
            'notes' =>  $fleet->notes. PHP_EOL .'<br/>'.nl2br(date('d/m/Y').' - '.$request->notes)
            ];

        $fleet->update($data);

        return redirect()->route('client.fleet.show', $fleet->slug)->with('success', 'That has been updated for you.');
    }

    public function updateAccessory(Request $request)
    {
        $fleet = Fleet::find($request->fleet_id);
        $data = [
            'accessories' =>  nl2br($request->accessories)
        ];

        $fleet->update($data);

        return redirect()->route('client.fleet.show', $fleet->slug)->with('success', 'That has been updated for you.');
    }

    public function updateNote(Request $request)
    {
        $fleet = Fleet::find($request->fleet_id);
        $data = [
            'notes' =>  nl2br($request->notes)
        ];

        $fleet->update($data);

        return redirect()->route('client.fleet.show', $fleet->slug)->with('success', 'That has been updated for you.');
    }

    public function bookingForm($id)
    {
        $user = Auth::user();
        $fleet = Fleet::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Booking Form : <strong>'.$fleet->reg_number.'</strong></h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" name="vehicle_id" value="'.$fleet->vehicle->id.'"/>
                    <input type="hidden" name="fleet_id" value="'.$fleet->id.'"/>

                    <div class="form-group">
                    '.Form::label('Name:').'
                    '.Form::select('borrower_id', Borrower::where(['company_id' => $user->client->userCompany->first()->id])->pluck('name', 'id') , null, ['class' => 'form-control', 'placeholder' => 'Select Borrower', 'required']).'
                    </div>

                    <div class="form-group">
                    '.Form::label('Booking Type:').'
                    '.Form::select('bookingtype_id', Bookingtype::pluck('name', 'id') , null, ['class' => 'form-control', 'placeholder' => 'Select Booking Type', 'required']).'
                    </div>

                    <div class="form-group">
                        '.Form::label('Start Date:').'
                        <div class="input-group">
                        '.Form::text('start_date', null, ['class' => 'form-control', 'id' => 'datepicker-autoclose-start-date', 'placeholder' => 'mm/dd/yyyy', 'required']).'
                        <span class="input-group-addon"><i class="icon-calender"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                        '.Form::label('End Date:').'
                        <div class="input-group">
                        '.Form::text('end_date', null, ['class' => 'form-control', 'id' => 'datepicker-autoclose-end-date', 'placeholder' => 'mm/dd/yyyy', 'required']).'
                        <span class="input-group-addon"><i class="icon-calender"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                    '.Form::label('Insurance:').'
                    '.Form::select('insurance_id', Insurance::where(['company_id' => $user->client->userCompany->first()->id])->orWhere(['id' => 1])->pluck('name', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Select Insurance']).'
                    </div>

                    <div class="form-group col-md-8">
                        '.Form::label('Collector:').'
                        '.Form::text('collector', null, ['class' => 'form-control', 'placeholder' => 'Person collecting', 'required']).'
                    </div>

                    <div class="form-group col-md-4">
                        '.Form::label('Pickup Time:').'
                        <div class="input-group clockpicker">
                        '.Form::text('collector_time', '09:30', ['class' => 'form-control', 'id' => 'single-input-start', 'placeholder' => 'HH:MM:SS', 'required']).'
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                        </div>
                    </div>

                    <div class="form-group col-md-8">
                        '.Form::label('Returnee:').'
                        '.Form::text('returnee', null, ['class' => 'form-control', 'placeholder' => 'Person returning', 'required']).'
                    </div>

                    <div class="form-group col-md-4">
                        '.Form::label('Return Time:').'
                        <div class="input-group clockpicker">
                        '.Form::text('returnee_time', '18:00', ['class' => 'form-control', 'id' => 'single-input-end', 'placeholder' => 'HH:MM:SS', 'required']).'
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                        </div>
                    </div>

                    <div class="form-group">
                        '.Form::label('Mileage:').'
                        '.Form::text('mileage_out', $fleet->mileage, ['class' => 'form-control', 'placeholder' => 'Item Miles', 'required']).'
                    </div>

                    <div class="form-group">
                        '.Form::label('Special Request:').'
                        '.Form::textarea('special_request', null, ['class' => 'form-control', 'placeholder' => 'Special Request']).'
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Book Fleet</button>
                </div>';

        return $html;
    }

    public function book(Request $request)
    {
        //dd($request->all());
        $model = Vehicle::find($request->vehicle_id);

        $booking = Booking::where(['fleet_id' => $request->fleet_id])
                            ->where('start_date', '<=', date("Y-m-d", strtotime(str_replace('/', '-', $request->start_date))) )
                            ->where('end_date', '>=', date("Y-m-d", strtotime(str_replace('/', '-', $request->start_date))) )
                            ->where(function ($q){
                                $q ->where('status_id', '=',  3)->orWhere('status_id', '=',  5);
                            })
                            ->first();
        if($booking){
            return redirect()->route('client.model.availability', $model->slug)->with('warning', 'You are unable to book that fleet because it has already been booked.');
        }

        $data = $request->all();

        $data['start_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $request->start_date)) );
        $data['end_date'] = date("Y-m-d", strtotime(str_replace('/', '-', $request->end_date)) );

        $fleet = Fleet::find($request->fleet_id);
        if(!$request->mileage_out){
            $data['mileage_out'] = $fleet->mileage_used;
        }

        Booking::create($data);

        $fleet->status_id = 3;
        $fleet->save();

        //Get borrower details.
        $borrow = Borrower::find($data['borrower_id']);
        //Constuct information for email.
        $mailData = [
          'name' => $borrow->name,
          'vehicle' => $model->name,
          'start_date' => $request->start_date,
          'end_date' => $request->end_date,
          'collector' => $data['collector'],
          'collector_time' => $data['collector_time'],
          'returnee' => $data['returnee'],
          'returnee_time' => $data['returnee_time'],
        ];
        //Send email to borrower about booking.
        Mail::to($borrow->email)->send(new FleetBookMail($mailData));

        return redirect()->route('client.model.availability', $model->slug)->with('success', 'That has been booked for you.');
    }

    public function collectedForm($id, $bid)
    {
        $fleet = Fleet::find($id);
        $booking = Booking::find($bid);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Collection for : <strong>'.$fleet->reg_number.'</strong></h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" name="booking_id" value="'.$booking->id.'"/>
                    <input type="hidden" name="vehicle_id" value="'.$fleet->vehicle->id.'"/>

                    <div class="form-group">
                        '.Form::label('Mileage Out:').'
                        '.Form::text('mileage_out', $fleet->mileage, ['class' => 'form-control', 'placeholder' => 'Item Miles', 'required']).'
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Mark as Collected</button>
                </div>';

        return $html;
    }

    public function collected(Request $request)
    {
        $model = Vehicle::find($request->vehicle_id);
        $data = $request->all();
        $data['status_id'] = 5;
        $booking = Booking::find($request->booking_id);
        $booking->update($data);

        return redirect()->route('client.model.availability', $model->slug)->with('success', 'That has been marked as collected.');
    }

    public function returnedForm($id, $bid)
    {
        $fleet = Fleet::find($id);
        $booking = Booking::find($bid);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Return for : <strong>'.$fleet->reg_number.'</strong></h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" name="fleet_id" value="'.$fleet->id.'"/>
                    <input type="hidden" name="booking_id" value="'.$booking->id.'"/>

                    <div class="form-group">
                        '.Form::label('Reurn Mileage:').'
                        '.Form::text('mileage_in', $fleet->mileage, ['class' => 'form-control', 'placeholder' => 'Item Miles', 'required']).'
                    </div>

                    <div class="form-group">
                        '.Form::label('Notes:').'
                        '.Form::textarea('notes', null, ['class' => 'form-control', 'placeholder' => 'Return Notes']).'
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Mark as Returned</button>
                </div>';

        return $html;
    }

    public function returned(Request $request)
    {

        $booking = Booking::find($request->booking_id);
        $booking->mileage_in = $request->mileage_in;
        $booking->mileage_used = $request->mileage_in - $booking->mileage_out;
        $booking->status_id = 6;
        $booking->save();

        $fleet = Fleet::find($request->fleet_id);
        $fleet->mileage = $request->mileage_in;
        if($request->notes){
            $fleet->notes = $fleet->notes. PHP_EOL .'<br/>'.date('d/m/Y').' - '.nl2br($request->notes);
        }
        $fleet->status_id = 2;
        $fleet->save();

        return redirect()->route('client.model.availability', $fleet->vehicle->slug)->with('success', 'That has been marked as collected.');
    }

    public function statusForm($id)
    {
        $fleet = Fleet::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Status for : <strong>'.$fleet->reg_number.'</strong></h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" name="fleet_id" value="'.$fleet->id.'"/>

                    <div class="form-group">
                        '.Form::label('Status:').'
                        '.Form::select('status_id', Status::where('id', '<', 5)->where('id', '!=', 3)->pluck('title', 'id') , $fleet->status_id, ['class' => 'form-control', 'placeholder' => 'Select Status']).'
                    </div>

                    <div class="form-group">
                        '.Form::label('Notes:').'
                        '.Form::textarea('notes', null, ['class' => 'form-control', 'placeholder' => 'Return Notes']).'
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Update Status</button>
                </div>';

        return $html;
    }

    public function updateStatus(Request $request)
    {
        $fleet = Fleet::find($request->fleet_id);
        if($request->notes){
            $fleet->notes = $fleet->notes. PHP_EOL .'<br/>'.date('d/m/Y').' - '.nl2br($request->notes);
        }
        $fleet->status_id = $request->status_id;
        $fleet->save();

        return redirect()->route('client.model.availability', $fleet->vehicle->slug)->with('success', 'That has been updated for you.');
    }

    public function returnForm($id)
    {
        $fleet = Fleet::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Return Form : <strong>'.$fleet->reg_number.'</strong></h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" name="fleet_id" value="'.$fleet->id.'"/>

                    <div class="form-group">
                        '.Form::label('Return Mileage:').'
                        '.Form::text('mileage_in', null, ['class' => 'form-control', 'placeholder' => 'Return Mileage', 'required']).'
                    </div>

                    <div class="form-group">
                        '.Form::label('Notes:').'
                        '.Form::textarea('notes', null, ['class' => 'form-control', 'placeholder' => 'Return Notes']).'
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light">Return Fleet</button>
                </div>';

        return $html;
    }

    public function returnItem(Request $request)
    {
        $booking = Booking::where(['fleet_id' => $request->fleet_id])->whereNull('mileage_used')->first();
        $booking->mileage_in = $request->mileage_in;
        $booking->mileage_used = $request->mileage_in-$booking->mileage_out;
        $booking->save();

        $fleet = Fleet::find($request->fleet_id);
        $fleet->mileage = $request->mileage_in;
        if($request->notes){
            $fleet->notes = $fleet->notes. PHP_EOL .'<br/>'.date('d/m/Y').' - '.nl2br($request->notes);
        }
        $fleet->status_id = 2;
        $fleet->save();

        return redirect()->route('client.fleets')->with('success', 'That has been returned.');
    }

    public function availableForm($id)
    {
        $fleet = Fleet::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Service Form : <strong>'.$fleet->reg_number.'</strong></h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" name="fleet_id" value="'.$fleet->id.'"/>

                    <div class="form-group">
                        '.Form::label('Notes:').'
                        '.Form::textarea('notes', null, ['class' => 'form-control', 'placeholder' => 'Service Notes']).'
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light">Make Available</button>
                </div>';

        return $html;
    }

    public function makeAvailable(Request $request)
    {
        $fleet = Fleet::find($request->fleet_id);
        if($request->notes){
            $fleet->notes = $fleet->notes . '<br/>' . nl2br(date('d/m/Y') . ' - ' . $request->notes);
        }
        $fleet->status_id = 1;
        $fleet->save();

        return redirect()->route('client.fleets')->with('success', 'That item is available.');
    }

    public function informationForm($id)
    {
        $fleet = Fleet::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Information Re: <strong>'.$fleet->reg_number.'</strong></h4>
                </div>

                <div class="modal-body">';

                    if($fleet->accessories){
                        $html .= '<div class="form-group">
                                    '.Form::label('Accessories:').'
                                    '.Form::textarea('accessories', $fleet->accessories, ['class' => 'form-control', 'placeholder' => 'Accessories', 'disabled']).'
                                </div>';
                    }

                    if($fleet->notes){
                        $html .= '<div class="form-group">
                                    '.Form::label('Notes:').'
                                    '.Form::textarea('notes', $fleet->notes, ['class' => 'form-control', 'placeholder' => 'Additional Notes', 'disabled']).'
                                </div>';
                    }

        $html .= '</div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                </div>';

        return $html;
    }

    public function delete($id)
    {
        $fleet = Fleet::find($id);
        $fleet->delete();

    }
}
