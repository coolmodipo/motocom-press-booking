<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateClientProfile;
use Intervention\Image\Facades\Image;

use Auth;
use Session;

use App\Booking;
use App\Client;
use App\ClientCompany;
use App\Company;
use App\Fleet;
use App\Notification;
use App\User;

class HomeController extends Controller
{
    protected $uploadClientPath;
    protected $uploadLogoPath;

    public function __construct()
    {
        parent::__construct();
        $this->uploadClientPath = public_path('images/clients');
        $this->uploadLogoPath = public_path('images/company');

    }

    /**
     * Clients dashboard/ Frontpage.
    */
    public function index()
    {
        //Get the bookings for each month - Needs redoing!!!
        $janBookings = Booking::whereBetween('start_date', array('2018-01-01', '2018-01-31'))->count();
        $febBookings = Booking::whereBetween('start_date', array('2018-02-01', '2018-02-31'))->count();
        $marBookings = Booking::whereBetween('start_date', array('2018-03-01', '2018-03-31'))->count();
        $aprBookings = Booking::whereBetween('start_date', array('2018-04-01', '2018-04-31'))->count();
        $mayBookings = Booking::whereBetween('start_date', array('2018-05-01', '2018-05-31'))->count();
        $junBookings = Booking::whereBetween('start_date', array('2018-06-01', '2018-06-31'))->count();
        $julBookings = Booking::whereBetween('start_date', array('2018-07-01', '2018-07-31'))->count();
        $augBookings = Booking::whereBetween('start_date', array('2018-08-01', '2018-08-31'))->count();
        $sepBookings = Booking::whereBetween('start_date', array('2018-09-01', '2018-09-31'))->count();
        $octBookings = Booking::whereBetween('start_date', array('2018-10-01', '2018-10-31'))->count();
        $novBookings = Booking::whereBetween('start_date', array('2018-11-01', '2018-11-31'))->count();
        $decBookings = Booking::whereBetween('start_date', array('2018-12-01', '2018-12-31'))->count();
        //Add the data into an array as milliseconds and booking count.
        $chartBookings = array(
          strtotime('01/01/2018') * 1000 => $janBookings,
          strtotime('02/01/2018') * 1000 => $febBookings,
          strtotime('03/01/2018') * 1000 => $marBookings,
          strtotime('04/01/2018') * 1000 => $aprBookings,
          strtotime('05/01/2018') * 1000 => $mayBookings,
          strtotime('06/01/2018') * 1000 => $junBookings,
          strtotime('07/01/2018') * 1000 => $julBookings,
          strtotime('08/01/2018') * 1000 => $augBookings,
          strtotime('09/01/2018') * 1000 => $sepBookings,
          strtotime('10/01/2018') * 1000 => $octBookings,
          strtotime('11/01/2018') * 1000 => $novBookings,
          strtotime('12/01/2018') * 1000 => $decBookings
        );

        $user = Auth::user();
        $bookings = Booking::all();

        if($user->client->company){
            $company = $user->client->company;
            $borrowers = $company->borrowers;
            $models = $user->client->company->vehicles;
            $fleets = $this->fleetsInfo($company);
            $bookingsInfo = $this->bookingsInfo($company);
        }
        else{
            $company = $user->client->userCompany->first();
            $borrowers = $company->borrowers;
            $models = $user->client->userCompany->first()->vehicles;
            $fleets = $this->fleetsInfo($company);
            $bookingsInfo = $this->bookingsInfo($company);
        }

        $bookingTypes = array();
        foreach ($bookings as $booking){
            if($booking->borrower->company->id == $company->id){
                $bookingTypes[] = $booking->bookingtype;
            }
        }
        $bookingTypes = array_unique($bookingTypes);

        return view('client.index', compact('bookings','bookingsInfo', 'bookingTypes', 'borrowers', 'chartBookings', 'company', 'fleets', 'models'));
    }

    /**
     * Clients profile page.
    */
    public function profile()
    {
        //dd('THIS IS THE CLIENT PAGE!');
        return view('client.profile');
    }

    /**
     * Update client profile.
    */
    public function updateProfile(UpdateClientProfile $request)
    {
        //dd($request);
        $user = User::find($request->user_id);
        $data = $this->handleRequest($request, $user);

        $data['name'] = $request->firstname.' '.$request->lastname;
        $user->update($data);
        $user->client->update($data);

        return redirect()->route('client.profile')->with('success', 'That has been updated for you.');
    }

    /**
     * Add company to clients profile - ADMIN.
    */
    public function addCompany(Request $request)
    {
        $user = User::find($request->user_id);
        $this->companyUpdate($user);

        $data = $request->all();
        $data['client_id'] = $user->client->id;
        $data['slug'] = str_slug($request->company_name);
        $data['default'] = 1;
        $company = Company::create($data);

        $this->clientCompanyRelation($user->client->id, $company->id);

        return redirect()->route('client.profile')->with('success', 'Your company has been added.');
    }

    /**
     * Get company form in a modal.
    */
    public function addUserCompanyForm($id)
    {
        $company = Company::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Add User to Company: <strong>'.$company->company_name.'</strong></h4>
                </div>

                <div class="modal-body">

                <input type="hidden" name="company_id" value="'.$company->id.'">

                <div class="form-group">
                    <label for="recipient-name" class="control-label">Firstname:</label>
                    <input type="text" class="form-control" name="firstname" required>
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="control-label">Surname:</label>
                    <input type="text" class="form-control" name="lastname" required>
                </div>

                <div class="form-group">
                    <label for="recipient-name" class="control-label">Email:</label>
                    <input type="email" class="form-control" name="email" required>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Add User</button>
            </div>';

        return $html;
    }

    /**
     * Create a new company from addUserCompany() function modal.
    */
    public function addUserCompany(Request $request)
    {
        //dd($request->all());
        $data = $request->all();
        $data['name'] = $request->firstname.' '.$request->lastname;
        $password = str_random(6);
        //CREATE A NEW USER
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($password),
        ]);

        //CREATE A NEW ClIENT
        $client = Client::create([
            'firstname'     => $data['firstname'],
            'lastname'      => $data['lastname'],
            'user_id'       => $user->id,
        ]);

        $this->clientCompanyRelation($client->id, $request->company_id);
        $this->clientNotification($client->id);

        //GIVE USER A ROLE
        $user->roles()->attach(3);

        return redirect()->route('client.profile')->with('success', 'That user has been created.');
    }

    /**
     * Make company default for the client.
    */
    public function makeDefaultCompany($uid, $id)
    {
        $user = User::find($uid);
        $this->companyUpdate($user);

        $company = Company::find($id);
        $company->default= 1;
        $company->save();

    }

    /**
     * Delete company.
     */
    public function deleteCompany($id)
    {
        $company = Company::find($id);
        $this->removeImage($company->logo);
        $company->delete();

    }

    /**
     * Update the company information relation.
    */
    private function companyUpdate($user)
    {
        if($user->client->company){

            $user->client->companies()->update(['default' => 0]);
        }
    }

    /**
     * Request information for image.
    */
    private function handleRequest($request, $user)
    {
        $data = $request->all();
        //dd($data);
        if ($request->hasFile('image'))
        {
            $this->removeImage($user->client->image);
            $image       = $request->file('image');
            $fileName    = $user->id.'-'.$image->getClientOriginalName();
            $destination = $this->uploadClientPath;

            $successfullyUploaded = $image->move($destination, $fileName);

            if($successfullyUploaded){
                Image::make($destination.'/'.$fileName)->resize(300, 200);
            }

            $data['image'] = $fileName;
        }

        if($user->client->companies){

          $companies = $user->client->companies->pluck('id')->toArray();

          foreach($companies as $id){

              if ($request->hasFile('logo_'.$id))
              {
                  $company = Company::find($id);

                  $this->removeImage($company->logo);

                  $image       = $request->file('logo_'.$id);
                  $fileName    = $id.'-'.$image->getClientOriginalName();
                  $destination = $this->uploadLogoPath;

                  $successfullyUploaded = $image->move($destination, $fileName);

                  if($successfullyUploaded){
                      Image::make($destination.'/'.$fileName)->resize(300, 200);
                  }

                  $company->logo = $fileName;
                  $company->save();
              }

          }

        }

        return $data;
    }

    /**
     * Remove images from the image directory.
    */
    private function removeImage($image)
    {
        if ( ! empty($image) )
        {
            $imageClientPath     = $this->uploadClientPath . '/' . $image;
            $imageLogoPath     = $this->uploadLogoPath . '/' .  $image;

            if ( file_exists($imageClientPath) ) unlink($imageClientPath);
            if ( file_exists($imageLogoPath) ) unlink($imageLogoPath);
        }
    }

    /**
     * Create a client company relation.
    */
    private function clientCompanyRelation($client, $company)
    {
        //CREATE A NEW CLIENT-COMPANY RELATION
        ClientCompany::create([
            'client_id'       => $client,
            'company_id'      => $company,
        ]);

    }

    /**
     * Create a client notification.
    */
    private function clientNotification($client)
    {
        Notification::create([
            'client_id'       => $client,
        ]);
    }

    private function fleetsInfo($company)
    {
        $fleets = Fleet::whereHas('vehicle', function($q) use ($company) {
            $q->whereHas('company',  function($q) use ($company){
                $q->where('company_id', '=', $company->id);
            });
        })->get();

        return $fleets;
    }

    private function bookingsInfo($company)
    {
        $bookings = Booking::whereHas('borrower', function($q) use ($company) {
            //$q->whereHas('company',  function($q) use ($company){
                $q->where('company_id', '=', $company->id);
            //});
        })->get();

        return $bookings;

    }

}
