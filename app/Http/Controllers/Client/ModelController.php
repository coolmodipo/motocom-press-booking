<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

use Auth;
use Form;

use App\Booking;
use App\Category;
use App\Colour;
use App\Vehicle;

class ModelController extends Controller
{
    protected $uploadPath;

    public function __construct()
    {
        parent::__construct();
        $this->uploadPath = public_path('images/vehicles');

    }

    public function index()
    {
        $user = Auth::user();

        if (!$user->client->company && !$user->client->userCompany) {
            return redirect()->route('client.index')->with('warning', 'You are not linked to a company at present.');
        }

        if ($user->client->company) {
            $company = $user->client->company;
            $models = $company->vehicles;

        } elseif ($user->client->userCompany) {
            $company = $user->client->userCompany->first();
            $models = $company->vehicles;
        }

        return view('client.models', compact('company', 'models'));

    }

    public function create(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->name);

        $vehicle = Vehicle::create($data);

        $image = $this->handleRequest($request, $vehicle);

        if ($image) {
            $vehicle->update($image);
        }

        return redirect()->route('client.models')->with('success', 'That Model has been created.');

    }

    public function getForm($id)
    {
        $vehicle = Vehicle::find($id);

        $html = '<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Model</h4>
            </div>

            <div class="modal-body">
            
                <input type="hidden" name="vehicle_id" value="' . $vehicle->id . '">

                <div class="form-group">
                    ' . Form::label('image') . '
                    <img src="' . $vehicle->image_url . '" alt="' . $vehicle->name . '"> 
                </div>


                <div class="form-group">
                    ' . Form::label('image') . '
                    ' . Form::file('image', ['class' => 'field']) . '
                    <small>350px X 350px</small>
                </div>

                <div class="form-group">
                    ' . Form::label('Name') . '
                    ' . Form::text('name', $vehicle->name, ['class' => 'form-control', 'placeholder' => 'Vehicle Name', 'required']) . '
                </div>

                <div class="form-group">
                    ' . Form::label('Category') . '
                    ' . Form::select('category_id', Category::pluck('name', 'id'), $vehicle->category->id, ['class' => 'form-control', 'placeholder' => 'Select Vehicle', 'required']) . '
                 </div>
                 
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update Model</button>
            </div>';

        return $html;
    }

    public function update(Request $request)
    {
        //dd($request);

        $vehicle = Vehicle::find($request->vehicle_id);

        $data = $this->handleRequest($request, $vehicle);
        $data['slug'] = str_slug($request->name);

        $vehicle->update($data);

        return redirect()->route('client.models')->with('success', 'That Vehicle has been updated.');
    }

    public function getDeleteForm($id)
    {

        $vehicle = Vehicle::find($id);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Delete</h4>
                </div>
                
                <input type="hidden" name="vehicle_id" value="' . $vehicle->id . '"/>
    
                <div class="modal-body">
                    <p>You are about to delete <strong>' . $vehicle->name . '</strong>.</p>
                    <p>Do you want to proceed?</p>
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger btn-ok">Delete</button>
                </div>';

        return $html;
    }

    public function delete(Request $request)
    {
        $vehicle = Vehicle::find($request->vehicle_id);
        $this->removeImage($vehicle->image);
        $vehicle->delete();

        return redirect()->route('admin.vehicles')->with('warning', 'That Vehicle has been deleted.');
    }

    public function getFleetForm($mid)
    {
        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Create New Fleet</h4>
                </div>
    
                <div class="modal-body">
    
                    <div class="form-group">
                        ' . Form::label('Reg Number:') . '
                        ' . Form::text('reg_number', null, ['class' => 'form-control', 'placeholder' => 'Registration Number', 'required']) . '
                    </div>
    
                    <input type="hidden" name="vehicle_id" value="' . $mid . '">
    
                    <div class="form-group">
                        ' . Form::label('Frame Number:') . '
                        ' . Form::text('frame_number', null, ['class' => 'form-control', 'placeholder' => 'Frame Number', 'required']) . '
                    </div>
    
                    <div class="form-group">
                        ' . Form::label('Engine:') . '
                        ' . Form::text('engine', null, ['class' => 'form-control', 'placeholder' => 'Engine Type', 'required']) . '
                    </div>
    
                    <div class="form-group">
                        ' . Form::label('Colour:') . '
                        ' . Form::select('colour_id', Colour::pluck('name', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Select Colour', 'required']) . '
                    </div>
    
                    <div class="form-group">
                        ' . Form::label('Accessories:') . '
                        ' . Form::textarea('accessories', null, ['class' => 'form-control', 'placeholder' => 'Accessories']) . '
                    </div>
    
                    <div class="form-group">
                        ' . Form::label('Notes:') . '
                        ' . Form::textarea('notes', null, ['class' => 'form-control', 'placeholder' => 'Additional Notes']) . '
                    </div>
    
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Create Fleet</button>
                </div>';

        return $html;
    }

    public function getAvailabilityForm($id)
    {
        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Check Model Availability</h4>
                </div>
    
                <div class="modal-body">
    
                    <div id="calendarAvailability"></div>
    
                </div>
    
                <div class="modal-footer">
                    <div class="pull-left">
                        Available | Booked | Service | Faulty
                    </div>
    
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                </div>';

        return $html;
    }

    public function availability($slug)
    {

        $model = Vehicle::findBySlug($slug);
        $bookings = Booking::where(['status_id' => 3])->orWhere(['status_id' => 5])->get();
        $fleets = $model->fleets->pluck('reg_number')->toArray();
        //dd($fleets);
        $unavailable = $model->unavailableFleets->pluck('id')->toArray();
        $faulty = $model->faultyFleets->pluck('id')->toArray();

        return view('client.fleet-availability', compact('bookings', 'faulty', 'fleets', 'model', 'unavailable'));

    }

    /**
     * Request information for image.
     */
    private function handleRequest($request, $vehicle)
    {
        $data = $request->all();

        if ($request->hasFile('image'))
        {
            $this->removeImage($vehicle->image);
            $image       = $request->file('image');
            $fileName    = $vehicle->id.'-'.$image->getClientOriginalName();
            $destination = $this->uploadPath;

            $successfullyUploaded = $image->move($destination, $fileName);

            if($successfullyUploaded){
                Image::make($destination.'/'.$fileName)->resize(269, 160)->save();
            }

            $data['image'] = $fileName;
        }

        return $data;
    }

    /**
     * Remove images from the image directory.
     */
    private function removeImage($image)
    {
        if ( ! empty($image) )
        {
            $imageClientPath     = $this->uploadPath . '/' . $image;

            if ( file_exists($imageClientPath) ) unlink($imageClientPath);
        }
    }
}
