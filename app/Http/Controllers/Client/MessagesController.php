<?php

namespace App\Http\Controllers\Client;

use App\MessageReply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Form;
use Auth;

use App\Client;
use App\Message;
use App\MessageType;

class MessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = 'Inbox')
    {

        $user = Auth::user();

        if($user->client->userCompany->count() < 1){
            return redirect()->route('client.index');
        }

        $messageType = MessageType::where(['type' => $type])->first();

        if(!$messageType){
            return abort(404);
        }

        $messages = Message::where(['type_id' => $messageType->id])->where(['client_id' => $user->client->id])->orderBy('created_at', 'desc')->get();

        return view('client.messages', compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);

        if(Message::count()){
            $id = Message::orderBy('id', 'desc')->first()->id+1;
        }
        else{
            $id = 1;
        }

        $data = $request->all();
        $data['parent_id'] =  $id;
        $data['slug'] = $id.'-'.str_slug($request->title);

        $message = Message::create($data);

        MessageReply::create(
            [
                'message_id' => $message->id,
                'reply_id' => $id,
            ]
        );

        return redirect()->route('client.messages', 'inbox')->with('success', 'Your message has been sent.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $message = Message::findBySlug($slug);
        $message->read = 1;
        $message->update();

        return view('client.message', compact('message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $message = Message::find($id);
        $message->type_id = 3;
        $message->save();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $message = Message::find($id);
        $message->type_id = 1;
        $message->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function read($id)
    {
        $message = Message::find($id);
        $message->read = 1;
        $message->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unread($id)
    {
        $message = Message::find($id);
        $message->read = 0;
        $message->type_id = 1;
        $message->save();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function trash($id)
    {
        $message = Message::find($id);
        $message->delete();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getReplyForm($id)
    {
        $user = Auth::user();
        $message = Message::find($id);

        //dd($message->parentID);

        $html = '<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Reply to : '.$message->title.'</h4>
                </div>

                <div class="modal-body">';

                    if($message->parentID->first()->children->count() > 1){

                        $html .= '<h4>Messages:</h4>
                                       <div style="overflow-y: scroll; height:250px;">';
                                        foreach ($message->parentID->first()->children as $key => $child) {
                                                if($key+1  < count($message->parentID->first()->children)) {
                                                    $html .= '<div class="media col-md-10 ' . ($user->client->id != $child->client_id ? 'col-md-offset-2' : '') . '">
                                                        <div class="media-body">
                                                        <h4 class="media-heading">' . $child->title . '</h4>
                                                        <small>' . ($user->client->id != $child->client_id ? 'you' : $child->sender->user->name) . '</small>
                                                        <br/>
                                                        ' . $child->message . '
                                                        </div>
                                                    </div>';
                                                }
                                        }

                        $html .= '</div>';

                    }

                    $html .= '<div class="media col-md-12">
                                                        <div class="media-body">
                                                        <h4 class="media-heading">'.$message->parentID->first()->title.'</h4>
                                                        <small>Original Message</small>
                                                        <br/>
                                                        '.$message->parentID->first()->message.'
                                                        </div>
                                                    </div>';

                    $html .= '<input type="hidden" name="parent_id" value="'.$message->parentID->first()->id.'"/>
                                <input type="hidden" name="client_id" value="'.$message->sender_id.'">
                              <input type="hidden" name="sender_id" value="'.$user->client->id.'">
                                <div class="form-group">
                                    '.Form::label('Title:').'
                                    '.Form::text('title', 'Re: '.$message->title, ['class' => 'form-control', 'placeholder' => 'Message Title']).'
                                </div>

                                <div class="form-group">
                                    '.Form::label('Message Body:').'
                                    '.Form::textarea('message', NULL, ['class' => 'form-control', 'placeholder' => 'Message Body', 'required']).'
                                </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Send Mail</button>
                </div>';

        return $html;
    }

    public function reply(Request $request)
    {
        $id = Message::orderBy('id', 'desc')->first()->id+1;
        $data = $request->all();

        $data['slug'] = str_slug($id.'-'.$request->title);
        $message = Message::create($data);

        MessageReply::create(
            [
               'message_id' => $message->id,
               'reply_id' => $request->parent_id,
            ]
        );

        return redirect()->route('client.messages', 'inbox')->with('success', 'Your message has been sent.');
    }
}
