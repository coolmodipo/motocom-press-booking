<?php

namespace App\Http\Controllers\Client;

use App\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;

class NotificationController extends Controller
{
    /**
     * Update notification from settings.
    */
    public function update($cid, $type)
    {
        $notification = Notification::where(['client_id' => $cid])->first();
        $notification->{$type} = $notification->{$type} == 1 ? 0 : 1;
        $notification->save();
    }
}
