<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Form;

use App\Borrower;

class BorrowerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($cid)
    {
        //Check to see if current user can access the page.
        if(Auth::user()->client->company->id != $cid){
            return redirect()->route('client.index')->with('warning', 'You do not have the right credentials to view that page.');
        }

        //Get all borrowers linked to the company.
        $borrowers = Borrower::where(['company_id' => $cid])->get();

        //Return the information to the browser.
        return view('client.borrowers', compact('borrowers'));
    }

    public function editForm($bid)
    {
        $borrower = Borrower::find($bid);

        $html = '<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Borrower: '.$borrower->name.'</h4>
                </div>
    
                <div class="modal-body">
                
                    <input type="hidden" name="borrower_id" value="'.$borrower->id.'">                
                    <input type="hidden" name="company_id" value="'.$borrower->company_id.'">                
                    
                   <div class="form-group">
                        '.Form::label('Name', NULL, ['class' => 'col-md-4 control-label']).'
                        '.Form::text('name', $borrower->name, ['class' => 'form-control', 'placeholder' => 'Name', 'required']).'                   
                    </div>
    
                    <div class="form-group">
                        '.Form::label('Organisation Name', NULL, ['class' => 'col-md-4 control-label']).'
                        '.Form::text('organisation_name', $borrower->organisation_name, ['class' => 'form-control', 'placeholder' => 'Organisation Name', 'required']).'                   
                    </div>
    
                    <div class="form-group">
                        '.Form::label('Address', NULL, ['class' => 'col-md-4 control-label']).'
                        '.Form::text('address', $borrower->address, ['class' => 'form-control', 'placeholder' => 'Organisation Address', 'required']).'                    
                    </div>
    
                    <div class="form-group">
                        '.Form::label('Postcode', NULL, ['class' => 'col-md-4 control-label']).'
                        '.Form::text('postcode', $borrower->postcode, ['class' => 'form-control', 'placeholder' => 'Organisation Postcode', 'required']).'
                    </div>
    
                    <div class="form-group">
                        '.Form::label('Telephone', NULL, ['class' => 'col-md-4 control-label']).'
                        '.Form::text('telephone', $borrower->telephone, ['class' => 'form-control', 'placeholder' => 'Organisation Telephone', 'required']).'
                    </div>
    
                    <div class="form-group">
                        '.Form::label('Email', NULL, ['class' => 'col-md-4 control-label']).'
                        '.Form::text('email', $borrower->email, ['class' => 'form-control', 'placeholder' => 'Organisation Email', 'required']).'
                    </div>    
    
                    <div class="form-group">
                        '.Form::label('Home Address', NULL, ['class' => 'col-md-4 control-label']).'
                        '.Form::text('home_address', $borrower->home_address, ['class' => 'form-control', 'placeholder' => 'Home Address']).'
                    </div>
    
                    <div class="form-group">
                        '.Form::label('Home Postcode', NULL, ['class' => 'col-md-4 control-label']).'
                        '.Form::text('home_postcode', $borrower->home_postcode, ['class' => 'form-control', 'placeholder' => 'Home Postcode']).'
                    </div>
    
                    <div class="form-group">
                        '.Form::label('Home Telephone', NULL, ['class' => 'col-md-4 control-label']).'
                        '.Form::text('home_telephone', $borrower->home_telephone, ['class' => 'form-control', 'placeholder' => 'Home Telephone']).'
                    </div>
    
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update Borrower</button>
                </div>';

        return $html;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $data = $request->all();

        Borrower::create($data);

        return redirect()->route('client.borrowers.index', $request->company_id)->with('success', 'That borrower has been created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();

        $borrower = Borrower::find($request->borrower_id);
        $borrower->update($data);

        return redirect()->route('client.borrowers.index', $request->company_id)->with('success', 'That borrower has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
