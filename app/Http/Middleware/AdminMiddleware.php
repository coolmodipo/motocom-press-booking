<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user()){

            return redirect()->route('login');

        }

        if( !Auth::user()->hasRole('super-administrator') && !Auth::user()->hasRole('administrator') ){

            return redirect()->route('login');

        }



        return $next($request);
    }
}
