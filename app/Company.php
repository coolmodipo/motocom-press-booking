<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'client_id','company_name','address_1','address_2','city','county','postcode','logo', 'default', 'slug'
    ];

    public function admin()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function clients()
    {
        return $this->belongsToMany(Client::class, 'client_company', 'company_id', 'client_id');
    }

    public function borrowers()
    {
        return $this->hasMany(Borrower::class, 'company_id');
    }

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class, 'company_id');
    }

    public function fleets()
    {
        return $this->hasMany(Vehicle::class, 'company_id');
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class, 'company_id');
    }

    public function getImageUrlAttribute($value) {

        $imageUrl = "";

        if (!is_null($this->logo) && $this->logo != '') {
            $imagePath = public_path() . "/images/company/" . $this->logo;
            if (file_exists($imagePath)) {
                $imageUrl = asset("/images/company/" . $this->logo);
            } else {
                $imageUrl = asset("/images/no-image.jpg");
            }
        } else {
            $imageUrl = asset("/images/no-image.jpg");
        }

        return $imageUrl;
    }

    public function scopeFindBySlug($query, $slug) {
        return $query->where('slug', '=', $slug)->first();
    }

}
