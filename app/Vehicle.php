<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name','category_id','company_id','slug','image'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function bookingCount()
    {
        return $this->hasMany(Booking::class, 'vehicle_id')->where(['status_id' => 3])->orWhere(['status_id' => 5]);
    }

    public function fleets()
    {
        return $this->hasMany(Fleet::class, 'vehicle_id');
    }

    public function availableFleets()
    {
        return $this->hasMany(Fleet::class, 'vehicle_id')->where(['status_id' => 1]);
    }

    public function unavailableFleets()
    {
        return $this->hasMany(Fleet::class, 'vehicle_id')->where(['status_id' => 2]);
    }

    public function bookedFleets()
    {
        return $this->hasMany(Fleet::class, 'vehicle_id')->where(['status_id' => 3]);
    }

    public function faultyFleets()
    {
        return $this->hasMany(Fleet::class, 'vehicle_id')->where(['status_id' => 4]);
    }

    public function scopeFindBySlug($query, $slug) {
        return $query->where('slug', '=', $slug)->first();
    }

    public function getImageUrlAttribute($value) {

        $imageUrl = "";

        if (!is_null($this->image) && $this->image != '') {
            $imagePath = public_path() . "/images/vehicles/" . $this->image;
            if (file_exists($imagePath)) {
                $imageUrl = asset("/images/vehicles/" . $this->image);
            } else {
                $imageUrl = asset("/images/no-image.jpg");
            }
        } else {
            $imageUrl = asset("/images/no-image.jpg");
        }

        return $imageUrl;
    }
}
