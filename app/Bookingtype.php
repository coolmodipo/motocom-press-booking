<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bookingtype extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name','slug', 'hex_code'
    ];
}
