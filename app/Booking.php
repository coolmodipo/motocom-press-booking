<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;

    protected $fillable = [
      'borrower_id','vehicle_id', 'status_id', 'fleet_id','bookingtype_id','start_date','end_date','special_request','collector','collector_time','returnee','returnee_time','insurance_id', 'mileage_out', 'mileage_in', 'mileage_used'
    ];

    protected $dates = [
        'start_date',
        'end_date'
    ];

    public function borrower()
    {
        return $this->belongsTo(Borrower::class, 'borrower_id');
    }

    public function fleet()
    {
        return $this->belongsTo(Fleet::class, 'fleet_id');
    }

    public function bookingtype()
    {
        return $this->belongsTo(Bookingtype::class, 'bookingtype_id');
    }

    public function insurance()
    {
        return $this->belongsTo(Insurance::class, 'insurance_id');
    }
}
