<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCompany extends Model
{
    protected $table = 'client_company';

    public $timestamps = false;

    protected $fillable = [
        'client_id', 'company_id'
    ];
}