<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fleet extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'vehicle_id','reg_number','frame_number','engine','colour_id','status_id','accessories','notes', 'slug', 'mileage'
    ];

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class, 'vehicle_id');
    }

    public function colour()
    {
        return $this->belongsTo(Colour::class, 'colour_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function scopeFindBySlug($query, $slug) {
        return $query->where('slug', '=', $slug)->first();
    }

    public function booked()
    {
        return $this->hasOne(Booking::class, 'fleet_id', 'id');
    }

    public function booking()
    {
        return $this->hasMany(Booking::class, 'fleet_id');
    }
}
