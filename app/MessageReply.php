<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageReply extends Model
{
    public $timestamps = false;

    protected $table = 'message_reply';

    protected $fillable = ['message_id', 'reply_id'];
}
