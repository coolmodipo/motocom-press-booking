<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;

class MessageType extends Model
{

    protected $fillable = [
        'type'
    ];

    public function messages()
    {
        return $this->hasMany(Message::class, 'type_id')->where(['client_id' => Auth::user()->client->id])->where(['read' => 0]);
    }
}
