-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 192.168.10.10    Database: laravel_site
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bookings`
--

DROP TABLE IF EXISTS `bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `borrower_id` int(11) NOT NULL,
  `fleet_id` int(11) NOT NULL,
  `bookingtype_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `special_request` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `collector` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `collector_time` time NOT NULL,
  `returnee` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `returnee_time` time NOT NULL,
  `insurance_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `mileage_out` int(11) DEFAULT NULL,
  `mileage_in` int(11) DEFAULT NULL,
  `mileage_used` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookings`
--

LOCK TABLES `bookings` WRITE;
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;
INSERT INTO `bookings` VALUES (1,1,1,2,'2018-04-09','2018-04-14','This is a test!','James Terry','09:30:00','James Terry','18:00:00',1,'2018-04-05 10:16:16','2018-04-04 19:27:28',NULL,6,1,20,40,20);
/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookingtypes`
--

DROP TABLE IF EXISTS `bookingtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookingtypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hex_code` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookingtypes`
--

LOCK TABLES `bookingtypes` WRITE;
/*!40000 ALTER TABLE `bookingtypes` DISABLE KEYS */;
INSERT INTO `bookingtypes` VALUES (1,'Single','#af7ac5','single','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(2,'Comparison','#f2d7d5','comparison','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(3,'Group','#f5b7b1','group','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(4,'Photoshoot','#7fb3d5','photoshoot','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(5,'Internal Evaluation / Run-in','#a2d9ce','internal-evaluation-run-in','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(6,'Launch','#1f618d','launch','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(7,'Long Term Test','#21618c','long-term-test','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(8,'Courtesy Loan (Service)','#2e4053','courtesy-loan-service','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(9,'Event','#943126','event','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(10,'N/A','#17202a','n-a','2018-03-01 10:18:53','2018-02-17 17:00:08',NULL),(11,'Demo','#aeb6bf','demo','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(12,'Single','#7d6608','single','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(13,'B-King Roadshow','#2980b9','b-king-roadshow','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(14,'Technical Training','#943126','technical-training','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(15,'Photoshoot','#5d6d7e','photoshoot','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(16,'Race','#78281f','race','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(17,'BSM Training School','#633974','bsm-training-school','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(18,'Defleet','#aab7b8','defleet','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(19,'Off road lease','#a93226','off-road-lease','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(20,'TT Marshall Bike','#abebc6','tt-marshall-bike','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(21,'Show Room','#717d7e','show-room','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(22,'Sales Campaign','#aed6f1','sales-campaign','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(23,'Tyre Launch','#7dcea0','tyre-launch','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(24,'Dealer Event','#212f3d','dealer-event','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(25,'SGB Event','#fdedec','sgb-event','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(26,'Race Team Loan','#fdedec','race-team-loan','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(27,'Ride 2 Work Week','#4a235a','ride-2-work-week','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(28,'Ace Cafe','#138d75','ace-cafe','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(29,'Brighton Burn Up','#48c9b0','brighton-burn-up','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(30,'Test Ride Roadshow','#e59866','test-ride-roadshow','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(31,'Track bike','#1b2631','track-bike','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(32,'M.O.R.E','#2874a6','m-o-r-e','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL),(33,'Copdock Bike Show','#117864','copdock-bike-show','2018-03-01 10:18:53','2018-03-01 10:18:53',NULL);
/*!40000 ALTER TABLE `bookingtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `borrowers`
--

DROP TABLE IF EXISTS `borrowers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `borrowers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `organisation_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_postcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `borrowers_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `borrowers`
--

LOCK TABLES `borrowers` WRITE;
/*!40000 ALTER TABLE `borrowers` DISABLE KEYS */;
INSERT INTO `borrowers` VALUES (1,'Dipo George','Motocom LTD','Liscombe Park, Soulbury, Nr Leighton Buzzard, Bedfordshire.','LU7 0JL','01525 270100','dipo.george@motocom.co.uk',NULL,NULL,NULL,1,'2018-02-17 18:06:25','2018-02-17 18:41:49',NULL);
/*!40000 ALTER TABLE `borrowers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Street','street','2018-02-26 10:09:12','2018-02-26 10:09:12',NULL),(2,'Supersports','supersports','2018-02-26 10:09:12','2018-02-26 10:09:12',NULL),(3,'Adventure Sports','adventure-sports','2018-02-26 10:09:12','2018-02-26 10:09:12',NULL),(4,'Cruiser','cruiser','2018-02-26 10:09:12','2018-02-26 10:09:12',NULL),(5,'Learner','learner','2018-02-26 10:09:12','2018-02-26 10:09:12',NULL),(6,'Scooter','scooter','2018-02-26 10:09:12','2018-02-26 10:09:12',NULL),(7,'Dual Purpose','dual-purpose','2018-02-26 10:09:12','2018-02-26 10:09:12',NULL),(8,'Motocross','motocross','2018-02-26 10:09:12','2018-02-26 10:09:12',NULL),(9,'Endurance','endurance','2018-02-26 10:09:12','2018-02-26 10:09:12',NULL),(10,'ATV','atv','2018-02-26 10:09:12','2018-02-26 10:09:12',NULL),(11,'Junior','junior','2018-02-26 10:09:12','2018-02-26 10:09:12',NULL),(12,'New Category','new-category','2018-02-16 16:29:50','2018-02-16 17:05:28','2018-02-16 17:05:28');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_company`
--

DROP TABLE IF EXISTS `client_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_company` (
  `client_id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  KEY `client_company_client_id_foreign` (`client_id`),
  KEY `client_company_company_id_foreign` (`company_id`),
  CONSTRAINT `client_company_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `client_company_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_company`
--

LOCK TABLES `client_company` WRITE;
/*!40000 ALTER TABLE `client_company` DISABLE KEYS */;
INSERT INTO `client_company` VALUES (6,1),(1,1),(1,7),(7,7),(8,7),(9,7);
/*!40000 ALTER TABLE `client_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clients_user_id_foreign` (`user_id`),
  CONSTRAINT `clients_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (1,'Dipo','George','01234567890','09876543210','This is a test! I am testing this!','3-phactrock10.jpg',3,'2018-02-13 11:54:12','2018-02-15 13:15:43',NULL),(2,'Steve','James',NULL,NULL,NULL,NULL,4,'2018-02-13 11:54:12','2018-02-14 20:10:48',NULL),(3,'Test','User',NULL,NULL,NULL,NULL,7,'2018-02-13 11:54:12','2018-02-13 11:54:12',NULL),(4,'James','Terry',NULL,NULL,NULL,NULL,10,'2018-02-14 16:52:37','2018-02-14 16:52:37',NULL),(6,'Fred','Perry',NULL,NULL,NULL,NULL,13,'2018-02-15 16:48:57','2018-02-15 16:48:57',NULL),(7,'Chris','Page',NULL,NULL,NULL,NULL,14,'2018-02-15 17:54:00','2018-02-15 17:54:00',NULL),(8,'Dipo','George',NULL,NULL,NULL,NULL,15,'2018-02-15 18:01:58','2018-02-15 18:01:58',NULL),(9,'Luke','Plumber',NULL,NULL,NULL,NULL,16,'2018-02-15 21:34:03','2018-02-15 21:34:03',NULL);
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colours`
--

DROP TABLE IF EXISTS `colours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hex` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colours`
--

LOCK TABLES `colours` WRITE;
/*!40000 ALTER TABLE `colours` DISABLE KEYS */;
INSERT INTO `colours` VALUES (1,'Red','red','#ff0000','2018-02-17 08:16:33','2018-02-17 09:29:49',NULL),(2,'Blue','blue',NULL,'2018-02-17 08:16:33','2018-02-17 08:16:33',NULL),(3,'Green','green',NULL,'2018-02-17 08:16:33','2018-02-17 08:16:33',NULL),(4,'White','white','#ffffff','2018-02-17 08:16:33','2018-02-17 08:16:33',NULL),(5,'Black','black','#000000','2018-02-17 08:16:33',NULL,NULL);
/*!40000 ALTER TABLE `colours` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `county` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default` int(11) DEFAULT '1',
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companies_client_id_foreign` (`client_id`),
  CONSTRAINT `companies_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,1,'Suzuki','123 Test Street','Test Ave.','Test','Test County','TE5 7ER','1-Suzuki-logo.png',1,'suzuki','2018-02-13 11:54:12','2018-02-17 01:20:59',NULL),(2,2,'Honda','Test Address 2','Test Address 2','Test Address City','Test Address County','TE5 7ER','2-honda_bikes_logo.jpg',1,'honda','2018-02-14 18:45:50','2018-02-16 22:43:33',NULL),(3,1,'Kawasaki','123 Test Street1','Test Address 2','Test Address City','Test Address County','TE5 7ER','3-kawasaki-logo.png',0,'kawasaki','2018-02-15 05:19:39','2018-02-17 01:20:59',NULL),(7,1,'MotoCom LTD','Liscombe Park','Soulbury','Leighton Buzzard','Buckinghamshire','LU7 0JL',NULL,0,'motocom-ltd','2018-02-15 17:42:41','2018-02-17 01:20:59',NULL),(8,8,'Yamaha','123 Test Street1','Test Address 2','Test Address City','Test Address County','TE5 7ER','8-yamaha.png',1,'yamaha','2018-02-15 21:11:49','2018-02-16 22:45:02',NULL);
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fleets`
--

DROP TABLE IF EXISTS `fleets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fleets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle_id` int(11) NOT NULL,
  `reg_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `frame_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `engine` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `colour_id` int(11) NOT NULL,
  `status_id` int(11) DEFAULT '1',
  `accessories` text COLLATE utf8mb4_unicode_ci,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `booking_id` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `mileage` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fleets`
--

LOCK TABLES `fleets` WRITE;
/*!40000 ALTER TABLE `fleets` DISABLE KEYS */;
INSERT INTO `fleets` VALUES (1,1,'KW15 PRT','RWT-8795KlJ-10','750CC',3,2,'This is the additional Accessories.','This is the notes part.\n<br/>04/04/2018 - Will need some service.',1,'kw15-prt','2018-02-16 20:58:43','2018-04-04 19:28:12',NULL,40),(2,1,'KW15 PPR','12456780','500CC',5,3,'This is the accessories part.',NULL,NULL,'kw15-ppr','2018-02-16 22:02:55','2018-03-02 18:57:31',NULL,NULL),(3,5,'KW16','RM250-10-re','250CC',4,3,NULL,NULL,NULL,'kw16','2018-02-16 22:04:20','2018-02-16 22:10:35','2018-02-16 22:10:35',NULL),(4,4,'fgdfg','dfgdfg','dfg',5,1,NULL,'This is the notes part.',NULL,'fgdfg','2018-02-16 22:04:20','2018-02-17 14:31:27',NULL,NULL),(5,3,'dasdadad','asdasda','dasda',2,3,'asdasda dasdas dasd','asdasdasd',NULL,'dasdadad','2018-02-17 09:53:08','2018-02-17 09:53:08',NULL,NULL),(6,1,'KW15 PRT 123','RWT-8795KlJ-10 123','250CC',3,1,'These are the accessories.','These are the notes.',NULL,'','2018-02-17 13:27:10','2018-02-17 14:18:34','2018-02-17 14:18:34',NULL);
/*!40000 ALTER TABLE `fleets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insurances`
--

DROP TABLE IF EXISTS `insurances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insurances` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insurances`
--

LOCK TABLES `insurances` WRITE;
/*!40000 ALTER TABLE `insurances` DISABLE KEYS */;
INSERT INTO `insurances` VALUES (1,'Provided by Borrower','provided-by-borrower','2018-03-02 11:38:22','2018-03-02 11:38:22',NULL,0),(2,'Suzuki GB','suzuki-gb','2018-03-02 11:38:22','2018-03-02 11:38:22',NULL,0),(3,'Honda GB','honda-gb','2018-03-02 11:38:22','2018-03-02 11:38:22',NULL,0),(4,'Kawasaki GB','kawasaki-gb','2018-03-02 11:38:22','2018-03-02 11:38:22',NULL,0),(5,'Yamaha GB','yamaha-gb','2018-03-02 11:38:22','2018-03-02 11:38:22',NULL,0);
/*!40000 ALTER TABLE `insurances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message_reply`
--

DROP TABLE IF EXISTS `message_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_reply` (
  `message_id` int(11) NOT NULL,
  `reply_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_reply`
--

LOCK TABLES `message_reply` WRITE;
/*!40000 ALTER TABLE `message_reply` DISABLE KEYS */;
INSERT INTO `message_reply` VALUES (1,1),(2,1),(3,1),(4,4),(5,5),(6,6),(7,6),(8,8);
/*!40000 ALTER TABLE `message_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message_types`
--

DROP TABLE IF EXISTS `message_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_types`
--

LOCK TABLES `message_types` WRITE;
/*!40000 ALTER TABLE `message_types` DISABLE KEYS */;
INSERT INTO `message_types` VALUES (1,'Inbox','2018-02-22 14:02:19','2018-02-22 14:02:19'),(2,'Sent','2018-02-22 14:02:19','2018-02-22 14:02:19'),(3,'Trash','2018-02-22 14:02:19','2018-02-22 14:02:19');
/*!40000 ALTER TABLE `message_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` int(11) NOT NULL DEFAULT '1',
  `sender_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,'First Email','This is the first email test.',1,1,6,1,1,'2018-02-16 15:28:43','2018-02-16 15:32:09',NULL,'1-first-email'),(2,'Re: First Email','OK. I have recieved it thank you.',1,6,1,1,1,'2018-02-16 15:32:41','2018-03-02 15:22:03',NULL,'2-re-first-email'),(3,'Re: Re: First Email','What about now?',1,1,6,1,1,'2018-02-16 15:37:18','2018-04-05 11:06:55',NULL,'3-re-re-first-email'),(4,'Test','This is a test',1,6,1,4,1,'2018-02-16 23:08:18','2018-04-04 19:37:55',NULL,'4-test'),(5,'This is the second test','Here you go.',1,1,6,5,1,'2018-02-16 23:42:18','2018-04-04 20:01:40',NULL,'5-this-is-the-second-test'),(6,'Motocom LTD','This is the final test.',3,6,1,6,1,'2018-02-16 23:44:04','2018-03-02 14:57:00','2018-03-02 14:57:00','6-motocom-ltd'),(7,'This is a test','This is a test.',1,1,6,6,1,'2018-04-04 19:37:37','2018-04-04 19:38:45',NULL,'6-this-is-a-test'),(8,'Message to Fred Perry','This is a message for Fred Perry from Dipo. Please could you reply back.\r\nThanks',1,1,6,8,1,'2018-04-05 11:14:18','2018-04-05 11:14:47',NULL,'8-message-to-fred-perry');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_02_12_144517_laratrust_setup_tables',2),(4,'2018_02_12_144744_create_tests_table',3),(8,'2018_02_15_141753_create_clients_table',4),(9,'2018_02_15_141807_create_companies_table',4),(10,'2018_02_19_161550_add_contact_details_to_clients_table',5),(11,'2018_02_20_164020_create_notifications_table',6),(12,'2018_02_21_115908_create_client_company_table',7),(13,'2018_02_22_112604_create_messages_table',8),(14,'2018_02_22_134810_create_message_types_table',9),(15,'2018_02_23_122840_create_message_replies_table',10),(16,'2018_02_26_094014_create_categories_table',11),(17,'2018_02_26_111256_create_vehicles_table',12),(18,'2018_02_26_135745_create_fleets_table',13),(19,'2018_02_28_091231_create_statuses_table',14),(20,'2018_02_28_091239_create_colours_table',14),(21,'2018_02_28_173048_create_bookingtypes_table',15),(22,'2018_03_01_102131_create_borrowers_table',16),(23,'2018_03_02_102151_create_bookings_table',17),(24,'2018_03_02_103252_create_insurances_table',17);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `email` int(11) NOT NULL DEFAULT '0',
  `site` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES (1,1,0,1,'2018-02-13 11:54:12','2018-02-15 13:16:01',NULL),(2,2,0,0,'2018-02-13 11:54:12','2018-02-13 11:54:12',NULL),(3,3,0,0,'2018-02-13 11:54:12','2018-02-13 11:54:12',NULL),(4,4,0,0,'2018-02-13 11:54:12','2018-02-13 11:54:12',NULL),(5,6,1,0,'2018-02-13 11:54:12','2018-04-04 20:01:24',NULL),(6,7,0,0,'2018-02-13 11:54:12',NULL,NULL),(7,8,0,0,'2018-02-15 18:01:58','2018-02-15 18:01:58',NULL),(8,9,0,0,'2018-02-15 21:34:03','2018-02-15 21:34:03',NULL);
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('steve.james@test.com','$2y$10$LP7NIOIPptyO2Zo/fh1bD.0PQeIzveRGkvfS8GWPzecMOvTp39Pga','2018-02-14 01:28:44'),('dipo@tester.com','$2y$10$b24UZ0mTWWLuwLC6ke.w0uGXJZ2hYcFbQ7CGBnDwLiZZkwOKs5.b6','2018-02-15 18:03:01'),('cris@motocom.co.uk','$2y$10$XQp.C5mzvR7jQIYInFBVrOKbkdUMAQ./hM0rNE7H.FX4ciy8B9mIC','2018-02-15 18:03:44');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(1,2),(2,2),(3,2),(4,2),(9,2),(10,2),(9,3),(10,3);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_user`
--

DROP TABLE IF EXISTS `permission_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_user` (
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  KEY `permission_user_permission_id_foreign` (`permission_id`),
  CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_user`
--

LOCK TABLES `permission_user` WRITE;
/*!40000 ALTER TABLE `permission_user` DISABLE KEYS */;
INSERT INTO `permission_user` VALUES (1,1,'App\\User'),(5,1,'App\\User'),(9,4,'App\\User'),(10,1,'App\\User'),(10,4,'App\\User'),(11,4,'App\\User');
/*!40000 ALTER TABLE `permission_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'create-users','Create Users','Create Users','2018-02-13 11:54:12','2018-02-13 11:54:12'),(2,'read-users','Read Users','Read Users','2018-02-13 11:54:12','2018-02-13 11:54:12'),(3,'update-users','Update Users','Update Users','2018-02-13 11:54:12','2018-02-13 11:54:12'),(4,'delete-users','Delete Users','Delete Users','2018-02-13 11:54:12','2018-02-13 11:54:12'),(5,'create-acl','Create Acl','Create Acl','2018-02-13 11:54:12','2018-02-13 11:54:12'),(6,'read-acl','Read Acl','Read Acl','2018-02-13 11:54:12','2018-02-13 11:54:12'),(7,'update-acl','Update Acl','Update Acl','2018-02-13 11:54:12','2018-02-13 11:54:12'),(8,'delete-acl','Delete Acl','Delete Acl','2018-02-13 11:54:12','2018-02-13 11:54:12'),(9,'read-profile','Read Profile','Read Profile','2018-02-13 11:54:12','2018-02-13 11:54:12'),(10,'update-profile','Update Profile','Update Profile','2018-02-13 11:54:12','2018-02-13 11:54:12'),(11,'create-profile','Create Profile','Create Profile','2018-02-13 11:54:12','2018-02-13 11:54:12'),(12,'create-blog','Create Blog','This is for the creating of blogs on the system. TEST','2018-02-14 14:16:03','2018-02-14 16:35:54');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1,'App\\User'),(2,2,'App\\User'),(3,3,'App\\User'),(3,4,'App\\User'),(3,5,'App\\User'),(3,7,'App\\User'),(3,10,'App\\User'),(3,13,'App\\User'),(3,14,'App\\User'),(3,15,'App\\User'),(3,16,'App\\User');
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'super-administrator','Super Administrator','Super Administrator role allows user to access all site.','2018-02-13 11:54:12','2018-02-14 15:13:30'),(2,'administrator','Administrator','Administrator role allows user to access must of the site.','2018-02-13 11:54:12','2018-02-13 11:54:12'),(3,'user','User','User role for access of the frontend site. THIS IS A TES!','2018-02-13 11:54:12','2018-02-14 16:35:21'),(4,'test-role','Test Role','This is a test role for testing purposes.','2018-02-14 14:04:12','2018-02-14 16:23:57');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statuses`
--

DROP TABLE IF EXISTS `statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statuses`
--

LOCK TABLES `statuses` WRITE;
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` VALUES (1,'Available','available','success','2018-02-17 08:16:53','2018-02-17 09:31:51',NULL),(2,'Unavailable','unavailable','warning','2018-02-17 08:16:33','2018-02-17 08:16:33',NULL),(3,'Booked','booked','info','2018-02-17 08:16:33','2018-02-17 08:16:33',NULL),(4,'Faulty','faulty','danger','2018-02-17 08:16:33','2018-02-17 08:16:33',NULL);
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Dipo George','dipo.george@motocom.co.uk','$2y$10$.v3mMqmuQhOIXCL4o6QFhufwY87W1q30SV7BuagShc9hwiLB2sdoa','Axs3eWAjGS1L4xD2wNS4C70LaOGNFQFPJDiQQLab710ST09RTc4RjlZXKqYd','2018-02-13 11:54:12','2018-02-13 12:08:20',NULL),(2,'Oladipo George','dipogeorge@hotmail.com','$2y$10$p5izNgRHE0tMJpQ8EE0lC.eTo8NC9UWkiyhOpqF3Cl61PGtAVzvWm','GX2OYwARQpWem1ANQf02SRxnpxWWCFwWQRC4h3EFTmdbTzZ44X2ShjFiUevg','2018-02-13 11:54:12','2018-02-14 12:41:56',NULL),(3,'Dipo George','dipogeorge@googlemail.com','$2y$10$fYYyx/6VTnh5.iAxhwMhkOv0vvOiCkoKx9Lilu1rCLZUxlq107nS6','nbICCiHE1KdJNwzEgBRlDk6gkjayRvJO74kFBbr7zYf0Rj0RR0ERsV9trXRc','2018-02-13 11:54:12','2018-02-15 11:11:17',NULL),(4,'Steve James','steve.jame@tester.com','$2y$10$AWi9ThmQog2NCOaTiX4J8u6hX1MhjhwX3ujTqG6tiEAriaC33Jmaa','EeqJSVYozxdWR1hWcelvpMPkb4r7hGWt1FUxZiCQPrET794XO4z4UhzaQ7Ro','2018-02-13 11:54:12','2018-02-14 20:10:47',NULL),(5,'Steven James','steve.james@test.com','$2y$10$2m4Yi6ntww.b/0laAhJkoeUtCi4QFsPUHIhNYvGkljTTvIKckhuRS',NULL,'2018-02-14 01:27:01','2018-02-14 16:34:31','2018-02-14 16:34:31'),(7,'Test User','test@test.com','$2y$10$bkAONsdDAZ1dHcgEzKulDuLyg6UWTtOX02Xf1GW7WRHVdMJqVd4IO','RW6dDB2AeBBtqrPhg9m13LUykTnwVkaRZ9RXJuApjLO9PQkiAwMczH4hgkAd','2018-02-14 03:31:09','2018-02-14 03:31:09',NULL),(8,'James Terry','james.terry1@test.com','$2y$10$iV6Sdf5y81ymb2jj8rB...a09Ir.OJ.Ne4EK9ZGT3Qnqn9lpNYM2G',NULL,'2018-02-14 16:45:33','2018-02-14 16:51:17','2018-02-14 16:51:17'),(10,'James Terry','james.terry@test.com','$2y$10$Am4HURn198JcdSTonaPpVeTmWRlStwqgokvjPdRg2s1Blz6FKcK9q','vDIa4z2Qx6BPPeQY3vdEiRNk79OiGVkmv6iq3XhGKxiNtCKLzECvKvQcDqBA','2018-02-14 16:52:37','2018-02-14 16:52:37',NULL),(13,'Fred Perry','fred.perry@test.com','$2y$10$OZNB7emUM82d3FxiXzbUt..xpH6mCgl4ckh.DJc7TwMtirdUxKtte','IobBvAELCp9xbI2EgKzoeRurj5zT5adqlk6rWQbCIPnnhjudjTjizTPbo8Tw','2018-02-15 16:48:57','2018-02-15 16:48:57',NULL),(14,'Chris Page','cris@motocom.co.uk','$2y$10$8XVWPctbKUJNHLeKyO5y.Oi5tqRM91B4qazCVBgru36qY9sovDlg2','hqqjuF1E8Ntho6zrm29COb31PWxJeJt6px0gWoCodiNIldlMF9W4cApuBS5s','2018-02-15 17:54:00','2018-02-15 17:54:00',NULL),(15,'Dipo George','dipo@tester.com','$2y$10$2uPn6Z0Pj1MPRj/BQpIVNOIn4.gyzT/42IMItTEASWPxTsZEigZjW','mB1fZeMpn7GgBB9R6yjNwPfPSF7izg23qbI6Oy1R2fEHNmryVbPtdylr6qDK','2018-02-15 18:01:58','2018-02-15 18:01:58',NULL),(16,'Luke Plumber','luke.p@test.com','$2y$10$Cftj5zMZ6ROn1nuFm1oUX.BAGsBv8RNJmJMK/KlTeBl0HO53PKaBe','c2Be8YXblCUCkAnd58CYUc904sGgttT0GbYivTgioYqIglOrDErPUaBYekZe','2018-02-15 21:34:03','2018-02-15 21:34:03',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicles`
--

LOCK TABLES `vehicles` WRITE;
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT INTO `vehicles` VALUES (1,'GSX-R600',1,1,'gsx-r600','1-gsx-r600.jpg','2018-02-16 18:13:52','2018-02-16 21:55:26',NULL),(2,'GSX-R750',11,3,'gsx-r750','2-gsx-r750.png','2018-02-16 19:36:55','2018-02-16 21:28:07',NULL),(3,'GSX-R1000',9,2,'gsx-r1000','3-gsx-r1000.jpg','2018-02-16 19:39:15','2018-02-16 21:58:42',NULL),(4,'RM125',1,1,'rm125','4-2008-Suzuki-RM125a.jpg','2018-02-16 20:05:18','2018-02-17 06:14:10',NULL),(5,'RM250',1,1,'rm250','5-rm250.png','2018-02-16 20:05:18','2018-02-16 21:27:12',NULL);
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-09 11:31:30
