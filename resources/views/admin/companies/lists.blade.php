@extends('_layouts.admin')

@section('title', 'Companies')

@section('additional-css')
    <link href="{{ asset('css/data-table.css') }}" rel="stylesheet">
@endsection

@section('content')

    @include('_modals.admin.company.new')
    @include('_modals.admin.company.profile')
    @include('_modals.admin.company.users')
    @include('_modals.admin.company.delete')

    @include('_sections.companies.lists')

    <section class="content">

        <div class="row">

            <div class="col-md-12">

                <ul>
                    <li>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_new">Add New Company</button>
                    </li>
                </ul>

                <div class="box">

                    <div class="box-body">
                        <table id="companyTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Company Name</th>
                                <th>Company Admin</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($companies->count())
                                @foreach($companies as $data)
                                    <tr>
                                        <td>{{ $data->id }}</td>
                                        <td>{{ $data->company_name }}</td>
                                        <td>{{ $data->admin->user->name }}</td>
                                        <td>
                                            <a class="label label-success" data-toggle="modal" data-target="#modal_profile" data-cid="{{ $data->id }}">Edit Company</a>
                                            <a class="label label-primary" data-toggle="modal" data-target="#modal_users" data-cid="{{ $data->id }}">Users</a>
                                            <a class="label label-danger" data-toggle="modal" data-target="#modal_delete" data-cid="{{ $data->id }}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">There are no companies at present</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </section>


@endsection

@section('additional-js')

    <script src="{{ asset('js/data-table.js') }}"></script>

    <script>

        $(function () {
            $('#companyTable').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        });

        $(document).on('show.bs.modal','#modal_profile', function (e) {

            var cid = $(e.relatedTarget).data('cid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/company-form') }}/"+ cid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_users', function (e) {

            var cid = $(e.relatedTarget).data('cid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/company-user-form') }}/"+ cid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_delete', function (e) {

            var cid = $(e.relatedTarget).data('cid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/company-delete-form') }}/"+ cid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

    </script>


@endsection