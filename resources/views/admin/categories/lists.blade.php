@extends('_layouts.admin')

@section('title', 'Booking Types')

@section('additional-css')
    <link href="{{ asset('css/data-table.css') }}" rel="stylesheet">
@endsection

@section('content')

    @include('_modals.admin.category.new')
    @include('_modals.admin.category.edit')
    @include('_modals.admin.category.delete')

    @include('_sections.categories.lists')

    <section class="content">

        <div class="row">

            <div class="col-md-12">

                <ul>
                    <li>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_new">Add New Category</button>
                    </li>
                </ul>

                <div class="box">

                    <div class="box-body">
                        <table id="categoryTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($categories->count())
                                @foreach($categories as $data)
                                    <tr>
                                        <td>{{ $data->id }}</td>
                                        <td>{{ $data->name }}</td>
                                        <td>
                                            <a class="label label-success" data-toggle="modal" data-target="#modal_edit" data-cid="{{ $data->id }}">Edit Category</a>
                                            <a class="label label-danger" data-toggle="modal" data-target="#modal_delete" data-cid="{{ $data->id }}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">There are no categories at present</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </section>

@endsection

@section('additional-js')

    <script src="{{ asset('js/data-table.js') }}"></script>

    <script>

        $(function () {
            $('#categoryTable').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        });

        $(document).on('show.bs.modal','#modal_edit', function (e) {

            var cid = $(e.relatedTarget).data('cid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/category-form') }}/"+ cid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_delete', function (e) {

            var cid = $(e.relatedTarget).data('cid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/category-delete-form') }}/"+ cid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        </script>

@endsection