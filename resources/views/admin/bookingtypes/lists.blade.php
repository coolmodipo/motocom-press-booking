@extends('_layouts.admin')

@section('title', 'Booking Types')

@section('additional-css')
    <link href="{{ asset('css/data-table.css') }}" rel="stylesheet">
@endsection

@section('content')

    @include('_modals.admin.bookingtype.new')
    @include('_modals.admin.bookingtype.edit')
    @include('_modals.admin.bookingtype.delete')

    @include('_sections.bookingtype.lists')

    <section class="content">

        <div class="row">

            <div class="col-md-12">

                <ul>
                    <li>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_new">Add New Booking Type</button>
                    </li>
                </ul>

                <div class="box">

                    <div class="box-body">
                        <table id="bookingTypeTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($bookingtypes->count())
                                @foreach($bookingtypes as $data)
                                    <tr>
                                        <td>{{ $data->id }}</td>
                                        <td>{{ $data->name }}</td>
                                        <td>
                                            <a class="label label-success" data-toggle="modal" data-target="#modal_edit" data-btid="{{ $data->id }}">Edit Booking Type</a>
                                            <a class="label label-danger" data-toggle="modal" data-target="#modal_delete" data-btid="{{ $data->id }}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">There are no booking types at present</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </section>

@endsection

@section('additional-js')

    <script src="{{ asset('js/data-table.js') }}"></script>

    <script>

        $(function () {
            $('#bookingTypeTable').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        });

        $(document).on('show.bs.modal','#modal_edit', function (e) {

            var btid = $(e.relatedTarget).data('btid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/bookingtype-form') }}/"+ btid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_delete', function (e) {

            var btid = $(e.relatedTarget).data('btid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/bookingtype-delete-form') }}/"+ btid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

    </script>

@endsection