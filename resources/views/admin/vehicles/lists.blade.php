@extends('_layouts.admin')

@section('title', 'Vehicles')

@section('additional-css')
    <link href="{{ asset('css/data-table.css') }}" rel="stylesheet">
@endsection

@section('content')

    @include('_modals.admin.vehicle.new')
    @include('_modals.admin.vehicle.edit')
    @include('_modals.admin.vehicle.delete')

    @include('_sections.vehicles.lists')

    <section class="content">

        <div class="row">

            <div class="col-md-12">

                <ul>
                    <li>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_new">Add New Model</button>
                    </li>
                </ul>

                <div class="box">

                    <div class="box-body">
                        <table id="vehicleTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Company/ Make</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($vehicles->count())
                                @foreach($vehicles as $data)
                                    <tr>
                                        <td>{{ $data->id }}</td>
                                        <td>{{ $data->name }}</td>
                                        <td>{{ $data->category->name }}</td>
                                        <td>{{ $data->company->company_name }}</td>
                                        <td>
                                            @if($data->image)
                                                <i class="fa fa-image"></i>
                                            @else
                                                <i class="fa fa-eye-slash"></i>
                                            @endif

                                            <a class="label label-success" data-toggle="modal" data-target="#modal_edit" data-vid="{{ $data->id }}">Edit Model</a>
                                            <a class="label label-danger" data-toggle="modal" data-target="#modal_delete" data-vid="{{ $data->id }}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">There are no vehicles at present</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </section>

@endsection

@section('additional-js')

    <script src="{{ asset('js/data-table.js') }}"></script>

    <script>

        $(function () {
            $('#vehicleTable').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        });

        $(document).on('show.bs.modal','#modal_edit', function (e) {

            var vid = $(e.relatedTarget).data('vid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/model-form') }}/"+ vid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_delete', function (e) {

            var vid = $(e.relatedTarget).data('vid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/model-delete-form') }}/"+ vid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

    </script>

@endsection