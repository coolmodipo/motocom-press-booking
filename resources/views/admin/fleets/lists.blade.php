@extends('_layouts.admin')

@section('title', 'Fleets')

@section('additional-css')
    <link href="{{ asset('css/data-table.css') }}" rel="stylesheet">
@endsection

@section('content')

    @include('_modals.admin.fleet.new')
    @include('_modals.admin.fleet.edit')
    @include('_modals.admin.fleet.delete')

    @include('_sections.fleets.lists')

    <section class="content">

        <div class="row">

            <div class="col-md-12">

                <ul>
                    <li>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_new">Add a new Fleet item</button>
                    </li>
                </ul>

                <div class="box">

                    <div class="box-body">
                        <table id="fleetTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Reg No.</th>
                                <th>Model</th>
                                <th>Company/ Make</th>
                                <th>Frame No.</th>
                                <th>Engine</th>
                                <th>Colour</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($fleets->count())
                                @foreach($fleets as $data)
                                    <tr>
                                        <td>{{ $data->id }}</td>
                                        <td>{{ $data->reg_number }}</td>
                                        <td><img src="{{ $data->vehicle->image_url }}" width="54" height="34"/> {{ $data->vehicle->name }}</td>
                                        <td>{{ $data->vehicle->company->company_name }}</td>
                                        <td>{{ $data->frame_number }}</td>
                                        <td>{{ $data->engine }}</td>
                                        <td>{{ $data->colour->name }}</td>
                                        <td>
                                            <a class="label label-success" data-toggle="modal" data-target="#modal_edit" data-fid="{{ $data->id }}">Edit Fleet item</a>
                                            <a class="label label-danger" data-toggle="modal" data-target="#modal_delete" data-fid="{{ $data->id }}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">There are no fleets at present</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </section>

@endsection

@section('additional-js')

    <script src="{{ asset('js/data-table.js') }}"></script>

    <script>

        $(function () {
            $('#fleetTable').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        });

        $(document).on('show.bs.modal','#modal_edit', function (e) {

            var fid = $(e.relatedTarget).data('fid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/fleet-form') }}/"+ fid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_delete', function (e) {

            var fid = $(e.relatedTarget).data('fid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/fleet-delete-form') }}/"+ fid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

    </script>

@endsection