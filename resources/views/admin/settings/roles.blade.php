@extends('_layouts.admin')

@section('title', 'Settings: Roles')

@section('content')

    @include('_sections.settings.roles')
    @include('_modals.admin.settings.roles.new')
    @include('_modals.admin.settings.roles.edit')
    @include('_modals.admin.settings.roles.delete')

    <section class="content">

        <div class="row">

            <div class="col-md-12">

                <ul>
                    <li>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_new">Add New Role</button>
                    </li>
                </ul>


                <div class="box">

                    <div class="box-body">
                        <table id="roleTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Display Name</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($roles->count())
                                @foreach($roles as $data)
                                    <tr>
                                        <td>{{ $data->id }}</td>
                                        <td>{{ $data->display_name }}</td>
                                        <td>{{ $data->description }}</td>
                                        <td>
                                            <a class="label label-success" data-toggle="modal" data-target="#modal_edit" data-rid="{{ $data->id }}">Edit Role</a>
                                            <a class="label label-danger" data-toggle="modal" data-target="#modal_delete" data-rid="{{ $data->id }}">Delete Role</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">There are no roles at present</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </section>

@endsection

@section('additional-js')

    <script src="{{ asset('js/bootstrap-datatable.js') }}"></script>
    <script src="{{ asset('js/data-table.js') }}"></script>

    <script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(function () {
        $('#roleTable').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : true
        })
    });

    $(document).on('show.bs.modal','#modal_edit', function (e) {

        var rid = $(e.relatedTarget).data('rid');

        $.ajax({
            type : 'post',
            url: "{{ url('/admin/settings/role-form') }}/"+ rid ,
            dataType: "html",
            success : function(data){
                $('.fetched-data').html(data);
            }
        });
    });

    $(document).on('show.bs.modal','#modal_delete', function (e) {

        var rid = $(e.relatedTarget).data('rid');

        $.ajax({
            type : 'post',
            url: "{{ url('/admin/settings/role-delete-form') }}/"+ rid ,
            dataType: "html",
            success : function(data){
                $('.fetched-data').html(data);
            }
        });
    });

    </script>

@endsection
