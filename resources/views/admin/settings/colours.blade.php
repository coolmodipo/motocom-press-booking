@extends('_layouts.admin')

@section('title', 'Settings: Colours')

@section('content')

    @include('_sections.settings.colours')
    @include('_modals.admin.settings.colour.new')
    @include('_modals.admin.settings.colour.edit')
    @include('_modals.admin.settings.colour.delete')

    <section class="content">

        <div class="row">

            <div class="col-md-12">

                <ul>
                    <li>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_new">Add New Colour</button>
                    </li>
                </ul>


                <div class="box">

                    <div class="box-body">
                        <table id="colourTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Hex Colour</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($colours->count())
                                @foreach($colours as $data)
                                    <tr>
                                        <td>{{ $data->id }}</td>
                                        <td>{{ $data->name }}</td>
                                        <td>{{ $data->hex }}</td>
                                        <td>
                                            <a class="label label-success" data-toggle="modal" data-target="#modal_edit" data-cid="{{ $data->id }}">Edit Colour</a>
                                            <a class="label label-danger" data-toggle="modal" data-target="#modal_delete" data-cid="{{ $data->id }}">Delete Colour</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">There are no colours at present</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </section>

@endsection

@section('additional-js')

    <script src="{{ asset('js/bootstrap-datatable.js') }}"></script>
    <script src="{{ asset('js/data-table.js') }}"></script>

    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function () {
            $('#colourTable').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        });

        $(document).on('show.bs.modal','#modal_edit', function (e) {

            var cid = $(e.relatedTarget).data('cid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/settings/colour-form') }}/"+ cid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_delete', function (e) {

            var cid = $(e.relatedTarget).data('cid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/settings/colour-delete-form') }}/"+ cid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

    </script>

@endsection
