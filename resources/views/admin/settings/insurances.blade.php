@extends('_layouts.admin')

@section('title', 'Settings: Insurances')

@section('content')

    @include('_sections.settings.insurances')
    @include('_modals.admin.settings.insurance.new')
    @include('_modals.admin.settings.insurance.edit')
    @include('_modals.admin.settings.insurance.delete')

    <section class="content">

        <div class="row">

            <div class="col-md-12">

                <ul>
                    <li>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_new">Add New Insurance</button>
                    </li>
                </ul>


                <div class="box">

                    <div class="box-body">
                        <table id="insuranceTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($insurances->count())
                                @foreach($insurances as $data)
                                    <tr>
                                        <td>{{ $data->id }}</td>
                                        <td>{{ $data->name }}</td>
                                        <td>
                                            <a class="label label-success" data-toggle="modal" data-target="#modal_edit" data-cid="{{ $data->id }}">Edit Insurance</a>
                                            <a class="label label-danger" data-toggle="modal" data-target="#modal_delete" data-cid="{{ $data->id }}">Delete Insurance</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">There are no Insurances at present</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </section>

@endsection

@section('additional-js')

    <script src="{{ asset('js/bootstrap-datatable.js') }}"></script>
    <script src="{{ asset('js/data-table.js') }}"></script>

    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function () {
            $('#insuranceTable').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        });

        $(document).on('show.bs.modal','#modal_edit', function (e) {

            var cid = $(e.relatedTarget).data('cid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/settings/insurance-form') }}/"+ cid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_delete', function (e) {

            var cid = $(e.relatedTarget).data('cid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/settings/insurance-delete-form') }}/"+ cid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

    </script>

@endsection
