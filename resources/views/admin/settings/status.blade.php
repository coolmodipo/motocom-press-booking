@extends('_layouts.admin')

@section('title', 'Settings: status')

@section('content')

    @include('_sections.settings.status')
    @include('_modals.admin.settings.status.new')
    @include('_modals.admin.settings.status.edit')
    @include('_modals.admin.settings.status.delete')

    <section class="content">

        <div class="row">

            <div class="col-md-12">

                <ul>
                    <li>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_new">Add New status</button>
                    </li>
                </ul>


                <div class="box">

                    <div class="box-body">
                        <table id="statusTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($statuses->count())
                                @foreach($statuses as $data)
                                    <tr>
                                        <td>{{ $data->id }}</td>
                                        <td>{{ $data->title }}</td>
                                        <td>
                                            <a class="label label-success" data-toggle="modal" data-target="#modal_edit" data-sid="{{ $data->id }}">Edit Status</a>
                                            <a class="label label-danger" data-toggle="modal" data-target="#modal_delete" data-sid="{{ $data->id }}">Delete Status</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">There are no statuses at present</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </section>

@endsection

@section('additional-js')

    <script src="{{ asset('js/bootstrap-datatable.js') }}"></script>
    <script src="{{ asset('js/data-table.js') }}"></script>

    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function () {
            $('#statusTable').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        });

        $(document).on('show.bs.modal','#modal_edit', function (e) {

            var sid = $(e.relatedTarget).data('sid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/settings/status-form') }}/"+ sid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_delete', function (e) {

            var sid = $(e.relatedTarget).data('sid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/settings/status-delete-form') }}/"+ sid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

    </script>

@endsection
