@extends('_layouts.admin')

@section('title', 'Settings: Permissions')

@section('content')

    @include('_sections.settings.permissions')
    @include('_modals.admin.settings.permissions.new')
    @include('_modals.admin.settings.permissions.edit')
    @include('_modals.admin.settings.permissions.delete')

    <section class="content">

        <div class="row">

            <div class="col-md-12">

                <ul>
                    <li>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_new">Add New Permission</button>
                    </li>
                </ul>

            <div class="box">

                <div class="box-body">

                    <table id="permissionTable" class="table table-bordered table-striped">

                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Display Name</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                        @if($permissions->count())
                            @foreach($permissions as $data)
                                <tr>
                                    <td>{{ $data->id }}</td>
                                    <td>{{ $data->display_name }}</td>
                                    <td>{{ $data->description }}</td>
                                    <td>
                                        <a class="label label-success" data-toggle="modal" data-target="#modal_edit" data-pid="{{ $data->id }}">Edit Permission</a>
                                        <a class="label label-danger" data-toggle="modal" data-target="#modal_delete" data-pid="{{ $data->id }}">Delete Permission</a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">There are no permissions at present</td>
                            </tr>
                        @endif
                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </section>

@endsection

@section('additional-js')

    <script src="{{ asset('js/bootstrap-datatable.js') }}"></script>
    <script src="{{ asset('js/data-table.js') }}"></script>

    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function () {
            $('#permissionTable').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        });

        $(document).on('show.bs.modal','#modal_edit', function (e) {

            var pid = $(e.relatedTarget).data('pid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/settings/permission-form') }}/"+ pid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_delete', function (e) {

            var pid = $(e.relatedTarget).data('pid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/settings/permission-delete-form') }}/"+ pid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

    </script>

@endsection