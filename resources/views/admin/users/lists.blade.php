@extends('_layouts.admin')

@section('title', 'Users')

@section('additional-css')
    <link href="{{ asset('css/data-table.css') }}" rel="stylesheet">
@endsection

@section('content')

    @include('_modals.admin.user.new')
    @include('_modals.admin.user.user_profile')
    @include('_modals.admin.user.edit_permissions')
    @include('_modals.admin.user.edit_role')
    @include('_modals.admin.user.delete')

    @include('_sections.users.lists')

    <section class="content">

        <div class="row">

            <div class="col-md-12">

                <ul>
                    <li>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_new">Add New User</button>
                    </li>
                </ul>

                <div class="box">

                    <div class="box-body">
                        <table id="userTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>User</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($users->count())
                                @foreach($users as $data)
                                    <tr>
                                        <td>{{ $data->id }}</td>
                                        <td>{{ $data->name }}</td>
                                        <td>{{ str_replace(array('["', '"]', '[', ']'), '', $data->roles->pluck('display_name')) }}</td>
                                        <td>
                                            @if($data->hasRole('user'))
                                            <a class="label label-warning" data-toggle="modal" data-target="#modal_user_profile" data-uid="{{ $data->id }}"><i class="fa fa-eye"></i></a>
                                            @endif
                                            <a class="label label-success" data-toggle="modal" data-target="#modal_edit_role" data-uid="{{ $data->id }}">Edit Role</a>
                                            <a class="label label-primary" data-toggle="modal" data-target="#modal_edit_permissions" data-uid="{{ $data->id }}">Edit Permissions</a>
                                            @if($data->hasRole('user'))
                                            <a href="{{ route('admin.login.as', $data->id) }}" class="label label-default">Login As</a>
                                            <a class="label label-danger" data-toggle="modal" data-target="#modal_delete" data-uid="{{ $data->id }}">Delete</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">There are no users at present</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </section>


@endsection

@section('additional-js')

    <script src="{{ asset('js/data-table.js') }}"></script>

    <script>

        $(function () {
            $('#userTable').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        });

        $(document).on('show.bs.modal','#modal_user_profile', function (e) {

            var uid = $(e.relatedTarget).data('uid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/user-form') }}/"+ uid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_edit_role', function (e) {

            var uid = $(e.relatedTarget).data('uid');

             $.ajax({
                 type : 'post',
                 url: "{{ url('/admin/user-role-form') }}/"+ uid ,
                 dataType: "html",
                 success : function(data){
                     $('.fetched-data').html(data);
                 }
            });
        });

        $(document).on('show.bs.modal','#modal_edit_permissions', function (e) {

            var uid = $(e.relatedTarget).data('uid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/user-permissions-form') }}/"+ uid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_delete', function (e) {

            var uid = $(e.relatedTarget).data('uid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/user-delete-form') }}/"+ uid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

    </script>


@endsection