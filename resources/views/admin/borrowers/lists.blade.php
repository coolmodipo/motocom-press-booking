@extends('_layouts.admin')

@section('title', 'Borrowers')

@section('additional-css')
    <link href="{{ asset('css/data-table.css') }}" rel="stylesheet">
@endsection

@section('content')

    @include('_modals.admin.borrower.new')
    @include('_modals.admin.borrower.edit')
    @include('_modals.admin.borrower.delete')

    @include('_sections.borrowers.lists')

    <section class="content">

        <div class="row">

            <div class="col-md-12">

                <ul>
                    <li>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal_new">Add New Borrowers</button>
                    </li>
                </ul>

                <div class="box">

                    <div class="box-body">
                        <table id="borrowersTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Organisation Name</th>
                                <th>Linked Company</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($borrowers->count())
                                @foreach($borrowers as $data)
                                    <tr>
                                        <td>{{ $data->id }}</td>
                                        <td>{{ $data->name }}</td>
                                        <td>{{ $data->organisation_name }}</td>
                                        <td>{{ $data->company->company_name }}</td>
                                        <td>
                                            <a class="label label-success" data-toggle="modal" data-target="#modal_edit" data-btid="{{ $data->id }}">Edit Borrowers</a>
                                            <a class="label label-danger" data-toggle="modal" data-target="#modal_delete" data-btid="{{ $data->id }}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">There are no Borrowers at present</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </section>

@endsection

@section('additional-js')

    <script src="{{ asset('js/data-table.js') }}"></script>

    <script>

        $(function () {
            $('#borrowersTable').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : true
            })
        });

        $(document).on('show.bs.modal','#modal_edit', function (e) {

            var btid = $(e.relatedTarget).data('btid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/borrower-form') }}/"+ btid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_delete', function (e) {

            var btid = $(e.relatedTarget).data('btid');

            $.ajax({
                type : 'post',
                url: "{{ url('/admin/borrower-delete-form') }}/"+ btid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

    </script>

@endsection