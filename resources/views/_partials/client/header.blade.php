<?php

//echo '<pre>';
//
////print_r($user->client->unreadMessages);
//
//foreach($user->client->unreadMessages  as $messages){
//
//        print_r($messages->sender->id);
//
//}
//exit;

?>


<nav class="navbar navbar-default navbar-static-top m-b-0">

    <div class="navbar-header">
        <div class="top-left-part">

            <a class="logo" href="{{ route('client.index') }}">
                @if($user->client->company)
                <b>
                    <img src="{{ $user->client->company->image_url }}" alt="{{ $user->client->company->company_name }} Logo" class="light-logo" height="35" width="35" />
                </b>
                <span class="hidden-xs">
                    <span class="company_text">{{ $user->client->company->company_name }}</span>
                 </span>
                @elseif($user->client->userCompany->first())
                    <b>
                        <img src="{{ $user->client->userCompany->first()->image_url }}" alt="{{ $user->client->userCompany->first()->company_name }} Logo" class="light-logo" height="35" width="35" />
                    </b>
                    <span class="hidden-xs">
                <span class="company_text">{{ $user->client->userCompany->first()->company_name }}</span>
                </span>
                @else
                <b>
                    <img src="{{ asset('images/company/motocom.jpg') }}" alt="Motocom Logo" class="light-logo" height="35" width="35" />
                </b>
                <span class="hidden-xs">
                <span class="company_text">{{ $user->name }}</span>
                </span>
                @endif
            </a>
        </div>

        <ul class="nav navbar-top-links navbar-left">
            <li><a href="javascript:void(0)" class="open-close waves-effect waves-light"><i class="ti-menu"></i></a></li>
            @if($user->client->userCompany->count())
            <li class="dropdown">
                <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"> <i class="mdi mdi-gmail"></i>
                    @if($user->client->unreadMessages->count())
                    <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                    @endif
                </a>
                @if($user->client->unreadMessages->count())
                <ul class="dropdown-menu mailbox animated bounceInDown">
                    <li>
                        <div class="drop-title">You have {{ $user->client->unreadMessages->count() }} new messages</div>
                    </li>
                    <li>
                        <div class="message-center">
                            @foreach($user->client->unreadMessages  as $message)

                            <a href="{{ route('client.show.message', $message->slug) }}">
                                <div class="user-img">
                                    <img src="{{ $message->sender->image_url }}" alt="user" class="img-circle">
                                    <span class="profile-status online pull-right"></span>
                                </div>
                                <div class="mail-contnet">
                                    <h5>{{ $message->sender->user->name }}</h5>
                                    <span class="mail-desc">{{ $message->title }}</span>
                                    <span class="time">{{ $message->created_at->format('h:i A') }} {{ $message->created_at->format('d/m/Y') }}</span>
                                </div>
                            </a>
                            @endforeach
                        </div>
                    </li>
                    <li>
                        <a class="text-center" href="{{route('client.messages', 'inbox')}}"> <strong>See all Messages</strong> <i class="fa fa-angle-right"></i> </a>
                    </li>
                </ul>
                @endif
            </li>
            @endif
            <li class="dropdown">
                <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"> <i class="mdi mdi-check-circle"></i>
                    {{--<div class="notify"><span class="heartbit"></span><span class="point"></span></div>--}}
                </a>
                <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                    <li>
                        <a href="#">
                            <div>
                                <p> <strong>Task 1</strong> <span class="pull-right text-muted">40% Complete</span> </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <p> <strong>Task 2</strong> <span class="pull-right text-muted">20% Complete</span> </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"> <span class="sr-only">20% Complete</span> </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <p> <strong>Task 3</strong> <span class="pull-right text-muted">60% Complete</span> </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%"> <span class="sr-only">60% Complete (warning)</span> </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <p> <strong>Task 4</strong> <span class="pull-right text-muted">80% Complete</span> </p>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"> <span class="sr-only">80% Complete (danger)</span> </div>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="text-center" href="#"> <strong>See All Tasks</strong> <i class="fa fa-angle-right"></i> </a>
                    </li>
                </ul>
            </li>
            @if(Session::has('adminUser'))
            <li class="adminUser"><form id="logBack" action="{{ route('client.logback.as', Session::get('adminUser')) }}"><button type="submit" class="btn btn-block btn-info btn-rounded">Admin</button></form></li>
            @endif
        </ul>
        <ul class="nav navbar-top-links navbar-right pull-right">

            <li class="dropdown">
                <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="{{ $user->client->image_url }}" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">{{ $user->client->firstname }}</b><span class="caret"></span> </a>
                <ul class="dropdown-menu dropdown-user animated flipInY">
                    <li>
                        <div class="dw-user-box">
                            <div class="u-img">
                                <img src="{{ $user->client->image_url }}" alt="user" />
                            </div>
                            <div class="u-text">
                                <h4>{{ $user->name }}</h4>
                                <p class="text-muted">{{ $user->email }}</p>
                                <a href="{{ route('client.profile') }}" class="btn btn-rounded btn-danger btn-sm">View Profile</a>
                            </div>
                        </div>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li><a class="account_settings" data-toggle="modal" data-target="#modal_account_settings"><i class="ti-settings"></i> Account Setting</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ route('user.logout') }}"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>

            </li>

        </ul>
    </div>

</nav>