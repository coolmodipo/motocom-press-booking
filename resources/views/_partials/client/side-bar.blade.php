<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
        <ul class="nav" id="side-menu">
            <li>
                <a href="{{ route('client.index') }}" class="waves-effect {{ Request::is('client') ? 'active' : '' }}"><i class="mdi mdi-av-timer fa-fw"></i> <span class="hide-menu"> Dashboard</span></a>
            </li>
            @if($user->client->userCompany->count())
            <li>
                <a href="{{ route('client.messages', 'inbox') }}" class="waves-effect {{ Request::is('client/message*') ? 'active' : '' }}"><i class="mdi mdi-gmail fa-fw" data-icon="v"></i> <span class="hide-menu"> Messages</span></a>
            </li>

            <li class="devider"></li>
            <li>
                <a href="{{ route('client.models') }}" class="waves-effect {{ Request::is('client/model*') ? 'active' : '' }}"><i class="mdi mdi-engine fa-fw" data-icon="v"></i> <span class="hide-menu"> Models</span></a>
            </li>
            <li>
                <a href="{{ route('client.fleets') }}" class="waves-effect {{ Request::is('client/fleet*') ? 'active' : '' }}"><i class="mdi mdi-alphabetical fa-fw" data-icon="v"></i> <span class="hide-menu"> Fleets</span></a>
            </li>
            <li>
                <a href="{{ route('client.bookings.index') }}" class="waves-effect {{ Request::is('client/bookings') ? 'active' : '' }}"><i class="mdi mdi-book-open-variant fa-fw" data-icon="v"></i> <span class="hide-menu"> Bookings</span></a>
            </li>
                <li>
                    <a href="{{ route('client.borrowers.index', $user->client->company->id) }}" class="waves-effect {{ Request::is('client/borrower*') ? 'active' : '' }}"><i class="mdi mdi-account-multiple-plus fa-fw" data-icon="v"></i> <span class="hide-menu"> Borrowers</span></a>
                </li>
            @endif
            <li class="devider"></li>
            <li> <a href="#" class="waves-effect"><i class="mdi mdi-content-copy fa-fw"></i><span class="hide-menu">Documentation</span></a> </li>

        </ul>
    </div>
</div>
