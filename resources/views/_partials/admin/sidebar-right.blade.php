<aside class="control-sidebar control-sidebar-dark">

    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li class="active">
            <a href="#control-sidebar-settings-tab" data-toggle="tab">
                <i class="fa fa-gears"></i> Settings
            </a>
        </li>
    </ul>

    <div class="tab-content">

        <div class="tab-pane active" id="control-sidebar-settings-tab">


            <p><a href="#" data-toggle="control-sidebar">Collapse</a></p>

            <h3 class="control-sidebar-heading">General Settings</h3>

            <div class="form-group">
                <label class="control-sidebar-subheading">
                    <a class="white" href="{{ route('admin.settings.roles') }}">User Roles</a>
                </label>
                <small>Create / Update user roles.</small>
            </div>

            <div class="form-group">
                <label class="control-sidebar-subheading">
                    <a class="white" href="{{ route('admin.settings.permissions') }}">User Permissions</a>
                </label>
                <small>Create / Update user permissions.</small>
            </div>

            <div class="form-group">
                <label class="control-sidebar-subheading">
                    <a class="white" href="{{ route('admin.settings.status') }}">Status</a>
                </label>
                <small>Create / Update Statuses.</small>
            </div>

            <div class="form-group">
                <label class="control-sidebar-subheading">
                    <a class="white" href="{{ route('admin.settings.colours') }}">Colours</a>
                </label>
                <small>Create / Update Colours.</small>
            </div>

            <div class="form-group">
                <label class="control-sidebar-subheading">
                    <a class="white" href="{{ route('admin.settings.insurances') }}">Insurance</a>
                </label>
                <small>Create / Update Insurance.</small>
            </div>

        </div>

    </div>

</aside>
