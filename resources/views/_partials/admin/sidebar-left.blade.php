<aside class="main-sidebar">

    <section class="sidebar">

        <ul class="sidebar-menu" data-widget="tree">

            <li class="{{ Request::is('admin') ? 'active' : '' }}">
                <a href="{{ route('admin.index') }}" >
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>

            <li class="{{ Request::is('admin/user*') ? 'menu-open active' : '' }}">
                <a href="{{ route('admin.users') }}">
                    <i class="fa fa-users"></i>
                    <span>Users</span>
                </a>
            </li>

            <li class="{{ Request::is('admin/companies*') ? 'menu-open active' : '' }}">
                <a href="{{ route('admin.companies') }}">
                    <i class="fa fa-building"></i>
                    <span>Companies</span>
                </a>
            </li>

            <li class="{{ Request::is('admin/borrowers*') ? 'menu-open active' : '' }}">
                <a href="{{ route('admin.borrowers') }}">
                    <i class="fa fa-sign-out"></i>
                    <span>Borrowers</span>
                </a>
            </li>

            <li class="{{ Request::is('admin/bookingtype*') ? 'menu-open active' : '' }}">
                <a href="{{ route('admin.bookingtypes') }}">
                    <i class="fa fa-bookmark"></i>
                    <span>Booking Type</span>
                </a>
            </li>

            <li class="{{ Request::is('admin/categories*') ? 'menu-open active' : '' }}">
                <a href="{{ route('admin.categories') }}">
                    <i class="fa fa-tags"></i>
                    <span>Category</span>
                </a>
            </li>

            <li class="{{ Request::is('admin/vehicle*') ? 'menu-open active' : '' }}">
                <a href="{{ route('admin.vehicles') }}">
                    <i class="fa fa-bicycle"></i>
                    <span>Models</span>
                </a>
            </li>

            <li class="treeview {{ Request::is('admin/fleet*') ? 'menu-open active' : '' }}">
                <a href="#">
                    <i class="fa fa-cubes"></i>
                    <span>Fleet</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                <li class="{{ Request::is('admin/fleets') ? 'active' : '' }}"><a href="{{ route('admin.fleets') }}"><i class="fa fa-circle-o"></i> All</a></li>
                <?php $fleetCompanies = \App\Company::all();?>
                @if($fleetCompanies)
                    @foreach($fleetCompanies as $fc)
                        <li class="{{ Request::is('admin/fleets/'.$fc->slug) ? 'active' : '' }}"><a href="{{ route('admin.fleets.company', $fc->slug) }}"><i class="fa fa-circle-o"></i>{{ $fc->company_name }}</a></li>
                    @endforeach
                @endif

                </ul>
            </li>


            {{--<li class="treeview {{ Request::is('admin/user*') ? 'menu-open active' : '' }}">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-users"></i>--}}
                    {{--<span>Users</span>--}}
                    {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li class="{{ Request::is('admin/users') ? 'active' : '' }}"><a href="{{ route('admin.users') }}"><i class="fa fa-circle-o"></i> Users</a></li>--}}
                    {{--<li class="{{ Request::is('admin/user/roles') ? 'active' : '' }}"><a href="{{ route('admin.roles') }}"><i class="fa fa-circle-o"></i> Role</a></li>--}}
                    {{--<li class="{{ Request::is('admin/user/permissions') ? 'active' : '' }}"><a href="{{ route('admin.permissions') }}"><i class="fa fa-circle-o"></i> Permission</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
        </ul>
    </section>

</aside>