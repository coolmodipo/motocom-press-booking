@if($user->hasRole('user'))
    <img src="{{ $user->client->image_url }}" class="user-image" alt="{{ $user->name }} Image">
@endif