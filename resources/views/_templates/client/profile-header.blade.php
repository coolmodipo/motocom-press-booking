<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Profile page</h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

         <ol class="breadcrumb">
            <li><a href="{{ route('client.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-user"></i> Profile page</li>
        </ol>
    </div>
</div>