<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title pull-left">Borrowers</h4>
        @if(Request::is('client/borrowers/*'))
            <div class="pull-right">
                <button data-toggle="modal" data-target="#modal_add" class="btn btn-block btn-success btn-rounded">Add New Borrower </button>
            </div>
        @endif
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="{{ route('client.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            @if(Request::is('client/borrower/*'))
                <li><a href="{{ route('client.borrowers') }}"><i class="mdi mdi-alphabetical"></i> Borrowers</a></li>
                <li class="active"><i class="mdi mdi-alpha"></i> Name: <strong></strong></li>
            @else
                <li class="active"><i class="mdi mdi-alphabetical"></i> Fleets</li>
            @endif
        </ol>
    </div>
</div>
