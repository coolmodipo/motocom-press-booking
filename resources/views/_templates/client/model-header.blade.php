<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title pull-left">Models</h4>
        @if(Request::is('client/models'))
        <div class="pull-right">
            <button data-toggle="modal" data-target="#modal_add" class="btn btn-block btn-success btn-rounded">Add New Model</button>
        </div>
        @endif
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="{{ route('client.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            @if(Request::is('client/models/*'))
                <li><a href="{{ route('client.models') }}"><i class="mdi mdi-engine"></i> Models</a></li>
                <li class="active"><i class="mdi mdi-engine-outline"></i> Model</li>
            @elseif(Request::is('client/model/*'))
                <li><a href="{{ route('client.models') }}"><i class="mdi mdi-engine"></i> Models</a></li>
                <li class="active"><i class="mdi mdi-calendar-clock"></i> Model Availability: <strong>{{ $model->name }}</strong></li>
            @else
                <li class="active"><i class="mdi mdi-engine"></i> Models</li>
            @endif

        </ol>
    </div>
</div>