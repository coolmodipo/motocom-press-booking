<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Message Mailbox</h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="{{ route('client.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            @if(Request::is('client/message/*'))
                <li><a href="{{ route('client.messages', 'inbox') }}"><i class="fa fa-envelope"></i> Messages</a></li>
                <li class="active"><i class="fa fa-envelope-o"></i> Message</li>
            @else
                <li class="active"><i class="fa fa-envelope"></i> Messages</li>
            @endif

        </ol>
    </div>
</div>