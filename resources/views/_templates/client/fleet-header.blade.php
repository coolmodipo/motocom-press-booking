<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title pull-left">Fleet</h4>
        @if(Request::is('client/fleets'))
        <div class="pull-right">
          <button data-toggle="modal" data-target="#modal_add" class="btn btn-block btn-success btn-rounded">Add New Fleet </button>
        </div>
       @endif
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="{{ route('client.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            @if(Request::is('client/fleet/*'))
                <li><a href="{{ route('client.fleets') }}"><i class="mdi mdi-alphabetical"></i> Fleets</a></li>
                <li class="active"><i class="mdi mdi-alpha"></i> Fleet: <strong>{{ $fleet->reg_number }}</strong></li>
            @else
                <li class="active"><i class="mdi mdi-alphabetical"></i> Fleets</li>
            @endif
        </ol>
    </div>
</div>
