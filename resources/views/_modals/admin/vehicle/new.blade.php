<div class="modal fade" id="modal_new">

    <div class="modal-dialog">

        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'admin.vehicle.create', 'files' => 'true', 'id' => 'post-form-vehicle', 'class' => 'form-horizontal']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Model</h4>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('image', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                    {!! Form::file('image', ['class' => 'field']) !!}
                    <small>350px X 350px</small>
                    </div>

                </div>

                <div class="form-group">
                    {!! Form::label('Name', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Vehicle Name', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Category', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('category_id', App\Category::pluck('name', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Select Vehicle Category', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Company', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('company_id', App\Company::pluck('company_name', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Select Company/ Make', 'required']) !!}
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Model</button>
            </div>

            {!! Form::close() !!}

        </div>

    </div>

</div>