<div class="modal fade" id="modal_new">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'admin.borrower.create', 'id' => 'post-form-borrower', 'class' => 'form-horizontal']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Borrower</h4>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('Name', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Organisation Name', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('organisation_name', null, ['class' => 'form-control', 'placeholder' => 'Organisation Name', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Address', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Organisation Address', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Postcode', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('postcode', null, ['class' => 'form-control', 'placeholder' => 'Organisation Postcode', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Telephone', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('telephone', null, ['class' => 'form-control', 'placeholder' => 'Organisation Telephone', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Email', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Organisation Email', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Linked Company', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('company_id', App\Company::pluck('company_name', 'id') , null, ['class' => 'form-control', 'placeholder' => 'Select Company', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Home Address', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('home_address', null, ['class' => 'form-control', 'placeholder' => 'Home Address']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Home Postcode', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('home_postcode', null, ['class' => 'form-control', 'placeholder' => 'Home Postcode']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Home Telephone', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('home_telephone', null, ['class' => 'form-control', 'placeholder' => 'Home Telephone']) !!}
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Borrower</button>
            </div>

            {!! Form::close() !!}

        </div>

    </div>

</div>