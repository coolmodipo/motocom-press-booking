<div class="modal fade" id="modal_new">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'admin.register.user', 'files' => TRUE, 'id' => 'post-form', 'class' => 'form-horizontal']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New User</h4>
            </div>

            <div class="modal-body">

                    <div class="form-group">
                        {!! Form::label('firstname', NULL, ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                             {!! Form::text('firstname', null, ['class' => 'form-control', 'placeholder' => 'User Firstname', 'required']) !!}
                            @if ($errors->has('firstname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('firstname') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('lastname', NULL, ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::text('lastname', null, ['class' => 'form-control', 'placeholder' => 'User Lastname', 'required']) !!}
                            @if ($errors->has('lastname'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('roles', NULL, ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::select('roles', App\Role::pluck('display_name', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Select Role', 'required']) !!}
                            @if ($errors->has('roles'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('roles') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('email', NULL, ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'User Email', 'required']) !!}
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('password', NULL, ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'User Password']) !!}
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('confirm password', NULL, ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::password('password-confirm', ['class' => 'form-control', 'placeholder' => ' Confirm User Password']) !!}
                        </div>
                    </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save User</button>
            </div>


            {!! Form::close() !!}

        </div>

    </div>

</div>