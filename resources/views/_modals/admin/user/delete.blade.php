<div class="modal fade" id="modal_delete">

    <div class="modal-dialog">

        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'admin.user.delete', 'id' => 'post-form-delete', 'class' => 'form-horizontal']) !!}

            <div class="fetched-data"></div>

            {!! Form::close() !!}

        </div>

    </div>

</div>