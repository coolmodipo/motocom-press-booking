<div class="modal fade" id="modal_edit">

    <div class="modal-dialog">

        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'admin.settings.permission.update', 'id' => 'post-form-settings-permission', 'class' => 'form-horizontal']) !!}

            <div class="fetched-data"></div>

            {!! Form::close() !!}

        </div>

    </div>

</div>