<div class="modal fade" id="modal_new">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'admin.settings.permission.create', 'id' => 'post-form', 'class' => 'form-horizontal']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Permission</h4>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('Display Name', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('display_name', null, ['class' => 'form-control', 'placeholder' => 'Display Name', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Description', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description', 'required']) !!}
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save Permission</button>
            </div>


            {!! Form::close() !!}

        </div>

    </div>

</div>