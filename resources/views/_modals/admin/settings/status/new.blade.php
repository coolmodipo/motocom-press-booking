<div class="modal fade" id="modal_new">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'admin.settings.status.create', 'id' => 'post-form-status', 'class' => 'form-horizontal']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Status</h4>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('Title', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Status Name', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Colour', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('class', ['success' => 'Green', 'info' => 'Blue', 'warning' => 'Orange', 'default' => 'Grey', 'danger' => 'Red'], null, ['class' => 'form-control']) !!}
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Status</button>
            </div>

            {!! Form::close() !!}

        </div>

    </div>

</div>
