<div class="modal fade" id="modal_new">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'admin.settings.colour.create', 'id' => 'post-form-colour', 'class' => 'form-horizontal']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Colour</h4>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('Name', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Colour Name', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Hex Colour', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('hex', null, ['class' => 'form-control', 'placeholder' => 'Colour Hex']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Company', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('company_id', App\Company::pluck('company_name', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Select Company', 'required']) !!}
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Colour</button>
            </div>

            {!! Form::close() !!}

        </div>

    </div>

</div>