<div class="modal fade" id="modal_delete">

    <div class="modal-dialog">

        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'admin.settings.insurance.delete', 'id' => 'post-form-insurance-delete', 'class' => 'form-horizontal']) !!}

            <div class="fetched-data"></div>

            {!! Form::close() !!}

        </div>

    </div>

</div>
