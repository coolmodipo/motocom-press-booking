<div class="modal fade" id="modal_new">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'admin.settings.insurance.create', 'id' => 'post-form-insurance', 'class' => 'form-horizontal']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Insurance</h4>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('Name', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Insurance Name', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Name', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('company_id', App\Company::pluck('company_name', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Select Company', 'required']) !!}
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Insurance</button>
            </div>

            {!! Form::close() !!}

        </div>

    </div>

</div>
