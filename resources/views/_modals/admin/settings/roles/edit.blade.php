<div class="modal fade" id="modal_edit">

    <div class="modal-dialog">

        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'admin.settings.role.update', 'id' => 'post-form-settings-role', 'class' => 'form-horizontal']) !!}

            <div class="fetched-data"></div>

            {!! Form::close() !!}

        </div>

    </div>

</div>