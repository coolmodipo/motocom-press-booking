<div class="modal fade" id="modal_new">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'admin.bookingtype.create', 'id' => 'post-form-bookingtype', 'class' => 'form-horizontal']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Booking Type</h4>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('Name', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Booking Name', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Colour', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('hex_code', null, ['class' => 'form-control', 'placeholder' => 'Background Colour', 'required']) !!}
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Booking Type</button>
            </div>

            {!! Form::close() !!}

        </div>

    </div>

</div>