<div class="modal fade" id="modal_edit">

    <div class="modal-dialog">

        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'admin.update.bookingtype', 'id' => 'post-form-bookingtype-update', 'class' => 'form-horizontal']) !!}

            <div class="fetched-data"></div>

            {!! Form::close() !!}

        </div>

    </div>

</div>