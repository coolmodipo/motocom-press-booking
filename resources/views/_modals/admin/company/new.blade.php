<div class="modal fade" id="modal_new">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'admin.register.company', 'id' => 'post-form', 'class' => 'form-horizontal']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Company</h4>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('company name', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('company_name', null, ['class' => 'form-control', 'placeholder' => 'Company Name', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Admin', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('client_id', App\Client::join('users', 'clients.user_id', '=','users.id')->pluck('users.name', 'clients.id'), null, ['class' => 'form-control', 'placeholder' => 'Select Client', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Address 1', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('address_1', null, ['class' => 'form-control', 'placeholder' => 'Company Address Line 1', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Address 2', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('address_2', null, ['class' => 'form-control', 'placeholder' => 'Company Address Line 2', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('City', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'Company City', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('County', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('county', null, ['class' => 'form-control', 'placeholder' => 'Company County', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Postcode', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('postcode', null, ['class' => 'form-control', 'placeholder' => 'Company Postcode', 'required']) !!}
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add Company</button>
            </div>

            {!! Form::close() !!}

        </div>

    </div>

</div>