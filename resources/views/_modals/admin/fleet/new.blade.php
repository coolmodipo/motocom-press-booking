

<div class="modal fade" id="modal_new">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'admin.fleet.create', 'id' => 'post-form-fleet', 'class' => 'form-horizontal']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New fleet</h4>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('Reg Number:', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('reg_number', null, ['class' => 'form-control', 'placeholder' => 'Registration Number', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Vehicle:', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('vehicle_id', (Route::current()->getName() == 'admin.fleets.company' ? App\Vehicle::whereHas('company', function($q) use($company){ $q->where('id', '=', $company->id); })->pluck('name', 'id') : App\Vehicle::pluck('name', 'id') ), null, ['class' => 'form-control', 'placeholder' => 'Select Vehicle', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Frame Number:', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('frame_number', null, ['class' => 'form-control', 'placeholder' => 'Frame Number', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Engine:', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('engine', null, ['class' => 'form-control', 'placeholder' => 'Engine Type', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Colour:', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('colour_id', App\Colour::pluck('name', 'id') , null, ['class' => 'form-control', 'placeholder' => 'Select Colour', 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Accessories:', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                    {!! Form::textarea('accessories', NULL, ['class' => 'form-control', 'placeholder' => 'Accessories']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Notes:', NULL, ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                    {!! Form::textarea('notes', NULL, ['class' => 'form-control', 'placeholder' => 'Additional Notes']) !!}
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Add fleet</button>
            </div>

            {!! Form::close() !!}

        </div>

    </div>

</div>