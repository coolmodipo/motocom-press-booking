<div id="modal_reply" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'client.message.reply', 'id' => 'post-form-company-add-user']) !!}

            <div class="fetched-data"></div>

            {!! Form::close() !!}
        </div>
    </div>

</div>
