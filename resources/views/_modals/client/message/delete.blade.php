<div id="modal_delete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'client.company.add.user', 'id' => 'post-form-company-add-user']) !!}

            <div class="fetched-data"></div>

            {!! Form::close() !!}
        </div>
    </div>

</div>
