<div id="modal_create" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'client.message.create', 'id' => 'post-form-company-add-user']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Create New Message</h4>
            </div>

            <div class="modal-body">

                <input type="hidden" name="sender_id" value="{{ $user->client->id }}">

                <div class="form-group">
                    {!! Form::label('Receipient:') !!}
                    @if($user->client->company)
                        <select class="form-control" required="" name="client_id">
                            <option value="">Select Client</option>
                            @foreach($user->client->company->first()->clients as $client)
                                @if($client->id != $user->client->id)
                                    <option value="{{ $client->id }}">{{ $client->user->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    @elseif($user->client->userCompany->count())
                        <select class="form-control" required="" name="client_id">
                            <option value="">Select Client</option>
                            @foreach($user->client->userCompany->first()->clients as $client)
                                @if($client->id != $user->client->id)
                                    <option value="{{ $client->id }}">{{ $client->user->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    @endif

                </div>

                <div class="form-group">
                    {!! Form::label('Title:') !!}
                    {!! Form::text('title', NULL, ['class' => 'form-control', 'placeholder' => 'Message Title', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Message Body:') !!}
                    {!! Form::textarea('message', NULL, ['class' => 'form-control', 'placeholder' => 'Message Body', 'required']) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Send Mail</button>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

</div>
