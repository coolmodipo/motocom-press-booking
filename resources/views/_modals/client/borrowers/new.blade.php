<div id="modal_add" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'client.borrower.create', 'id' => 'post-form-fleet-book']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">New Borrower</h4>
            </div>

            <div class="modal-body">

                    <input type="hidden" name="company_id" value="{!! Auth::user()->client->company->id !!}">

                    <div class="form-group">
                        {!! Form::label('Name', NULL, ['class' => 'col-md-4 control-label']) !!}
                        {!! Form::text('name', NULL, ['class' => 'form-control', 'placeholder' => 'Name', 'required']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Organisation Name', NULL, ['class' => 'col-md-4 control-label']) !!}
                        {!! Form::text('organisation_name', NULL, ['class' => 'form-control', 'placeholder' => 'Organisation Name', 'required']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Address', NULL, ['class' => 'col-md-4 control-label']) !!}
                        {!! Form::text('address', NULL, ['class' => 'form-control', 'placeholder' => 'Organisation Address', 'required']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Postcode', NULL, ['class' => 'col-md-4 control-label']) !!}
                        {!! Form::text('postcode', NULL, ['class' => 'form-control', 'placeholder' => 'Organisation Postcode', 'required']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Telephone', NULL, ['class' => 'col-md-4 control-label']) !!}
                        {!! Form::text('telephone', NULL, ['class' => 'form-control', 'placeholder' => 'Organisation Telephone', 'required']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Email', NULL, ['class' => 'col-md-4 control-label']) !!}
                        {!! Form::text('email', NULL, ['class' => 'form-control', 'placeholder' => 'Organisation Email', 'required']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Home Address', NULL, ['class' => 'col-md-4 control-label']) !!}
                        {!! Form::text('home_address', NULL, ['class' => 'form-control', 'placeholder' => 'Home Address']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Home Postcode', NULL, ['class' => 'col-md-4 control-label']) !!}
                        {!! Form::text('home_postcode', NULL, ['class' => 'form-control', 'placeholder' => 'Home Postcode']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Home Telephone', NULL, ['class' => 'col-md-4 control-label']) !!}
                        {!! Form::text('home_telephone', NULL, ['class' => 'form-control', 'placeholder' => 'Home Telephone']) !!}
                    </div>               

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Create Borrower</button>
            </div>

            {!! Form::close() !!}

        </div>
    </div>

</div>