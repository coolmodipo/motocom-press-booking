<div id="modal_add" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'client.fleet.create', 'id' => 'post-form-fleet-book']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Create New Fleet</h4>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('Reg Number:') !!}
                    {!! Form::text('reg_number', null, ['class' => 'form-control', 'placeholder' => 'Registration Number', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Vehicle:') !!}
                    {!! Form::select('vehicle_id', App\Vehicle::where(['company_id' => $company->id])->pluck('name', 'id') , null, ['class' => 'form-control', 'placeholder' => 'Select Model', 'required']) !!}
                </div>


                <div class="form-group">
                    {!! Form::label('Frame Number:') !!}
                    {!! Form::text('frame_number', null, ['class' => 'form-control', 'placeholder' => 'Frame Number', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Engine:') !!}
                    {!! Form::text('engine', null, ['class' => 'form-control', 'placeholder' => 'Engine Type', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Colour:') !!}
                    {!! Form::select('colour_id', App\Colour::where(['company_id' => $company->id])->pluck('name', 'id') , null, ['class' => 'form-control', 'placeholder' => 'Select Colour', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Accessories:') !!}
                    {!! Form::textarea('accessories', null, ['class' => 'form-control', 'placeholder' => 'Accessories']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Notes:') !!}
                    {!! Form::textarea('notes', null, ['class' => 'form-control', 'placeholder' => 'Additional Notes']) !!}
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Create Fleet</button>
            </div>

            {!! Form::close() !!}

        </div>
    </div>

</div>