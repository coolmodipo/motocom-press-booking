<div id="modal_return" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'client.fleet.return', 'id' => 'post-form-fleet-return']) !!}

            <div class="fetched-data"></div>

            {!! Form::close() !!}

        </div>
    </div>

</div>
