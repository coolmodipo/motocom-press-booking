<div id="modal_returned" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'client.fleet.returned', 'id' => 'post-form-fleet-returned']) !!}

            <div class="fetched-data"></div>

            {!! Form::close() !!}

        </div>
    </div>

</div>
