<div id="modal_new_book" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'client.fleet.book', 'id' => 'post-form-fleet-book']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Booking Form</h4>
            </div>

            <div class="modal-body">

                <input type="hidden" name="status_id" value="3"/>
                <input type="hidden" name="vehicle_id" value="{{ $model->id }}"/>

                <div class="form-group">
                    {!! Form::label('Reg Number:') !!}
                    <select class="form-control" required="" name="fleet_id">
                        <option selected="selected" value="">Select Registration</option>
                        @foreach($model->fleets as  $fleet)
                            @if(!in_array($fleet->id, $faulty) && !in_array($fleet->id, $unavailable))
                                <option value="{{ $fleet->id }}">{{ $fleet->reg_number }} - {{ $fleet->colour->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    {!! Form::label('Name:') !!}
                    {!! Form::select('borrower_id', App\Borrower::where(['company_id' => $user->client->userCompany->first()->id])->pluck('name', 'id') , null, ['class' => 'form-control', 'placeholder' => 'Select Borrower', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Booking Type:') !!}
                    {!! Form::select('bookingtype_id', App\Bookingtype::pluck('name', 'id') , null, ['class' => 'form-control', 'placeholder' => 'Select Booking Type', 'required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Start Date:') !!}
                    <div class="input-group">
                        {!! Form::text('start_date', null, ['class' => 'form-control', 'id' => 'datepicker-autoclose-start-date', 'placeholder' => 'mm/dd/yyyy', 'required']) !!}
                        <span class="input-group-addon"><i class="icon-calender"></i></span>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('End Date:') !!}
                    <div class="input-group">
                        {!! Form::text('end_date', null, ['class' => 'form-control', 'id' => 'datepicker-autoclose-end-date', 'placeholder' => 'mm/dd/yyyy', 'required']) !!}
                        <span class="input-group-addon"><i class="icon-calender"></i></span>
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('Insurance:') !!}
                    {!! Form::select('insurance_id', App\Insurance::where(['company_id' => $user->client->userCompany->first()->id])->orWhere(['id' => 1])->pluck('name', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Select Insurance']) !!}
                </div>

                <div class="form-group col-md-8">
                    {!! Form::label('Collector:') !!}
                    {!! Form::text('collector', null, ['class' => 'form-control', 'placeholder' => 'Person collecting', 'required']) !!}
                </div>

                <div class="form-group col-md-4">
                    {!! Form::label('Pickup Time:') !!}
                    <div class="input-group clockpicker">
                        {!! Form::text('collector_time', '09:30', ['class' => 'form-control', 'id' => 'single-input-start', 'placeholder' => 'HH:MM:SS', 'required']) !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>

                <div class="form-group col-md-8">
                    {!! Form::label('Returnee:') !!}
                    {!! Form::text('returnee', null, ['class' => 'form-control', 'placeholder' => 'Person returning', 'required']) !!}
                </div>

                <div class="form-group col-md-4">
                    {!! Form::label('Return Time:') !!}
                    <div class="input-group clockpicker">
                        {!! Form::text('returnee_time', '18:00', ['class' => 'form-control', 'id' => 'single-input-end', 'placeholder' => 'HH:MM:SS', 'required']) !!}
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-time"></span>
                        </span>
                    </div>
                </div>

                {{--<div class="form-group">--}}
                    {{--{!! Form::label('Mileage:') !!}--}}
                    {{--{!! Form::text('mileage_out', null, ['class' => 'form-control', 'placeholder' => 'Item Miles', 'required']) !!}--}}
                {{--</div>--}}

                <div class="form-group">
                    {!! Form::label('Special Request:') !!}
                    {!! Form::textarea('special_request', null, ['class' => 'form-control', 'placeholder' => 'Special Request']) !!}
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Book Fleet</button>
            </div>

            {!! Form::close() !!}

        </div>
    </div>

</div>
