<div id="modal_addAccessory" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'client.fleet.add.accessory', 'id' => 'post-form-fleet-accessory-add']) !!}

            <input type="hidden" name="fleet_id" value="{{ $fleet->id }}">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Accessory</h4>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('Accessories:') !!}
                    {!! Form::textarea('old_accessories', strip_tags($fleet->accessories), ['class' => 'form-control', 'placeholder' => 'Accessories', 'style' => 'white-space: pre-line', 'disabled']) !!}
                </div>

                <div class="form-group">
                    {!! Form::textarea('accessories', null, ['class' => 'form-control', 'placeholder' => 'Accessories', 'required']) !!}
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Add</button>
            </div>

            {!! Form::close() !!}

        </div>
    </div>

</div>