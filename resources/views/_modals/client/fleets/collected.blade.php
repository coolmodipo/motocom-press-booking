<div id="modal_collected" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'client.fleet.collected', 'id' => 'post-form-fleet-collected']) !!}

            <div class="fetched-data"></div>

            {!! Form::close() !!}

        </div>
    </div>

</div>
