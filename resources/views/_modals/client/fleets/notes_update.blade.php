<div id="modal_updateNote" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'client.fleet.update.note', 'id' => 'post-form-fleet-note-add']) !!}

            <input type="hidden" name="fleet_id" value="{{ $fleet->id }}">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Update Notes</h4>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('Note:') !!}
                    {!! Form::textarea('notes', strip_tags($fleet->notes), ['class' => 'form-control', 'placeholder' => 'Notes', 'required']) !!}
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
            </div>

            {!! Form::close() !!}

        </div>
    </div>

</div>