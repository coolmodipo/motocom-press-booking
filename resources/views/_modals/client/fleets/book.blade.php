<div id="modal_book" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'client.fleet.book', 'id' => 'post-form-fleet-book']) !!}

                <div class="fetched-data"></div>

            {!! Form::close() !!}

        </div>
    </div>

</div>
