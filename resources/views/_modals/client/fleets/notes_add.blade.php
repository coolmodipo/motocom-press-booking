<div id="modal_addNote" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'client.fleet.add.note', 'id' => 'post-form-fleet-note-add']) !!}

            <input type="hidden" name="fleet_id" value="{{ $fleet->id }}">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add Note</h4>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('Note:') !!}
                    {!! Form::textarea('older_notes',  strip_tags($fleet->notes), ['class' => 'form-control', 'placeholder' => 'Notes', 'disabled']) !!}
                </div>

                <div class="form-group">
                    {!! Form::textarea('notes', null, ['class' => 'form-control', 'placeholder' => 'Notes', 'required']) !!}
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Add</button>
            </div>

            {!! Form::close() !!}

        </div>
    </div>

</div>