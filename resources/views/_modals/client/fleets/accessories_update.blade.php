<div id="modal_updateAccessory" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'client.fleet.update.accessory', 'id' => 'post-form-fleet-accessory-update']) !!}

            <input type="hidden" name="fleet_id" value="{{ $fleet->id }}">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Update Accessories</h4>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('Accessories:') !!}
                    {!! Form::textarea('accessories', strip_tags($fleet->accessories), ['class' => 'form-control', 'placeholder' => 'Accessories', 'required']) !!}
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
            </div>

            {!! Form::close() !!}

        </div>
    </div>

</div>