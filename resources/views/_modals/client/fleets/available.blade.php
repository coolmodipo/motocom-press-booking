<div id="modal_available" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">

        {!! Form::open(['method' => 'POST', 'route' => 'client.fleet.available', 'id' => 'post-form-fleet-available']) !!}

        <div class="fetched-data"></div>

        {!! Form::close() !!}

    </div>
</div>

</div>
