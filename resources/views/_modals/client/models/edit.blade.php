<div id="modal_edit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'client.update.model', 'files' => true,'id' => 'post-form-model-edit']) !!}

            <div class="fetched-data"></div>

            {!! Form::close() !!}

        </div>
    </div>

</div>
