<div id="modal_add" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            {!! Form::open(['method' => 'POST', 'route' => 'client.model.create', 'files' => true, 'id' => 'post-form-fleet-book']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Create New Model</h4>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    {!! Form::label('image') !!}
                    {!! Form::file('image', ['class' => 'field']) !!}
                    <small>350px X 350px</small>
                </div>

                <div class="form-group">
                    {!! Form::label('Name') !!}
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Vehicle Name', 'required']) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('Category') !!}
                    {!! Form::select('category_id', App\Category::pluck('name', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Select Vehicle Category', 'required']) !!}
                </div>

                <input type="hidden" name="company_id" value="{{ $company->id }}">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Create Model</button>
            </div>

            {!! Form::close() !!}

        </div>
    </div>

</div>