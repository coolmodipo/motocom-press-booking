<div id="modal_add_company" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Add New Company</h4> </div>
            {!! Form::open(['method' => 'POST', 'route' => 'client.add.company', 'id' => 'post-form-company']) !!}
            <div class="modal-body">
                <input type="hidden" name="user_id" value="{{ $user->id }}">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Company Name:</label>
                    <input type="text" class="form-control" name="company_name" required>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Address 1:</label>
                    <input type="text" class="form-control" name="address_1" required>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Address2:</label>
                    <input type="text" class="form-control" name="address_2">
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">City:</label>
                    <input type="text" class="form-control" name="city" required>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">County:</label>
                    <input type="text" class="form-control" name="county" required>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Postcode:</label>
                    <input type="text" class="form-control" name="postcode" required>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary waves-effect waves-light">Add Company</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

</div>
