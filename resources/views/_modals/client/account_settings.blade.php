
<div id="modal_account_settings" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Account Settings</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="cid" id="cid" value="{{ $user->client->id }}">
                <div class="switchery-demo m-b-30 row">
                    <div class="col-md-3">Email Alerts:</div>
                    <div class="col-md-6">
                      <input type="checkbox" class="js-switch" data-color="#13dafe" id="emailAlert"{{ $user->client->notification->email == 1 ? 'checked' : '' }}/>
                    </div>

                </div>
                <div class="switchery-demo m-b-30 row">
                    <div class="col-md-3">Site Alerts: </div>
                    <div class="col-md-6">
                      <input type="checkbox" class="js-switch" data-color="#13dafe" id="siteAlert"{{ $user->client->notification->site == 1 ? 'checked' : '' }} class="js-switch"/>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>

</div>
