<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionic/1.3.2/css/ionic.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Theme style -->
    <link href="{{ asset('css/admin/data-table.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/custom.css') }}" rel="stylesheet">

{{--    <link href="{{ asset('css/ionicons1.min.css') }}" rel="stylesheet">--}}

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style type="text/css">

    </style>
    @yield('additional-css')
</head>

<body class="hold-transition skin-blue sidebar-mini">

    <div class="wrapper">

        @include('_partials.admin.header')

        @include('_partials.admin.sidebar-left')

        <div class="content-wrapper">
           @include('_includes.alerts')
           @yield('content')
        </div>

        @include('_partials.admin.footer')

        @include('_partials.admin.sidebar-right')

        <div class="control-sidebar-bg"></div>

    </div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/admin.js') }}"></script>
<script src="{{ asset('js/dashboard-chart.js') }}"></script>
<script src="{{ asset('js/vectormap.js') }}"></script>
<script src="{{ asset('js/adminlte-vectormap.js') }}"></script>
<script src="{{ asset('js/sparkline.js') }}"></script>
<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>
@yield('additional-js')
</body>
</html>
