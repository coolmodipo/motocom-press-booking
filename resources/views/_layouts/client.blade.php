<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="@yield('description')">
    <meta name="author" content="@yield('author')">
    <link rel="icon" type="image/png" sizes="16x16" href="">
    <title>@yield('title')</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('ampleadmin/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="{{ asset('ampleadmin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
    <link href="{{ asset('ampleadmin/css/animate.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('ampleadmin/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('ampleadmin/css/custom.css') }}" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{ asset('ampleadmin/css/colors/megna-dark.css') }}" id="theme" rel="stylesheet">
    <link href="{{ asset('ampleadmin/plugins/bower_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <!-- Switchery -->
    <link href="{{ asset('ampleadmin/plugins/bower_components/switchery/dist/switchery.min.css') }}" rel="stylesheet" />

    @yield('additional-css')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="fix-header">

<div id="wrapper">

    @include('_partials.client.header')

    @include('_partials.client.side-bar')

    <div id="page-wrapper">

        @include('_includes.clients.alerts')
        @include('_modals.client.account_settings')

        @yield('content')

    </div>

    @include('_partials.client.footer')

</div>

<script src="{{ asset('ampleadmin/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('ampleadmin/plugins/bower_components/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('ampleadmin/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('ampleadmin/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('ampleadmin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>
<script src="{{ asset('ampleadmin/plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>
<!-- Menu Plugin JavaScript -->
<script src="{{ asset('ampleadmin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
<!--slimscroll JavaScript -->
<script src="{{ asset('ampleadmin/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('ampleadmin/js/custom.js') }}"></script>
<script type="text/javascript">

    $(document).ready(function () {

        var cid = $('#cid').val();

        $('#emailAlert').change(function () {
            //alert('This has been clicked.')
            $.ajax({
                type : 'post',
                url: "{{ url('/client/notification/update') }}/"+ cid + "/email",
                dataType: "html",
                success : function(data){
                    swal({
                        title:  "Success!",
                        text:   "That has been updated for you.",
                        type:   "success",
                        timer:  3000,
                        showCancelButton: false,
                        showConfirmButton: false
                    });

                }
            });

        });

        $('#siteAlert').change(function () {
            //alert('This has been clicked.')
            $.ajax({
                type : 'post',
                url: "{{ url('/client/notification/update') }}/"+ cid + "/site",
                dataType: "html",
                success : function(data){
                    swal({
                        title:  "Success!",
                        text:   "That has been updated for you.",
                        type:   "success",
                        timer:  3000,
                        showCancelButton: false,
                        showConfirmButton: false
                    });

                }
            });

        });

    });

</script>

<script>
    jQuery(document).ready(function() {
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
    });
</script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

@yield('additional-js')

</body>
</html>
