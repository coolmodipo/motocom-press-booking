@extends('_layouts.auth')

@section('title', 'Account Login')

@section('content')

    <div class="login-box">

        @include('_includes.auth.header')

        <div class="login-box-body">

            <p class="login-box-msg">Sign in to start your session</p>

            {!! Form::open(['route' => 'login']) !!}

            <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::email('email', NULL, array('class' => 'form-control', 'placeholder' => 'Email')) !!}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                {!! Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) !!}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{!! $errors->first('password') !!}</strong>
                    </span>
                @endif
            </div>

            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            {!! Form::checkbox('remember') !!} Remember Me
                        </label>
                    </div>
                </div>

                <div class="col-xs-4">
                    {!! Form::submit('Sign In', array('class' => 'btn btn-primary btn-block btn-flat')) !!}
                </div>

            </div>

            {!! Form::close() !!}

            <a href="{{ route('password.request') }}">Forgotten Password</a>

        </div>

    </div>

@endsection
