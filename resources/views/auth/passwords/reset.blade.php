@extends('_layouts.auth')

@section('title', 'Account Password Reset')

@section('content')


    <div class="login-box">

        @include('_includes.auth.header')

        <div class="login-box-body">

            <p class="login-box-msg">Reset Password</p>

            {!! Form::open(['route' => 'password.request']) !!}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::email('email', old('email'), array('class' => 'form-control', 'placeholder' => 'Email')) !!}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                {{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Confirm Password')) }}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>

            <div class="row">

                <div class="col-md-12">
                    {!! Form::submit('Reset Password', array('class' => 'btn btn-primary btn-block btn-flat')) !!}
                </div>

            </div>

            {!! Form::close() !!}

        </div>

    </div>

@endsection
