@extends('_layouts.auth')

@section('title', 'Reset Password')

@section('content')

    <div class="login-box">

        <div class="login-logo">
            <a href="../../index2.html"><b>Admin</b>LTE</a>
        </div>

        <div class="login-box-body">

            <p class="login-box-msg">Reset Password</p>

            @if (session('status'))
            <div class="alert alert-success">
            {{ session('status') }}
            </div>
            @endif

            {!! Form::open(['route' => 'password.email']) !!}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::email('email', NULL, array('class' => 'form-control', 'placeholder' => 'Email')) !!}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="row">

                <div class="col-md-12">
                    {!! Form::submit('Send Password Reset Link', array('class' => 'btn btn-primary btn-block btn-flat')) !!}
                </div>

            </div>

            {!! Form::close() !!}

        </div>

    </div>

@endsection
