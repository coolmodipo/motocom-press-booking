@extends('_layouts.error')

@section('title', '503 - Site temp out of order')

@section('content')

<section class="content">

    <div class="error-page">

        <h2 class="headline text-yellow"> 503</h2>

        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> Oops! Permission Error.</h3>

            <p>
                You do not have the correct permission for that page.
                Please speak with the system administrator.
            </p>

        </div>

    </div>

</section>

@endsection