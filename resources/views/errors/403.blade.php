@extends('_layouts.error')

@section('title', '403 - Permission Denied.')

@section('content')

<section class="content">

    <div class="error-page">

        <h2 class="headline text-yellow"> 403</h2>

        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i>@t('Permission Denied', 'en').</h3>

            <p>
                @t('You do not have the right permissions to view this page', 'en').
                @t('Meanwhile, you may ', 'en')<a href="{{ url('/') }}">@t('return to dashboard', 'en')</a>@t(' or try using the search form', 'en').
            </p>

        </div>
        <!-- /.error-content -->
    </div>
    <!-- /.error-page -->
</section>

@endsection
