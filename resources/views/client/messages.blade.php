@extends('_layouts.client')

@section('title', 'User Messages')

@section('additional-css')

@endsection

@section('content')

    <div class="container-fluid">

        @include('_modals.client.message.create')

        @include('_templates.client.message-header')

        <div class="row">

            @include('_includes.clients.message-type')

            <div class="col-md-8 col-sm-8">

                <div class="white-box">

                    <div class="panel panel-default">

                        <div class="panel-wrapper collapse in">

                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>Sender</th>
                                    <th>Subject</th>
                                    <th>Date</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($messages->count())
                                @foreach($messages as $key => $message)
                                    <tr class="pointer">
                                      <td class="clickable-row" data-href='{{ route('client.show.message', $message->slug) }}' align="center">{{ $key+1 }}</td>
                                      <td class="clickable-row" data-href='{{ route('client.show.message', $message->slug) }}' >{{ $message->sender->user->name }}</td>
                                      <td class="clickable-row" data-href='{{ route('client.show.message', $message->slug) }}' >{{ $message->title }}</td>
                                      <td class="clickable-row" data-href='{{ route('client.show.message', $message->slug) }}' >{{ $message->created_at->format('d/m/Y') }}</td>
                                      <td><a class="text-inverse pointer" {!! $message->read == 0 ? 'onclick="readMessage('.$message->id.')"><i class="fa fa-eye" title="Mark as Read"></i>' : 'onclick="unreadMessage('.$message->id.')"><i class="fa fa-eye-slash" title="Mark as Unread"></i>' !!}</a></td>
                                      <td><a class="text-inverse pointer" title="Delete Message" data-toggle="tooltip" @if($message->type_id == 3) onclick="trashMessage({{ $message->id }})" @else onclick="deleteMessage({{ $message->id }})" @endif><i class="ti-trash"></i></a></td>
                                    </tr>
                                @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">You do not have any messages.</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>

                        </div>

                    </div>


                </div>

            </div>


        </div>

    </div>

@endsection

@section('additional-js')

    <script type="text/javascript">

        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });

        function readMessage(id)
        {
          $.ajax({
              type : 'post',
              url: "{{ url('/client/message/read') }}/"+ id ,
              dataType: "html",
              success : function(data){
                  swal({
                      title:  "Read!",
                      text:   "Message marked as Read.",
                      type:   "success",
                      timer:  3500,
                      showCancelButton: false,
                      showConfirmButton: false
                  });
                  setTimeout(function(){
                   location.reload();
                }, 3000);
              }
          });
        }

        function unreadMessage(id)
        {
          $.ajax({
              type : 'post',
              url: "{{ url('/client/message/unread') }}/"+ id ,
              dataType: "html",
              success : function(data){
                  swal({
                      title:  "Unread!",
                      text:   "Message marked as unread.",
                      type:   "success",
                      timer:  3500,
                      showCancelButton: false,
                      showConfirmButton: false
                  });
                  setTimeout(function(){
                   location.reload();
                }, 3000);
              }
          });
        }

        function deleteMessage(id)
        {
          swal({
              title: "Are you sure?",
              text: "You are about to delete this email.",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Delete",
              closeOnConfirm: false
          }, function(){

              $.ajax({
                  type : 'post',
                  url: "{{ url('/client/message/delete') }}/"+ id ,
                  dataType: "html",
                  success : function(data){
                      swal({
                          title:  "Deleted!",
                          text:   "That email has been deleted.",
                          type:   "success",
                          timer:  3500,
                          showCancelButton: false,
                          showConfirmButton: false
                      });
                      setTimeout(function(){
                       location.reload();
                    }, 3000);
                  }
              });

          });
        }

        function trashMessage(id)
        {
          swal({
              title: "Are you sure?",
              text: "You are about to permantly delete this email.",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Delete",
              closeOnConfirm: false
          }, function(){

              $.ajax({
                  type : 'post',
                  url: "{{ url('/client/message/trash') }}/"+ id ,
                  dataType: "html",
                  success : function(data){
                      swal({
                          title:  "Deleted!",
                          text:   "That email has been deleted.",
                          type:   "success",
                          timer:  3500,
                          showCancelButton: false,
                          showConfirmButton: false
                      });
                      setTimeout(function(){
                       location.reload();
                    }, 3000);
                  }
              });

          });
        }
    </script>

@endsection
