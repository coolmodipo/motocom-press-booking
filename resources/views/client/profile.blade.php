@extends('_layouts.client')

@section('title', 'User Profile Page : '. $user->name)

@section('additional-css')

    <style type="text/css">
        .profile-bg{margin:-25px;height:auto;overflow:hidden;position:relative}
        .user-profile{padding: 15px;}
        .default{margin-top: -15px;margin-left: 100px;}
    </style>

@endsection

@section('content')

    @include('_modals.client.add_company')
    @include('_modals.client.add_user')

            <div class="container-fluid">

                @include('_templates.client.profile-header')

                <div class="row">

                    <div class="col-md-4 col-xs-12">

                        <div class="white-box">
                            <div class="user-bg">
                                @if($user->client && $user->client->company)
                                <img width="100%" alt="user" src="{{ $user->client->company->image_url }}">
                                @elseif($user->client->userCompany->first())
                                <img width="100%" alt="user" src="{{ $user->client->userCompany->first()->image_url }}">
                                @endif
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <img src="{{ $user->client->image_url }}" class="thumb-lg img-circle" alt="img">

                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="white-box">

                            <div class="profile-bg">

                                    <div class="row container">
                                        <br>
                                        <div class="col-md-12 col-xs-6 b-r"> <strong>Full Name</strong>
                                            <br>
                                            <p class="text-muted">{{ $user->name }}</p>
                                        </div>
                                        @if($user->client->phone)
                                            <div class="col-md-12 col-xs-12 b-r"> <strong>Phone</strong>
                                                <br>
                                                <p class="text-muted">{{ $user->client->phone }}</p>
                                            </div>
                                        @endif
                                        @if($user->client->mobile)
                                        <div class="col-md-12 col-xs-12 b-r"> <strong>Mobile</strong>
                                            <br>
                                            <p class="text-muted">{{ $user->client->mobile }}</p>
                                        </div>
                                        @endif
                                        <div class="col-md-12 col-xs-12 b-r"> <strong>Email</strong>
                                            <br>
                                            <p class="text-muted">{{ $user->email }}</p>
                                        </div>
                                        @if($user->client->company)
                                        <div class="col-md-12 col-xs-12 b-r"> <strong>Company</strong>
                                            <br>
                                            <p class="text-muted">{{ $user->client->company->company_name }}</p>
                                        </div>
                                        @elseif($user->client->userCompany->first())
                                        <div class="col-md-12 col-xs-12 b-r"> <strong>Company</strong>
                                            <br>
                                            <p class="text-muted">{{ $user->client->userCompany->first()->company_name }}</p>
                                        </div>
                                        @endif

                                    </div>
                               @if($user->client->profile)
                                    <hr>
                                <div class="user-profile">
                                    <h4>Profile</h4>
                                    <p class="m-t-30">{{ $user->client->profile }}</p>
                                </div>
                               @endif

                            </div>

                         </div>
                     </div>

                    <div class="col-md-8 col-xs-12">

                        <div class="white-box">

                            <ul class="nav nav-tabs tabs customtab">

                                <li class="tab active">
                                    <a href="#edit-profile" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Edit Profile</span> </a>
                                </li>

                                @if($user->client->company)
                                    <li class="dropdown" role="presentation"> <a aria-controls="myTabDrop1-contents" data-toggle="dropdown" class="dropdown-toggle" id="myTabDrop1" href="#" aria-expanded="false">Company <span class="caret"></span></a>
                                        <ul id="myTabDrop1-contents" aria-labelledby="myTabDrop1" class="dropdown-menu">
                                @foreach($user->client->companies as $key => $company)
                                    <li class=""><a aria-controls="dropdown{{ $key }}" data-toggle="tab" id="company_{{ $key }}-tab" role="tab" href="#company_{{ $key }}" aria-expanded="true">@if( $company->default )<span class="label label-rouded label-inverse pull-right default">Default</span>@endif{{ $company->company_name }}</a></li>
                                @endforeach
                                        </ul>
                                    </li>
                                @endif

                            </ul>

                            {!! Form::open(['route' => 'client.update.profile', 'class' => 'form-horizontal form-material', 'files' => 'true']) !!}

                            <div class="tab-content">

                            <div class="tab-pane active" id="edit-profile">

                                        <input type="hidden" name="user_id" value="{{ $user->id }}"/>

                                        <div class="form-group">
                                            {!! Form::label('Image', NULL, ['class' => 'col-md-12']) !!}
                                            <div class="col-sm-12">
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                <div class="form-control" data-trigger="fileinput">
                                                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                    <span class="fileinput-filename"></span>
                                                </div>
                                                <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="hidden"><input name="image" type="file">
                                                </span>
                                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('firstname') ? 'has-error' : '' }}">
                                            {!! Form::label('firstname', NULL, ['class' => 'col-md-12']) !!}
                                            <div class="col-md-12">
                                            {!! Form::text('firstname', $user->client->firstname, ['class' => 'form-control form-control-line', 'placeholder' => 'Firstname']) !!}
                                            @if ($errors->has('firstname'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('firstname') }}</strong>
                                            </span>
                                            @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('lastname') ? 'has-error' : '' }}">
                                            {!! Form::label('lastname', NULL, ['class' => 'col-md-12']) !!}
                                            <div class="col-md-12">
                                            {!! Form::text('lastname', $user->client->lastname, ['class' => 'form-control form-control-line', 'placeholder' => 'Lastname']) !!}
                                            @if ($errors->has('lastname'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('lastname') }}</strong>
                                            </span>
                                            @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                            {!! Form::label('email', NULL, ['class' => 'col-md-12']) !!}
                                            <div class="col-md-12">
                                            {!! Form::email('email', $user->email, ['class' => 'form-control form-control-line', 'placeholder' => 'Email Address']) !!}
                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                                            {!! Form::label('phone', NULL, ['class' => 'col-md-12']) !!}
                                            <div class="col-md-12">
                                            {!! Form::text('phone', $user->client->phone, ['class' => 'form-control form-control-line', 'placeholder' => 'Contact Phone Number']) !!}
                                            @if ($errors->has('phone'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
                                            {!! Form::label('mobile', NULL, ['class' => 'col-md-12']) !!}
                                            <div class="col-md-12">
                                            {!! Form::text('mobile', $user->client->mobile, ['class' => 'form-control form-control-line', 'placeholder' => 'Contact Mobile Number']) !!}
                                            @if ($errors->has('mobile'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('mobile') }}</strong>
                                            </span>
                                            @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label('profile', NULL, ['class' => 'col-md-12']) !!}
                                            <div class="col-md-12">
                                            {!! Form::textarea('profile', $user->client->profile, ['class' => 'form-control form-control-line', 'placeholder' => 'Profile', 'row' => '5']) !!}
                                            @if ($errors->has('profile'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('profile') }}</strong>
                                            </span>
                                            @endif
                                            </div>
                                        </div>


                                </div>

                            @if($user->client->companies)

                                @foreach($user->client->companies as $key => $company)

                                    <div class="tab-pane" id="company_{{ $key }}">

                                        <div class="form-group">
                                            {!! Form::label('Company Logo', NULL, ['class' => 'col-md-12']) !!}
                                            <div class="col-sm-12">
                                                <img src="{{ $company->image_url }}" alt="{{ $company->company_name }} Logo" width="120" height="120">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label('Upload Logo', NULL, ['class' => 'col-md-12']) !!}
                                            <div class="col-sm-12">
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <span class="input-group-addon btn btn-default btn-file">
                                                    <span class="fileinput-new">Select file</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="hidden"><input name="logo_{{ $company->id }}" type="file">
                                                </span>
                                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('company_name') ? 'has-error' : '' }}">
                                            {!! Form::label('Company Name', NULL, ['class' => 'col-md-12']) !!}
                                            <div class="col-md-12">
                                                {!! Form::text('company_name_'.$company->id, $company->company_name, ['class' => 'form-control form-control-line', 'placeholder' => 'Company Name']) !!}
                                                @if ($errors->has('company_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('company_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('address_1') ? 'has-error' : '' }}">
                                            {!! Form::label('Address 1', NULL, ['class' => 'col-md-12']) !!}
                                            <div class="col-md-12">
                                                {!! Form::text('address_1_'.$company->id, $company->address_1, ['class' => 'form-control form-control-line', 'placeholder' => 'Company Address 1']) !!}
                                                @if ($errors->has('address_1'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('address_1') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label('Address 2', NULL, ['class' => 'col-md-12']) !!}
                                            <div class="col-md-12">
                                                {!! Form::text('address_2_'.$company->id, $company->address_2, ['class' => 'form-control form-control-line', 'placeholder' => 'Company Address 2']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
                                            {!! Form::label('City', NULL, ['class' => 'col-md-12']) !!}
                                            <div class="col-md-12">
                                                {!! Form::text('city_'.$company->id, $company->city, ['class' => 'form-control form-control-line', 'placeholder' => 'Company City']) !!}
                                                @if ($errors->has('city'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('city') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('county') ? 'has-error' : '' }}">
                                            {!! Form::label('County', NULL, ['class' => 'col-md-12']) !!}
                                            <div class="col-md-12">
                                                {!! Form::text('county_'.$company->id, $company->county, ['class' => 'form-control form-control-line', 'placeholder' => 'Company County']) !!}
                                                @if ($errors->has('county'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('county') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('postcode') ? 'has-error' : '' }}">
                                            {!! Form::label('Postcode', NULL, ['class' => 'col-md-12']) !!}
                                            <div class="col-md-12">
                                                {!! Form::text('postcode_'.$company->id, $company->postcode, ['class' => 'form-control form-control-line', 'placeholder' => 'Company Postcode']) !!}
                                                @if ($errors->has('postcode'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('postcode') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <button type="button" data-toggle="modal" data-target="#modal_add_user" class="btn btn-block btn-outline btn-info" data-cid="{{ $company->id }}">Add User</button>
                                        @if(!$company->default)
                                            <button type="button" onclick="makeDefault({{ $user->id }},{{ $company->id }})" class="btn btn-block btn-outline btn-primary">Make Default Company</button>
                                            <button type="button" class="btn btn-block btn-outline btn-danger" onclick="deleteCompany({{ $company->id }})">Delete Company</button>
                                        @endif


                                    </div>

                                @endforeach

                            @endif

                            </div>

                        </div>

                        <div class="col-sm-3">
                            <button class="btn btn-success">Update Profile</button>
                        </div>

                        {!! Form::close() !!}

                        @if(Session::has('adminUser'))
                        <div class="col-sm-3">
                            <button data-toggle="modal" data-target="#modal_add_company" class="btn btn-primary model_img img-responsive">Add Company</button>
                        </div>
                        @endif

                    </div>
                </div>


            </div>


@endsection

@section('additional-js')

    <script src="{{ asset('ampleadmin/js/waves.js') }}"></script>
    <script src="{{ asset('ampleadmin/js/jasny-bootstrap.js') }}"></script>

    <script type="text/javascript">

        $(document).on('show.bs.modal','#modal_add_user', function (e) {

            var cid = $(e.relatedTarget).data('cid');

            $.ajax({
                type : 'post',
                url: "{{ url('/client/company/add-user-form') }}/"+ cid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        function makeDefault(uid, id) {

            $.ajax({
                type : 'post',
                url: "{{ url('/client/company/make-default') }}/" + uid + "/"+ id ,
                dataType: "html",
                success : function(data){
                    //$('.fetched-data').html(data);
                    swal({
                        title:  "Success!",
                        text:   "That has been updated for you.",
                        type:   "success",
                        timer:  3000,
                        showCancelButton: false,
                        showConfirmButton: false
                    });
                    location.reload();
                }
            });
        }

        function deleteCompany(id) {

            swal({
                title: "Are you sure?",
                text: "You are about to delete this company.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Delete",
                closeOnConfirm: false
            }, function(){

                $.ajax({
                    type : 'post',
                    url: "{{ url('/client/company/delete') }}/"+ id ,
                    dataType: "html",
                    success : function(data){
                        //$('.fetched-data').html(data);
                        swal({
                            title:  "Deleted!",
                            text:   "That company has been deleted.",
                            type:   "success",
                            timer:  3000,
                            showCancelButton: false,
                            showConfirmButton: false
                        });
                        location.reload();
                    }
                });

            });

        }

    </script>

@endsection