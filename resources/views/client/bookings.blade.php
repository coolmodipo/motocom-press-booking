@extends('_layouts.client')

@section('title', 'Fleet Bookings')

@section('additional-css')
    <link href="{{ asset('ampleadmin/plugins/bower_components/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <div class="container-fluid">

        @include('_templates.client.bookings')

        <div class="row">

            <div class="col-sm-12">

                <div class="white-box">

                    <h3 class="box-title m-b-0">Booking Lists</h3>
                    <br/>
                    <div class="table-responsive">
                        <table id="fleetBooking" class="display nowrap" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Company</th>
                                <th>Model</th>
                                <th>Start date</th>
                                <th>Return date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Company</th>
                                <th>Model</th>
                                <th>Start date</th>
                                <th>Return date</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                              @if ($bookings->count())
                              @foreach ($bookings as $booking)
                                <tr>
                                    <td>{{ $booking->borrower->name }}</td>
                                    <td>{{ $booking->borrower->organisation_name }}</td>
                                    <td>{{ $booking->fleet->vehicle->name }} - {{ $booking->fleet->reg_number }}</td>
                                    <td>{{ $booking->start_date->format('d/m/Y') }}</td>
                                    <td>{{ $booking->end_date->format('d/m/Y') }}</td>
                                    <td>
                                    @if($booking->status_id == 6)
                                        <button class="btn btn-block btn-outline btn-rounded btn-success">Completed</button>
                                    @endif
                                    </td>
                                </tr>
                              @endforeach
                              @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


@endsection

@section('additional-js')

<script src="{{ asset('ampleadmin/plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

<script>

    $('#fleetBooking').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
</script>

@endsection
