<?php

//dd($fleet->booking);
//exit;
?>

@extends('_layouts.client')

@section('title', 'Fleet: '.$fleet->reg_number)

@section('additional-css')
    <link href="{{ asset('ampleadmin/plugins/bower_components/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

    <div class="container-fluid">

        @include('_templates.client.fleet-header')
        @include('_modals.client.fleets.accessories_add')
        @include('_modals.client.fleets.notes_add')
        @include('_modals.client.fleets.accessories_update')
        @include('_modals.client.fleets.notes_update')

        <div class="row">

            <div class="col-md-4 col-xs-12">

                <div class="white-box">

                    <div class="fleet-bg">
                      <img alt="user" src="{{ $fleet->vehicle->image_url }}" width="100%">
                    </div>

                    <div class="user-btm-box">
                        <div class="col-md-4 col-sm-3 text-center">
                            <p class="text-purple">
                              <i class="fa fa-book"></i> Bookings
                            </p>
                            <h1>{{ $fleet->booking->count() }}</h1>
                        </div>
                        <div class="col-md-4 col-sm-3 text-center">
                            <p class="text-danger">
                              <i class="fa fa-dashboard"></i> Total Mileage
                            </p>
                            <h1>{{ $fleet->mileage ?  $fleet->mileage : '0' }}</h1>
                        </div>
                    </div>

                </div>

            </div>

            <div class="col-md-8 col-xs-12">

                <div class="white-box">

                    <ul class="nav nav-tabs tabs customtab">
                        <li class="tab active">
                            <a href="#specifications" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Specifications</span> </a>
                        </li>
                        <li class="tab">
                            <a href="#history" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Booking History</span> </a>
                        </li>

                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane active" id="specifications">
                            <div class="row">
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Model</strong>
                                    <br>
                                    <p class="text-muted">{{ $fleet->vehicle->name }}</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Frame Number</strong>
                                    <br>
                                    <p class="text-muted">{{ $fleet->frame_number }}</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Engine</strong>
                                    <br>
                                    <p class="text-muted">{{ $fleet->engine }}</p>
                                </div>
                                <div class="col-md-3 col-xs-6"> <strong>Colour</strong>
                                    <br>
                                    <p class="text-muted">{{ $fleet->colour->name }}</p>
                                </div>
                            </div>
                            <h4 class="font-bold m-t-30">Accessories</h4>
                            <hr>
                            <p class="m-t-30" style="overflow-y: scroll; height:150px;">
                                {!! $fleet->accessories !!}
                            </p>
                            <div class="clear-fix pull-right">
                            @if($fleet->accessories)
                            <button class="btn btn-warning" data-toggle="modal" data-target="#modal_updateAccessory" >Update Accessories</button>
                            @endif
                            <button class="btn btn-primary" data-toggle="modal" data-target="#modal_addAccessory" >Add Accessories</button>
                            </div>
                            <div class="clear-fix">&nbsp;</div>
                            <h4 class="font-bold m-t-30">Notes</h4>
                            <hr>
                            <p class="m-t-30" style="overflow-y: scroll; height:150px;">
                                {!! $fleet->notes !!}
                            </p>
                            <div class="clear-fix pull-right">
                            @if($fleet->notes)
                            <button class="btn btn-warning" data-toggle="modal" data-target="#modal_updateNote" >Update Note</button>
                            @endif
                            <button class="btn btn-primary" data-toggle="modal" data-target="#modal_addNote" >Add Note</button>
                            </div>
                            <div class="clear-fix">&nbsp;</div>
                        </div>

                        <div class="tab-pane" id="history">

                            <div class="table-responsive">

                                <table id="myTable" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Company Name</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Miles</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($fleet->booking->count())
                                    @foreach($fleet->booking as $booking)
                                        <tr>
                                            <td>{{ $booking->borrower->name }}</td>
                                            <td>{{ $booking->borrower->organisation_name }}</td>
                                            <td>{{ $booking->start_date->format('d/m/Y') }}</td>
                                            <td>{{ $booking->end_date->format('d/m/Y') }}</td>
                                            <td>{{ $booking->mileage_used }} Miles</td>
                                        </tr>
                                    @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5">There are no current bookings.</td>
                                        </tr>
                                    @endif

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection

@section('additional-js')

    <script src="{{ asset('ampleadmin/plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            $('#myTable').DataTable();
        });

    </script>

@endsection
