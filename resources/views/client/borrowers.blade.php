@extends('_layouts.client')

@section('title', 'Borrowers')

@section('additional-css')

@endsection

@section('content')

    <div class="container-fluid">

        @include('_templates.client.borrowers')
        @include('_modals.client.borrowers.new')
        @include('_modals.client.borrowers.edit')

        <div class="row">

            <div class="col-sm-12">

                <div class="panel">
                    <div class="panel-heading">Manage Borrowers</div>
                    <div class="table-responsive">
                        <table class="table table-hover manage-u-table">
                            <thead>
                            <tr>
                                <th width="70" class="text-center">&nbsp;</th>
                                <th>Name</th>
                                <th>Organisation</th>
                                <th>Contact</th>
                                <th>Bookings</th>
                                <th width="300">MANAGE</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($borrowers->count())
                                @foreach($borrowers as $borrower)
                                <tr>
                                    <td class="text-center">&nbsp;</td>
                                    <td>{{ $borrower->name }}
                                    <td>{{ $borrower->organisation_name }}
                                        <br>
                                        <span class="text-muted">Address : {{ $borrower->address }}. {{ $borrower->postcode }}</span>
                                    </td>
                                    <td>Email: {{ $borrower->email }}
                                        <br>
                                        <span class="text-muted">Tel: {{ $borrower->telephone }}</span>
                                    </td>
                                    <td>Current Booking: 0
                                        <br>
                                        <span class="text-muted">No. Bookings: 0</span>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5" data-toggle="modal" data-target="#modal_edit" data-bid="{{ $borrower->id }}"><i class="ti-pencil-alt"></i></button>
                                        <button type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-trash"></i></button>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6">There are no borrowers yet.</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('additional-js')

    <script type="text/javascript">

        // fire below when modal opens
        $(document).on('shown.bs.modal','#modal_edit', function (e) {

            var bid = $(e.relatedTarget).data('bid');;

            $.ajax({
                type : 'post',
                url: "{{ url('/client/borrower-form') }}/"+ bid,
                dataType: "html",
                success : function(data){

                    // set our html
                    $('.fetched-data').html(data);

                }
            });

        });

    </script>

@endsection