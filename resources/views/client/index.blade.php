<?php

?>

@extends('_layouts.client')

@section('title', 'Dashboard')

@section('additional-css')

    <link href="{{ asset('ampleadmin/plugins/bower_components/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('ampleadmin/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') }}" rel="stylesheet">
    <link href="{{ asset('ampleadmin/plugins/bower_components/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet" />
    <!-- Calendar CSS -->
    <link href="{{ asset('ampleadmin/plugins/bower_components/calendar/dist/fullcalendar.css') }}" rel="stylesheet" />
    <!-- color CSS -->
    <link href="{{ asset('ampleadmin/css/colors/megna-dark.css') }}" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>-->

    <style type="text/css">

        #flot-tooltip {
            font-size: 12px;
            font-family: Verdana, Arial, sans-serif;
            position: absolute;
            display: none;
            border: 2px solid;
            padding: 2px;
            background-color: #000000 !important;
            opacity: 0.8;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -khtml-border-radius: 5px;
            border-radius: 5px;
        }

        .circleBase {
            border-radius: 50%;
        }
        .type  {
            width: 25px;
            height: 25px;
            margin-right: 10px;
        }


    </style>

@endsection

@section('content')

    <div class="container-fluid">

        @include('_templates.client.dashboard-header')

        <div class="row">
            <div class="col-lg-3 col-sm-12 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title">Borrowers</h3>
                    <ul class="list-inline two-part">
                        <li><i class="icon-people text-info"></i></li>
                        <li class="text-right"><span class="counter">{{ $borrowers->count() }}</span></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title">Models</h3>
                    <ul class="list-inline two-part">
                        <li><i class="icon-folder text-purple"></i></li>
                        <li class="text-right"><span class="counter">{{ $models->count() }}</span></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title">Fleets</h3>
                    <ul class="list-inline two-part">
                        <li><i class="icon-folder-alt text-danger"></i></li>
                        <li class="text-right"><span class="">{{ $fleets->count() }}</span></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title">Bookings</h3>
                    <ul class="list-inline two-part">
                        <li><i class="ti-wallet text-success"></i></li>
                        <li class="text-right"><span class="">{{ $bookingsInfo->count() }}</span></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title">Booking Information</h3>
                    <div class="flot-chart">
                        <div class="flot-chart-content" id="flot-bar-chart" style="width:100%;height:320px;"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title">Fleet Information</h3>
                    <div class="flot-chart">
                        <div class="sales-bars-chart" style="width:100%;height:320px;"></div>
                    </div>
                </div>
            </div>
            <!-- /.row -->

        </div>


        <div class="row">

            <div class="col-lg-2 col-md-2 col-sm-12">

                <div class="white-box">

                    <h3 class="box-title">Booking Type</h3>

                    @foreach($bookingTypes as $bookingtype)
                        <p><div class="circleBase type pull-left" style="background: {{ $bookingtype->hex_code }};"></div>  {{ $bookingtype->name }}</p>
                    @endforeach

                </div>

            </div>

            <div class="col-lg-10 col-md-10 col-sm-12">

                <div class="white-box">

                    <div id="calendar"></div>

                </div>

            </div>

        </div>

    </div>

@endsection

@section('additional-js')

    <script src="{{ asset('ampleadmin/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('ampleadmin/plugins/bower_components/calendar/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('ampleadmin/plugins/bower_components/moment/moment.js') }}"></script>
    <script src="{{ asset('ampleadmin/plugins/bower_components/calendar/dist/fullcalendar.min.js') }}"></script>
    <!-- Flot Charts JavaScript -->
    <script src="{{ asset('ampleadmin/plugins/bower_components/flot/excanvas.min.js') }}"></script>
    <script src="{{ asset('ampleadmin/plugins/bower_components/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('ampleadmin/plugins/bower_components/flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('ampleadmin/plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.js') }}"></script>
    <script src="{{ asset('ampleadmin/js/flot-data.js') }}"></script>

    <script type="text/javascript">

        $(document).ready(function() {

            $('#calendar').fullCalendar({

                events: [
                        @foreach($bookings as $booking)
                        @if($booking->borrower->company->id == $company->id)
                            {
                                title  : '{{ $booking->fleet->vehicle->name }} - {{ $booking->fleet->reg_number }}',
                                start: '{{ $booking->start_date }}',
                                end: '{{ $booking->end_date }}',
                                color  : '{{ $booking->bookingtype->hex_code }}'
                            },
                        @endif
                        @endforeach
                ],displayEventTime: false,
            });

            var sparklineLogin = function() {

                $("#sparkline8").sparkline([2,4,4,6,8,5,6,4,8,6,6,2 ], {
                    type: 'line',
                    width: '100%',
                    height: '50',
                    lineColor: '#ff7676',
                    fillColor: '#ff7676',
                    maxSpotColor: '#ff7676',
                    highlightLineColor: 'rgba(0, 0, 0, 0.2)',
                    highlightSpotColor: '#ff7676'
                });
                $("#sparkline9").sparkline([0,2,8,6,8,5,6,4,8,6,6,2 ], {
                    type: 'line',
                    width: '100%',
                    height: '50',
                    lineColor: '#2cabe3',
                    fillColor: '#2cabe3',
                    minSpotColor:'#2cabe3',
                    maxSpotColor: '#2cabe3',
                    highlightLineColor: 'rgba(0, 0, 0, 0.2)',
                    highlightSpotColor: '#2cabe3'
                });

            }
            var sparkResize;

            $(window).resize(function(e) {
                clearTimeout(sparkResize);
                sparkResize = setTimeout(sparklineLogin, 500);
            });
            sparklineLogin();

            //FLOT CHART - Booking Data
            $(function() {

                var barOptions = {
                    series: {
                        bars: {
                            show: true,
                            barWidth: 15*24*60*60*1000
                        }
                    },
                    xaxis: {
                        mode: "time",
                        timeformat: "%m/%y",
                        minTickSize: [1, "month"]
                    },
                    yaxis: {
                        tickDecimals: 0
                    },
                    grid: {
                        hoverable: true
                    },
                    legend: {
                        show: false
                    },
                    grid: {
                            color: "#AFAFAF",
                            hoverable: true,
                            borderWidth: 0,
                            backgroundColor: '#e6e6e6'
                        },
                    tooltip: true,
                    tooltipOpts: {
                        content: "Bookings: %y, Date: %x"
                    }
                };
                //Create a json of the Chart Booking array.
                var chartData = {!! json_encode($chartBookings) !!};
                //Empty object to push the Chart Booking data.
                var barChartData = []
                //Loop through the Chart Booking array to set the correct format for the flot chart.
                for (var prop in chartData) {
                  if (chartData.hasOwnProperty(prop)) {
                    //Push the data to the Bar Chart Data
                    barChartData.push([parseInt(prop), chartData[prop]])
                  }
                }

                //alert(JSON.stringify(barChartData));

                var barData = {
                    label: "bar",
                    color: "#fb9678",
                    data: barChartData
                };
                $.plot($("#flot-bar-chart"), [barData], barOptions);

            });

        });




    </script>

@endsection
