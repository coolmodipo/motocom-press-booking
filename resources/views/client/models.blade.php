@extends('_layouts.client')

@section('title', 'Models')

@section('additional-css')

    <!-- Calendar CSS -->
    <link href="{{ asset('ampleadmin/plugins/bower_components/calendar/dist/fullcalendar.css') }}" rel="stylesheet" />
    <!-- color CSS -->
    <link href="{{ asset('ampleadmin/css/colors/megna-dark.css') }}" id="theme" rel="stylesheet">

@endsection

@section('content')

    <div class="container-fluid">

        @include('_templates.client.model-header')
        @include('_modals.client.models.add')
        @include('_modals.client.models.edit')
        @include('_modals.client.models.availability')
        @include('_modals.client.models.fleet_add')

        <div class="row">
            @if($models->count())
                @foreach($models as $model)
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="white-box">
                            <div class="product-img">
                                <img src="{{ $model->image_url }}" />
                                <div class="pro-img-overlay">
                                    <a data-toggle="modal" data-target="#modal_edit" class="bg-primary" data-mid="{{ $model->id }}" >
                                        <i class="ti-pencil"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="product-text">
                                <h3 class="box-title m-b-0">{{ $model->name }}</h3>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6 col-xs-12" style="margin-top: 10px;">
                                        <button data-toggle="modal" data-target="#modal_fleet_add" class="btn btn-block btn-default btn-rounded" data-mid="{{ $model->id }}"><i class="mdi mdi-playlist-plus" title="Add Fleet"></i></button>
                                    </div>
                                    @if($model->fleets->count())
                                    <div class="col-lg-6 col-sm-6 col-xs-12" style="margin-top: 10px;">
                                        <a href="{{ route('client.model.availability', $model->slug)  }}" class="btn btn-block btn-primary btn-rounded"><i class="mdi mdi-calendar-check" title="Availability"></i></a>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif

        </div>


    </div>

@endsection

@section('additional-js')

    <script src="{{ asset('ampleadmin/plugins/bower_components/calendar/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('ampleadmin/plugins/bower_components/moment/moment.js') }}"></script>
    <script src="{{ asset('ampleadmin/plugins/bower_components/calendar/dist/fullcalendar.min.js') }}"></script>

    <script type="text/javascript">

        $(document).on('show.bs.modal','#modal_fleet_add', function (e) {

            var mid = $(e.relatedTarget).data('mid');

            $.ajax({
                type : 'post',
                url: "{{ url('/client/model-fleet-create-form') }}/"+ mid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_edit', function (e) {

            var mid = $(e.relatedTarget).data('mid');

            $.ajax({
                type : 'post',
                url: "{{ url('/client/model-edit-form') }}/"+ mid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_availability', function (e) {

            var mid = $(e.relatedTarget).data('mid');

            $.ajax({
                type : 'post',
                url: "{{ url('/client/model-availability-form') }}/"+ mid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $("#calendarAvailability").fullCalendar('refetchEvents');

    </script>

@endsection
