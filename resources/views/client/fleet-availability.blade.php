<?php
////$bookedFleet = $model->bookedFleets->pluck('id')->toArray();
//echo '<pre>';
//foreach ($model->faultyFleets as $booked){
// print_r($booked->reg_number);
//}
//exit;
?>

@extends('_layouts.client')

@section('title', 'Fleet Availability')

@section('additional-css')
    <!-- Calendar CSS -->
    <link href="{{ asset('ampleadmin/plugins/bower_components/calendar/dist/fullcalendar.css') }}" rel="stylesheet" /><!-- Date picker plugins css -->
    <link href="{{ asset('ampleadmin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="{{ asset('ampleadmin/plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('ampleadmin/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <!-- Page plugins css -->
    <link href="{{ asset('ampleadmin/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css') }}" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{ asset('ampleadmin/css/colors/megna-dark.css') }}" id="theme" rel="stylesheet">
    <style type="text/css">
        .popover{
            z-index: 1000000000 !important;
        }
    </style>

@endsection

@section('content')

    <div class="container-fluid">

    @include('_templates.client.model-header')
    @include('_modals.client.fleets.book')
    @include('_modals.client.fleets.new_book')
    @include('_modals.client.fleets.collected')
    @include('_modals.client.fleets.returned')
    @include('_modals.client.fleets.status')

        <div class="row">
            <div class="col-lg-3 col-sm-12 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title">Available</h3>
                    <ul class="list-inline two-part">
                        <li><i class="icon-check text-success"></i></li>
                        <li class="text-right"><span class="counter">{{ $model->fleets->count()-$model->unavailableFleets->count()-$model->faultyFleets->count() }}</span></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title">Bookings</h3>
                    <ul class="list-inline two-part">
                        <li><i class="icon-book-open text-primary"></i></li>
                        <li class="text-right"><span class="counter">{{ $model->bookingCount->count() }}</span></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title">Service</h3>
                    <ul class="list-inline two-part">
                        <li><i class="icon-wrench text-warning"></i></li>
                        <li class="text-right"><span class="">{{ $model->unavailableFleets->count() }}</span></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title">Faulty</h3>
                    <ul class="list-inline two-part">
                        <li><i class="icon-close  text-danger"></i></li>
                        <li class="text-right"><span class="">{{ $model->faultyFleets->count() }}</span></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="white-box">
                <input type="hidden" name="eventModal" id="eventModal">
                    <div id="calendar"></div>
                </div>
            </div>

        </div>

    </div>

@endsection

@section('additional-js')

    <script src="{{ asset('ampleadmin/plugins/bower_components/moment/moment.js') }}"></script>
    <!-- Full Calendar JS -->
    <script src="{{ asset('ampleadmin/plugins/bower_components/calendar/dist/fullcalendar.min.js') }}"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="{{ asset('ampleadmin/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js') }}"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="{{ asset('ampleadmin/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js') }}"></script>
    <script src="{{ asset('ampleadmin/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js') }}"></script>
    <script src="{{ asset('ampleadmin/plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js') }}"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="{{ asset('ampleadmin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="{{ asset('ampleadmin/plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('ampleadmin/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <script src="{{ asset('ampleadmin/js/waves.js') }}"></script>
    <script src="{{ asset('ampleadmin/js/jasny-bootstrap.js') }}"></script>

    <script type="text/javascript">

        $(document).on('shown.bs.modal','#modal_new_book', function () {
            // load our date pickers
            var $modal = $(this),
                start = $modal.data('start');

            $('INPUT[name="start_date"], INPUT[name="end_date"]', $modal).datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true,
                'default': start
            });

            // load our time pickers
            $('INPUT[name="collector_time"], INPUT[name="returnee_time"]', $modal).clockpicker({
                placement: 'bottom',
                align: 'left',
                autoclose: true,
                'default': 'now'
            });
        });


        // fire below when modal opens
        $(document).on('shown.bs.modal','#modal_book', function () {

            var $modal = $(this),
                ebid = $modal.data('ebid');

            $.ajax({
                type : 'post',
                url: "{{ url('/client/fleet-book-form') }}/"+ ebid,
                dataType: "html",
                success : function(data){

                    // set our html
                    $('.fetched-data').html(data);

                    // load our date pickers
                    $('INPUT[name="start_date"], INPUT[name="end_date"]', $modal).datepicker({
                        format: 'd/m/yyyy',
                        autoclose: true,
                        todayHighlight: true
                    });

                    // load our time pickers
                    $('INPUT[name="collector_time"], INPUT[name="returnee_time"]', $modal).clockpicker({
                        placement: 'bottom',
                        align: 'left',
                        autoclose: true,
                        'default': 'now'
                    });
                }
            });

        });


        // fire below when modal opens
        $(document).on('shown.bs.modal','#modal_collected', function () {

            var $modal = $(this);
            var fid = $modal.data('fid');
            var bid = $modal.data('bid');

            $.ajax({
                type : 'post',
                url: "{{ url('/client/fleet-collected-form') }}/"+ fid + "/" + bid,
                dataType: "html",
                success : function(data){

                    // set our html
                    $('.fetched-data').html(data);

                }
            });

        });

        // fire below when modal opens
        $(document).on('shown.bs.modal','#modal_returned', function () {

            var $modal = $(this);
            var fid = $modal.data('fid');
            var bid = $modal.data('bid');

            $.ajax({
                type : 'post',
                url: "{{ url('/client/fleet-returned-form') }}/"+ fid + "/" + bid,
                dataType: "html",
                success : function(data){

                    // set our html
                    $('.fetched-data').html(data);

                }
            });

        });

        // fire below when modal opens
        $(document).on('shown.bs.modal','#modal_status', function () {

            var $modal = $(this);
            var fid = $modal.data('fid');

            $.ajax({
                type : 'post',
                url: "{{ url('/client/fleet-status-form') }}/"+ fid,
                dataType: "html",
                success : function(data){

                    // set our html
                    $('.fetched-data').html(data);

                }
            });

        });


        $('#calendar').fullCalendar({
            header:{
                left:   'month, agendaWeek, agendaDay',
                center: 'title',
                right:  'today prev,next'
            },
            selectable: true,
            selectHelper: true,
            editable: true,
            select: function(start, end){

                var today = new Date();
                //alert(today);
                if(start > today)
                {
                    $( "#datepicker-autoclose-start-date" ).val(start.format('DD/MM/YYYY'));
                    $( "#datepicker-autoclose-end-date" ).val(end.format('DD/MM/YYYY'));
                    $("#modal_new_book").modal('toggle');
                }






            },
            events: [
                    @foreach($bookings as $booked)
                    @if(in_array($booked->fleet->reg_number, $fleets))
                    {
                        title: '{{ $booked->fleet->reg_number }}',
                        start: '{{ $booked->start_date }}',
                        end: '{{ $booked->end_date }}',
                        @if($booked->status_id == 3)
                        color: '#707cd2',
                        @elseif($booked->status_id == 5)
                        color: '#af7ac5',
                        @endif
                        fid: '{{  $booked->fleet_id }}',
                        bid: '{{  $booked->id }}',
                        type: '{{ $booked->status_id }}',
                        allDay: false
                    },
                    @endif
                    @endforeach
                    @foreach($model->unavailableFleets as $booked)
                    @if(in_array($booked->reg_number, $fleets))
                    {
                        title: '{{ $booked->reg_number }}',
                        start: '{{ date('Y-m-d') }}',
                        end: '{{ date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 365 day")) }}',
                        color: '#ffc36d',
                        id: '{{  $booked->id }}',
                        type: '{{ $booked->status_id }}',
                        allDay: false
                    },
                    @endif
                    @endforeach
                    @foreach($model->faultyFleets as $booked)
                    @if(in_array($booked->reg_number, $fleets))
                    {
                        title: '{{ $booked->reg_number }}',
                        start: '{{ date('Y-m-d') }}',
                        end: '{{ date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 365 day")) }}',
                        color: '#ff7676',
                        id: '{{  $booked->id }}',
                        type: '{{ $booked->status_id }}',
                        allDay: false
                    },
                    @endif
                    @endforeach
            ],
            displayEventTime: false,
            eventClick: function(event) {

                if(event.type == 3) {
                    $("#modal_collected").data("fid", event.fid).data("bid", event.bid).modal('toggle');
                }
                else if(event.type == 5){
                    $("#modal_returned").data("fid", event.fid).data("bid", event.bid).modal('toggle');
                }
                else{
                    $("#modal_status").data("fid", event.id).modal('toggle');
                }


            },

        });


    </script>

@endsection
