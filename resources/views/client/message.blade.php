@extends('_layouts.client')

@section('title', 'User Messages')

@section('additional-css')

@endsection

@section('content')

    @include('_modals.client.message.delete')
    @include('_modals.client.message.save')
    @include('_modals.client.message.reply')

    <div class="container-fluid">

        @include('_modals.client.message.create')

        @include('_templates.client.message-header')

        <div class="row">

            @include('_includes.clients.message-type')

            <div class="col-md-8 col-sm-8">

                <div class="white-box">

                    <h3 class="box-title m-b-0">{{ $message->title }}</h3>
                    <small class="pull-left">{{ $message->created_at->format('d/m/Y') }}</small>
                    <small class="pull-right"><strong>Sent By:</strong> {{ $message->sender->user->name }}</small>
                    <hr>

                    <div class="inline-editor note-air-editor note-editable panel-body" id="note-editor-1" contenteditable="true">

                        <p>{{ $message->message }}</p>

                    </div>

                </div>

                <div class="white-box">
                    @if($message->type_id == 3)
                    <a class="btn btn-warning btn-rounded" data-toggle="modal" onclick="restoreMessage({{ $message->id }})" type="button">Restore Inbox</a>
                    @else
                    <a class="btn btn-info btn-rounded" data-toggle="modal" data-target="#modal_reply" data-mid="{{ $message->id }}" type="button">Reply</a>
                    <a class="btn btn-danger btn-rounded" data-toggle="modal" onclick="deleteMessage({{ $message->id }})" type="button">Delete</a>
                    @endif
                    @if($message->read == 1)
                    <a class="btn btn-success btn-rounded" data-toggle="modal" onclick="unreadMessage({{ $message->id }})" type="button">Mark Unread</a>
                    @endif
                </div>

            </div>

        </div>

    </div>

@endsection

@section('additional-js')

    <script type="text/javascript">

        $(document).on('show.bs.modal','#modal_reply', function (e) {

            var mid = $(e.relatedTarget).data('mid');

            $.ajax({
                type : 'post',
                url: "{{ url('/client/message/reply-message-form') }}/"+ mid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        function deleteMessage(id)
        {
          swal({
              title: "Are you sure?",
              text: "You are about to delete this email.",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Delete",
              closeOnConfirm: false
          }, function(){

              $.ajax({
                  type : 'post',
                  url: "{{ url('/client/message/delete') }}/"+ id ,
                  dataType: "html",
                  success : function(data){
                      //$('.fetched-data').html(data);
                      swal({
                          title:  "Deleted!",
                          text:   "That email has been deleted.",
                          type:   "success",
                          timer:  3000,
                          showCancelButton: false,
                          showConfirmButton: false
                      });
                      location.reload();
                  }
              });

          });
        }

        function restoreMessage(id)
        {
          swal({
              title: "Are you sure?",
              text: "Would you like to restore this email to your inbox.",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#ffc36d",
              confirmButtonText: "Restore",
              closeOnConfirm: false
          }, function(){

              $.ajax({
                  type : 'post',
                  url: "{{ url('/client/message/restore') }}/"+ id ,
                  dataType: "html",
                  success : function(data){
                      //$('.fetched-data').html(data);
                      swal({
                          title:  "Restored!",
                          text:   "That email has been restored.",
                          type:   "success",
                          timer:  3000,
                          showCancelButton: false,
                          showConfirmButton: false
                      });
                      location.reload();
                  }
              });

          });
        }

        function unreadMessage(id)
        {
          swal({
              title: "Are you sure?",
              text: "Would you like to mark this email as unread.",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#53e69d",
              confirmButtonText: "Unread",
              closeOnConfirm: false
          }, function(){

              $.ajax({
                  type : 'post',
                  url: "{{ url('/client/message/unread') }}/"+ id ,
                  dataType: "html",
                  success : function(data){
                      //$('.fetched-data').html(data);
                      swal({
                          title:  "Restored!",
                          text:   "That email has been marked as unread.",
                          type:   "success",
                          timer:  3000,
                          showCancelButton: false,
                          showConfirmButton: false
                      });
                      window.location.href = '{{ route('client.messages', 'inbox') }}'
                  }
              });

          });
        }
    </script>

@endsection
