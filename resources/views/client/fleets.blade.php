@extends('_layouts.client')

@section('title', 'Fleets')

@section('additional-css')

    <!-- Date picker plugins css -->
    <link href="{{ asset('ampleadmin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="{{ asset('ampleadmin/plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('ampleadmin/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <!-- Page plugins css -->
    <link href="{{ asset('ampleadmin/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css') }}" rel="stylesheet">
    <!-- Color picker plugins css -->
    <link href="{{ asset('ampleadmin/plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css') }}" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="{{ asset('ampleadmin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="{{ asset('ampleadmin/plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('ampleadmin/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

    <style type="text/css">
        .popover{
            z-index: 1000000000 !important;
        }
    </style>

@endsection

@section('content')

    <div class="container-fluid">

        @include('_modals.client.fleets.add')
        @include('_modals.client.fleets.edit')
        @include('_modals.client.fleets.book')
        @include('_modals.client.fleets.return')
        @include('_modals.client.fleets.available')
        @include('_modals.client.fleets.information')
        @include('_templates.client.fleet-header')

        <div class="row">

            <div class="white-box">

                <div class="table-responsive">

                    <table class="table product-overview" id="fleetTable">
                        <thead>
                        <tr>
                            <th>Photo</th>
                            <th>Reg Number</th>
                            <th>Model</th>
                            <th>Frame Number</th>
                            <th>Engine</th>
                            <th>Colour</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($fleets->count())
                            @foreach($fleets as $fleet)
                                <tr>
                                    <td> <img src="{{ $fleet->vehicle->image_url }}" alt="iMac" width="80"> </td>
                                    <td>{{ $fleet->reg_number }}</td>
                                    <td>{{ $fleet->vehicle->name }}</td>
                                    <td>R{{ $fleet->frame_number }}</td>
                                    <td>{{ $fleet->engine }}</td>
                                    <td>{{ $fleet->colour->name }}</td>
                                    <td> <span class="label label-{{ $fleet->status->class }} font-weight-100">{{ $fleet->status->title }}</span> </td>
                                    <td>
                                        <a class="text-inverse pointer" data-toggle="modal" data-target="#modal_edit" data-fid="{{ $fleet->id }}" title="Edit Fleet Item"><i class="ti-marker-alt"></i></a>
                                        @if($fleet->status_id == 1)
                                            <a class="text-inverse pointer" data-toggle="modal" data-target="#modal_book" data-fid="{{ $fleet->id }}" title="Book Fleet Item"><i class="ti-book"></i></a>
                                        @endif
                                        @if($fleet->status_id == 2)
                                            <a class="text-inverse pointer" data-toggle="modal" data-target="#modal_available" data-fid="{{ $fleet->id }}" title="Make item available"><i class="ti-hummer"></i></a>
                                        @endif
                                        @if($fleet->status_id == 3)
                                            <a class="text-inverse pointer" data-toggle="modal" data-target="#modal_return" data-fid="{{ $fleet->id }}" title="Return Fleet Item"><i class="ti-check"></i></a>
                                        @endif
                                        @if($fleet->accessories || $fleet->notes)
                                            <a href="{{ route('client.fleet.show', $fleet->slug) }}" class="text-inverse pointer" title="Fleet Information"><i class="ti-info-alt"></i></a>
                                        @endif
                                        <a class="text-inverse pointer" title="Delete Fleet Item" data-toggle="tooltip" onclick="deleteFleet({{ $fleet->id }})"><i class="ti-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">There are no fleet items at present.</td>
                            </tr>
                        @endif
                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

@endsection

@section('additional-js')

    <script src="{{ asset('ampleadmin/plugins/bower_components/moment/moment.js') }}"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="{{ asset('ampleadmin/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js') }}"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="{{ asset('ampleadmin/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js') }}"></script>
    <script src="{{ asset('ampleadmin/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js') }}"></script>
    <script src="{{ asset('ampleadmin/plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js') }}"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="{{ asset('ampleadmin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="{{ asset('ampleadmin/plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('ampleadmin/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <script src="{{ asset('ampleadmin/js/waves.js') }}"></script>
    <script src="{{ asset('ampleadmin/js/jasny-bootstrap.js') }}"></script>

    <script>

        $(document).ready(function(){

            $(function() {

                $("body").delegate("#datepicker-autoclose-start-date", "focusin", function(){
                    $(this).datepicker({
                        format: 'd/m/yyyy',
                        autoclose: true,
                        todayHighlight: true
                    });
                });

                $("body").delegate("#datepicker-autoclose-end-date", "focusin", function(){
                    $(this).datepicker({
                        format: 'd/m/yyyy',
                        autoclose: true,
                        todayHighlight: true
                    });
                });

                $("body").delegate("#single-input-start", "focusin", function(){
                    $(this).clockpicker({
                        placement: 'bottom',
                        align: 'left',
                        autoclose: true,
                        'default': 'now'
                    });
                });

                $("body").delegate("#single-input-end", "focusin", function(){
                    $(this).clockpicker({
                        placement: 'bottom',
                        align: 'left',
                        autoclose: true,
                        'default': 'now'
                    });
                });

            });

        })

        $(document).on('show.bs.modal','#modal_edit', function (e) {

            var fid = $(e.relatedTarget).data('fid');

            $.ajax({
                type : 'post',
                url: "{{ url('/client/fleet-edit-form') }}/"+ fid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_book', function (e) {
            var $modal = $(this);
            var fid = $(e.relatedTarget).data('fid');

            $.ajax({
                type : 'post',
                url: "{{ url('/client/fleet-book-form') }}/"+ fid,
                dataType: "html",
                success : function(data){

                    // set our html
                    $('.fetched-data').html(data);

                    // load our date pickers
                    $('INPUT[name="start_date"], INPUT[name="end_date"]', $modal).datepicker({
                        format: 'd/m/yyyy',
                        autoclose: true,
                        todayHighlight: true
                    });

                    // load our time pickers
                    $('INPUT[name="collector_time"], INPUT[name="returnee_time"]', $modal).clockpicker({
                        placement: 'bottom',
                        align: 'left',
                        autoclose: true,
                        'default': 'now'
                    });
                }
            });
        });

        $(document).on('show.bs.modal','#modal_return', function (e) {

            var fid = $(e.relatedTarget).data('fid');

            $.ajax({
                type : 'post',
                url: "{{ url('/client/fleet-return-form') }}/"+ fid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_information', function (e) {

            var fid = $(e.relatedTarget).data('fid');

            $.ajax({
                type : 'post',
                url: "{{ url('/client/fleet-information-form') }}/"+ fid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        $(document).on('show.bs.modal','#modal_available', function (e) {

            var fid = $(e.relatedTarget).data('fid');

            $.ajax({
                type : 'post',
                url: "{{ url('/client/fleet-available-form') }}/"+ fid ,
                dataType: "html",
                success : function(data){
                    $('.fetched-data').html(data);
                }
            });
        });

        function deleteFleet(id) {

            swal({
                title: "Are you sure?",
                text: "You are about to delete this fleet item.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Delete",
                closeOnConfirm: false
            }, function(){

                $.ajax({
                    type : 'post',
                    url: "{{ url('/client/fleet/delete') }}/"+ id ,
                    dataType: "html",
                    success : function(data){
                        //$('.fetched-data').html(data);
                        swal({
                            title:  "Deleted!",
                            text:   "That fleet item has been deleted.",
                            type:   "success",
                            timer:  3000,
                            showCancelButton: false,
                            showConfirmButton: false
                        });
                        location.reload();
                    }
                });

            });

        }



    </script>


@endsection
