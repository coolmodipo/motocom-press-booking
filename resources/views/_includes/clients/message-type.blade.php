<div class="col-md-4 col-sm-4">

    <?php $messageTypes = \App\MessageType::all(); ?>

    <ul class="list-group list-group-full">
        @foreach($messageTypes as $type)
            <a class="dark" href="{{ route('client.messages', strtolower($type->type)) }}">
                <li class="list-group-item {{ Request::is('client/messages/' .strtolower($type->type) ) ? 'active' : '' }}">
                    @if($type->messages->count())
                        <span class="badge badge-success">{{ $type->messages->count() }}</span>
                    @endif
                    {{ $type->type }}
                </li>
            </a>
        @endforeach

    </ul>

    <button class="btn btn-primary btn-rounded waves-effect waves-light" type="button" data-toggle="modal" data-target="#modal_create" ><span class="btn-label"><i class="fa fa-envelope-o"></i></span>Compose Mail</button>

</div>
