<section class="content-header">
    <h1>
        Booking Type List
    </h1>

    <ol class="breadcrumb">
        <li><a href="{{ route('admin.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-bookmark"></i> Booking Type</li>
    </ol>
</section>