<section class="content-header">
    <h1>
        Fleet List{{ Route::current()->getName() == 'admin.fleets.company' ? ': '. $company->company_name : '' }}
    </h1>

    <ol class="breadcrumb">
        <li><a href="{{ route('admin.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        @if(Route::current()->getName() == 'admin.fleets.company')
            <li><a href="{{ route('admin.fleets') }}"><i class="fa fa-cubes"></i> Fleets</a></li>
            <li class="active"><i class="fa fa-cube"></i> {{ $company->company_name }}</li>
        @else
            <li class="active"><i class="fa fa-cubes"></i> Fleets</li>
        @endif

    </ol>
</section>