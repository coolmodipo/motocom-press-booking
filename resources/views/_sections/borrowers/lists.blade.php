<section class="content-header">
    <h1>
        Borrowers
    </h1>

    <ol class="breadcrumb">
        <li><a href="{{ route('admin.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-sign-out"></i> Borrowers</li>
    </ol>
</section>