<section class="content-header">
    <h1>
        User Permissions
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('admin.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('admin.users') }}"><i class="fa fa-users"></i> Users</a></li>
        <li class="active"> Permissions</li>
    </ol>
</section>