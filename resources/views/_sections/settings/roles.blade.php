<section class="content-header">
    <h1>
        Roles
    </h1>

    <ol class="breadcrumb">
        <li><a href="{{ route('admin.index') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i> Settings</a></li>
        <li class="active"><i class="fa fa-gear"></i> Roles</li>
    </ol>
</section>
