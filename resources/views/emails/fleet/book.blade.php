@component('mail::message')
# Dear {{ $content['name'] }}

Your {{ $content['vehicle'] }} has been booked for you.

The dates are from {{ $content['start_date'] }} to {{ $content['end_date'] }}.

The model will be collected by {{ $content['collector'] }} at {{ $content['collector_time'] }} and returned by {{ $content['returnee'] }} at  {{ $content['returnee_time'] }}.

If there are any issues, please let us know as soon as possible,

Thanks,<br>
{{ env('APP_NAME') }}
@endcomponent
